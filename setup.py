from setuptools import setup, find_packages

setup(
	name = "croc_wlt",
	version = "1.5.3",
	author = "Michael Grippo",
	description = (
		"Python software to perform wafer-level testing of the CMS Readout "
		"Chips for High Luminosity LHC"
	),
	packages = find_packages(),
	install_requires = [
		"pyserial",
		"numpy",
		"toml",
		"lxml",
		"matplotlib<3.8",
		"PyPDF2<3",
		"requests",
		"ilock"
	],
	#entry_points = {}
)
