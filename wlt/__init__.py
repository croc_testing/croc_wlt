"""
Python package for the wafer-level testing (WLT) of the CMS Readout Chips (CROCs)
for the High Luminosity upgrade.

Modules:
	wafertester: operates the probe station (chuck movement, status, ...)

	chiptester: runs a certain list of tests on a chip

	wafermap: utility module to create wafer maps with the wafer probing results

	waferanalyzer: analyzes the outputs of the wafer probing process

Packages:
	hardware: contains the libraries used to manage the probe station, the 
		power supplies and the Wafer Probing Auxiliary Card (WPAC)
	daq: manages the CMS Inner Tracker DAQ
	analysis: contains the utilities used for the analysis of waferprobing 
		results
"""

__version__ = "1.5.3"

import os

from enum import Enum
from dataclasses import dataclass


#enumeration for the powering **modes** of the chip
class PoweringModes(Enum):
	"""Stores the different powering modes of the chip."""
	LDO = "ldo"
	SLDO = "shunt"
	DIRECT = "direct"


#enumeration for the powering **configurations** of the chip
class PoweringConfigs(Enum):
	"""Stores the different powering configurations of the chip."""
	STARTUP = "startup_power"
	STANDARD = "standard_power"
	LOW = "low_power"


#dataclass for storing veto conditions
@dataclass
class VetoCondition:
	"""Stores veto condition data."""
	code: int
	name_: str


#veto conditions for discarding a chip
class VetoConditions(VetoCondition, Enum):
	"""
	Enumeration of possible veto conditions.

	Veto conditions are applied to override all other test results. Conditions 
	that force a chip to be set as yellow have codes from 1 to 99, whilst veto 
	conditions that mark a chip as red have a code of 100 or bigger.

	The absence of a veto condition does not force a chip to be marked as 
	green.
	"""

	#only one green condition
	NO_VETO = 0, "No veto"

	#yellow conditions
	USED_FOR_DEBUGGING = 1, "Used for debugging"
	OPTICAL_INSPECTION_MILD = 2, "Failed optical inspection (mild)"
	NO_EFUSES = 3, "Efuses not written"
	BAD_PADS = 4, "Pads in bad condition"
	DAQ_MILD = 5, "DAQ problems (mild)"
	BAD_CONTACT = 6, "Bad contact when tested"
	BAD_WAFER_MILD = 7, "Wafer with odd results"
	CONTAMINATION_MILD = 8, "Contamination"

	#red conditions
	MECHANICAL_DAMAGE = 100, "Mechanical damage"
	ELECTRICAL_DAMAGE = 101, "Electrical damage"
	CONTAMINATION = 102, "Contamination (severe)"
	VACUUM_LOSS = 103, "Vacuum loss"
	DRY_AIR_LOSS = 104, "Dry air loss during"
	PROBER_MALFUNCTION = 105, "Prober malfunction"
	OPTICAL_INSPECTION_SEVERE = 106, "Optical inspection (severe)"
	DAQ_SEVERE = 107, "DAQ problems (severe)"
	PROBING_ACCIDENT = 108, "Probing accident (severe)"
	POWER_OUTAGE = 109, "Power outage"
	WRONG_EFUSES = 110, "Wrong efuses"
	POWER_SUPPLY_TRIP = 111, "Power supply trip"
	LOST_CONTACT = 112, "Lost contact when testing"
	POWER_SUPPLY_PROBLEM = 113, "Power supply problem"
	BAD_WAFER_SEVERE = 114, "Wafer with bad results"
	UNTESTED = 115, "Untested chip"


#class used to stop the waferprobing process altogether
class AbortWaferprobing(Exception):
	"""Interrupts the waferprobing process when raised."""


class SkipChip(Exception):
	"""Exception used to skip testing of a single chip."""


#class used to stop the waferprobing process in case of high contact resistance
class HighContactResistance(AbortWaferprobing):
	"""
	Exception raised when the contact resistance detected is too high.

	It inherits from AbortWaferprobing, so it is capable of aborting the 
	waferprobing process.
	"""


#checking that the setup.sh file has been sourced
if "WLT_DIR" not in os.environ:
	raise RuntimeError("Environment variables not defined because the setup.sh "
		"script has not been sourced. Perform this operation and try again.")
