"""Probing site definitions."""

from enum import Enum
from dataclasses import dataclass


#class to store probing site information
@dataclass
class ProbingSite:
	"""Stores information about a probing site."""
	name_: str
	id_: int


#enumeration to store all possible probing sites
class ProbingSites(ProbingSite, Enum):
	"""Enumeration that stores all probing sites."""
	TORINO = "Torino", 1
	KSU = "Kansas", 2
