"""
Module for the configuration of the `daq` subpackage and, in turn, of Ph2_ACF.
"""

import copy

from wlt.duts.chips import CROC_V1, CROC_V2
from wlt.duts.chips import ITKPIX_V1, ITKPIX_V2

#dictionary that stores the DAQ configuration (chip registers and configuration
#of testing routines) for different chips
DAQ_CONFIGS = {
	CROC_V1: {},
	CROC_V2: {},
	ITKPIX_V1: {},
	ITKPIX_V2: {}
}

#common structure for chip configuration
_BASE_CHIP_CONFIG = {
	#pixel registers
	"Pixels": {"tdac": "tdac.csv", "enable": "enable.csv"},
	#global registers
	"Registers": {},
	#special case: core columns
	"CoreColumns": {"disable": [], "disableInjections": []}
}

#common structure for tools configuration
#TODO: remove DACtest configuration from base config and move it to chips config
_BASE_TOOLS_CONFIG = {
	"RegReader": {
		"type": "RD53BRegReader", "args": {}
	},
	"FWRegReader": {
		"type": "RD53BFWRegReader", "args": {}
	},
	"GlobalRegTest": {
		"type": "RD53BRegTest",
		"args": {
			"testRegs": True,
			"testPixels": False,
			"saveSuccesses": False,
			"ServiceFrameSkip": 50,
			"customVectors": {
				"ClockEnableConf": [],
				"IOLaneMappingMux": [],
				"DataMergingMux": [],
				"DataMerging": [],
				"DAC_COMP_LIN": [170, 256, 85],
				"DAC_PREAMP_M_LIN": [170, 256, 85],
				"VOLTAGE_TRIM": [170, 85]
			}
		}
	},
	"PixelRegTest": {
		"type": "RD53BRegTest",
		"args": {
			"testRegs": False,
			"testPixels": True,
			"saveSuccesses": False,
			"ServiceFrameSkip": 50,
			"customVectors": {}
		}
	},
	"DigitalScan": {
		"type": "RD53BInjectionTool",
		"args": {
			"injectionType": "Digital",
			"nInjections": 100,
			"pulseDuration": 6,
			"enableToTReadout": True,
			"enableBCIDReadout": False,
			"expectedTot": 0,
			"triggerDuration": 4,
			"triggerLatency": 133,
			"delayAfterPrime": 100,
			"delayAfterInject": 32,
			"delayAfterTrigger": 600,
			"readoutTimeoutSeconds": 1,
			"maxFailedAttempts": 3,
			"verboseMode": True,
			"offset": [0, 0],
			"size": [0, 0],
			"maskGen": [
				{"dim": 0, "size": 0, "parallel": False, "shift": [1]},
				{"dim": 1, "size": 4, "parallel": True, "shift": []},
				{"dim": 1, "size": 2, "parallel": True, "shift": []},
				{"dim": 0, "size": 1, "parallel": True, "shift": [4]},
				{"dim": 1, "size": 0, "parallel": True, "shift": []},
				{"dim": 0, "size": 8, "parallel": True, "shift": []}
			],
		}
	},
	"DisableTest": {
		"type": "RD53BInjectionTool",
		"args": {
			"injectionType": "Digital",
			"nInjections": 100,
			"pulseDuration": 6,
			"enableToTReadout": True,
			"enableBCIDReadout": False,
			"expectedTot": 0,
			"triggerDuration": 4,
			"triggerLatency": 133,
			"delayAfterPrime": 100,
			"delayAfterInject": 32,
			"delayAfterTrigger": 600,
			"readoutTimeoutSeconds": 1,
			"maxFailedAttempts": 3,
			"disableUnusedPixels": True,
			"injectUnusedPixels": True,
			"verboseMode": True,
			"offset": [0, 0],
			"size": [0, 0],
			"maskGen": [
				{"dim": 0, "size": 0, "parallel": False, "shift": [1]},
				{"dim": 1, "size": 4, "parallel": True, "shift": []},
				{"dim": 1, "size": 2, "parallel": True, "shift": []},
				{"dim": 0, "size": 1, "parallel": True, "shift": [4]},
				{"dim": 1, "size": 0, "parallel": True, "shift": []},
				{"dim": 0, "size": 8, "parallel": True, "shift": []}
			],
		}
	},
	"ToTMemTest": {
		"type": "RD53BInjectionTool",
		"args": {
			"injectionType": "Digital",
			"nInjections": 10,
			"pulseDuration": 6,
			"enableToTReadout": True,
			"enableBCIDReadout": False,
			"expectedTot": 0,
			"triggerDuration": 1,
			"injectionDuration": 8,
			"triggerLatency": 377,
			"delayAfterPrime": 100,
			"delayAfterInject": 9,
			"delayAfterTrigger": 10,
			"decoderThreads": 2,
			"maxEventQueueSize": 5,
			"verboseMode": True,
			"offset": [0, 0],
			"size": [0, 0],
			"maskGen": [
				{ "dim": 0, "size": 0, "parallel": False, "shift": [1] },
				{ "dim": 1, "size": 4, "parallel": True, "shift": [] },
				{ "dim": 1, "size": 2, "parallel": True, "shift": [] },
				{ "dim": 0, "size": 1, "parallel": True, "shift": [4] },
				{ "dim": 1, "size": 0, "parallel": True, "shift": [] },
				{ "dim": 0, "size": 1, "parallel": True, "shift": [] },
				{ "dim": 0, "size": 2, "parallel": True, "shift": []}
			]
		}
	},
	"AnalogScan": {
		"type": "RD53BInjectionTool",
		"args": {
			"injectionType": "Analog",
			"nInjections": 100,
			"enableToTReadout": True,
			"enableBCIDReadout": False,
			"triggerDuration": 4,
			"triggerLatency": 133,
			"delayAfterPrime": 100,
			"delayAfterInject": 32,
			"delayAfterTrigger": 600,
			"readoutTimeoutSeconds": 1,
			"maxFailedAttempts": 3,
			"verboseMode": True,
			"offset": [0, 0],
			"size": [0, 0],
			"maskGen": [
				{"dim": 0, "size": 0, "parallel": False, "shift": [1]},
				{"dim": 1, "size": 4, "parallel": True, "shift": []},
				{"dim": 1, "size": 2, "parallel": True, "shift": []},
				{"dim": 0, "size": 1, "parallel": True, "shift": [4]},
				{"dim": 1, "size": 0, "parallel": True, "shift": []},
				{"dim": 0, "size": 8, "parallel": True, "shift": []}
			],
		}
	},
	"ThresholdScan": {
		"type": "RD53BThresholdScan",
		"args": {
			"injectionTool": "AnalogScan",
			"vcalMed": 300,
			"vcalRange": [0, 1000],
			"vcalStep": 30
		}
	},
	"ThresholdEqualization": {
		"type": "RD53BThresholdEqualization",
		"args": {
			"thresholdScan": "ThresholdScan",
			"injectionTool": "AnalogScan",
			"initialTDAC": 16,
			"targetThreshold": 0,
			"nSteps": 7
		}
	},
	"RingOsc": {
		"type": "RD53RingOscillatorWLT",
			"args": {
				"vmeter": "MyKeithley",
				"vmeterCH": "Front",
				"minVDDD": 0,
				"maxVDDD": 15,
				"abortVDDD": 1.29
			}
	},
	"CapMeasure": {
		"type": "RD53BCapMeasure",
		"args": {
			"powerSupplyName": "MyKeithley",
			"powerSupplyChannelId": "Front",
			"resistance": 5e3,
			"gndCorrection": True
		}
	},
	"ADCCalib": {
		"type": "RD53BADCCalib",
		"args": {
			"voltmeterId": "MyKeithley",
			"voltmeterChannelId": "Front",
			"powerSupplyId": "MyKeithley",
			"powerSupplyChannelId": "Front",
			"powerSupplySamples": 50,
			"nVoltages": 20,
			"adcSamples": 10,
			"minVoltage": 0.01,
			"maxVoltage": 0.8,
			"groundCorrection": True,
			"abortOnLimit": True
		}
	},
	"DACTest": {
		"type": "RD53BDACTest",
		"args": {
			"voltmeterId": "MyKeithley",
			"voltmeterChannelId": "Front",
			"keithSpeed": 1.0,
			"voltmeterSamples": 1,
			"currentMuxResistance": 5.00e3,
			"n": {
				"FC_BIAS_LIN": 3,
				"Vthreshold_LIN": 3,
				"COMP_LIN": 3,
				"COMP_STAR_LIN": 3,
				"LDAC_LIN": 3,
				"KRUM_CURR_LIN": 3,
				"PA_IN_BIAS_LIN": 3,
				"REF_KRUM_LIN": 3
			},
			"gndsel": 19,
			"debug": False,
			"target": {
				#"Vthreshold_LIN": ,
				"DAC_PREAMP_M_LIN": 28.4e-6,
				"DAC_PREAMP_T_LIN": 28.4e-6,
				"DAC_PREAMP_R_LIN": 28.4e-6,
				"DAC_PREAMP_L_LIN": 28.4e-6,
				"DAC_PREAMP_TR_LIN": 28.4e-6,
				"DAC_PREAMP_TL_LIN": 28.4e-6,
				"DAC_LDAC_LIN": 14.2e-6,
				"DAC_COMP_LIN": 10.3e-6,
				"DAC_COMP_TA_LIN": 10.1e-6,
				"DAC_GDAC_M_LIN": 0.460,
				"DAC_GDAC_L_LIN": 0.460,
				"DAC_GDAC_R_LIN": 0.460,
				"DAC_REF_KRUM_LIN": 0.365
				#"COMP_LIN": 10.3-6,
				#"COMP_STAR_LIN": 10.1-6,
				#"LDAC_LIN": 12.8-6,
				#"PA_IN_BIAS_LIN": 28.4-6,
			}
		}
	},
	"DataMergingTest": {
		"type": "RD53BDataMergingTestWLT",
		"args": {}
	},
	"ChipId": {
		"type": "RD53BChipIdTest",
		"args": {
			"wpacPort": "/dev/null", #must configure this at runtime
			#"extraReads": 9
		}
	},
	"Efuse": {
		"type": "RD53BEfuseProgrammer",
		"args": {
			"wpacPort": "/dev/null", #must configure this at runtime
			"word": 0 #safety value to be overwriten at runtime
		}
	},
	"TempCalib": {
		"type": "RD53TempSensorWLT",
		"args": {
			"voltmeterId": "MyKeithley",
			"voltmeterChannelId": "Front",
			"millisecondsAfterSettingVMUX": 0,
			"nDEM": 16,
			"useADC": False,
			"adcSamples": 10,
			"wpacTempSens": 4, #use the WPAC temperature sensor by default
			"wpacPort": "/dev/null",
		}
	},
	"TempMeas": {
		"type": "RD53BTemp",
		"args": {
			"voltmeterId": "MyKeithley",
			"voltmeterChannelId": "Front",
			"enSens": [
				"TEMPSENS_SLDOA",
				"TEMPSENS_SLDOD",
				"TEMPSENS_ACB",
				"RPOLYTSENS_TOP",
				"RPOLYTSENS_BOTTOM"
			]
		}
	},
	"MonMuxTest": {
		"type": "RD53BMonMuxTest",
		"args": {
			"voltmeterId": "MyKeithley",
			"voltmeterChannelId": "Front",
			"currentMuxResistance": 5.00e3,
			"GNDA_REF1": 0.0,
			"GNDA_REF2": 0.0,
			"GNDD_REF": 0.0,
			"vList": [0, 2, 3, 31, 32, 33, 34, 35, 36, 37, 38, 39],
			"iList": [0, 1, 2, 3, 4, 5, 6, 7, 8, 28, 29, 30, 31],
		}
	},
	"IVMonMuxTest": {
		"type": "RD53BMonMuxTest",
		"args": {
			"GNDA_REF1": 0.0,
			"GNDA_REF2": 0.0,
			"GNDD_REF": 0.0,
			"voltmeterId": "MyKeithley",
			"voltmeterChannelId": "Front",
			"currentMuxResistance": 5.00e3,
			"vList": [4, 31, 32, 33, 34, 35, 36, 37, 38, 39],
			"iList": [0, 1, 2, 3, 4, 5, 6, 7, 8, 28, 29, 30, 31]
		}
	},
	"ScanChainTest": {
		"type": "RD53BScanChainTestWLT",
		"args": {}
	}
}

#base XML configuration for the DAQ
_BASE_DAQ_CONFIG = b"""<?xml version="1.0" encoding="UTF-8"?>

<HwDescription>
  <BeBoard Id="0" boardType="RD53" eventType="VR">
    <connection id="cmsinnertracker.crate0.slot0" uri="chtcp-2.0://localhost:10203?target=192.168.1.80:50001" address_table="file://${PH2ACF_BASE_DIR}/settings/address_tables/CMSIT_address_table.xml" />

    <!-- Frontend chip configuration -->
    <OpticalGroup Id="0" FMCId="0">
      <Hybrid Id="0" enable="1">
        <RD53_Files path="./" />

        <CROC Id="15" Lane="0" configfile="CROC.toml" RxGroups="6" RxChannels="0" TxGroups="3" TxChannels="0">
          <LaneConfig outputLanes="1234"/>
          <Settings />
        </CROC>

        <Global />
      </Hybrid>
    </OpticalGroup>

    <!-- Configuration for backend readout board -->
    <Register name="user">
      <Register name="ctrl_regs">

        <Register name="fast_cmd_reg_2">
          <Register name="trigger_source"> 2 </Register>
          <Register name="HitOr_enable_l12"> 0 </Register>
        </Register>

        <Register name="ext_tlu_reg1">
          <Register name="dio5_ch1_thr"> 128 </Register>
          <Register name="dio5_ch2_thr"> 128 </Register>
        </Register>

        <Register name="ext_tlu_reg2">
          <Register name="dio5_ch3_thr"> 128 </Register>
          <Register name="dio5_ch4_thr"> 128 </Register>
          <Register name="dio5_ch5_thr"> 128 </Register>

          <Register name="ext_clk_en"> 0 </Register>
        </Register>

        <Register name="fast_cmd_reg_3">
          <Register name="triggers_to_accept"> 10 </Register>
        </Register>

        <Register name="gtx_drp">
          <Register name="aurora_speed"> 0 </Register>
        </Register>

      </Register>
    </Register>

  </BeBoard>

  <Settings>
    <Setting name="RD53BToolsConfigFile">CROCTools.toml</Setting>
  </Settings>

  <!-- === Communication parameters === -->
  <CommunicationSettings>
      <PowerSupplyClient ip="127.0.0.1" port="7000" enableConnection="1"/>
  </CommunicationSettings>

</HwDescription>
"""

_ITKPIX_V2_DAQ_CONFIG = b"""<?xml version="1.0" encoding="UTF-8"?>

<HwDescription>
  <BeBoard Id="0" boardType="RD53" eventType="VR">
    <connection id="cmsinnertracker.crate0.slot0" uri="chtcp-2.0://localhost:10203?target=192.168.1.80:50001" address_table="file://${PH2ACF_BASE_DIR}/settings/address_tables/CMSIT_address_table.xml" />

    <!-- Frontend chip configuration -->
    <OpticalGroup Id="0" FMCId="0">
      <Hybrid Id="0" enable="1">
        <RD53_Files path="./" />

        <ITkPixV2 Id="15" Lane="0" configfile="ITkPixV2.toml" RxGroups="6" RxChannels="0" TxGroups="3" TxChannels="0">
          <LaneConfig outputLanes="1234"/>
          <Settings />
        </ITkPixV2>

        <Global />
      </Hybrid>
    </OpticalGroup>

    <!-- Configuration for backend readout board -->
    <Register name="user">
      <Register name="ctrl_regs">

        <Register name="fast_cmd_reg_2">
          <Register name="trigger_source"> 2 </Register>
          <Register name="HitOr_enable_l12"> 0 </Register>
        </Register>

        <Register name="ext_tlu_reg1">
          <Register name="dio5_ch1_thr"> 128 </Register>
          <Register name="dio5_ch2_thr"> 128 </Register>
        </Register>

        <Register name="ext_tlu_reg2">
          <Register name="dio5_ch3_thr"> 128 </Register>
          <Register name="dio5_ch4_thr"> 128 </Register>
          <Register name="dio5_ch5_thr"> 128 </Register>

          <Register name="ext_clk_en"> 0 </Register>
        </Register>

        <Register name="fast_cmd_reg_3">
          <Register name="triggers_to_accept"> 10 </Register>
        </Register>

        <Register name="gtx_drp">
          <Register name="aurora_speed"> 0 </Register>
        </Register>

      </Register>
    </Register>

  </BeBoard>

  <Settings>
    <Setting name="RD53BToolsConfigFile">ITkPixV2Tools.toml</Setting>
  </Settings>

  <!-- === Communication parameters === -->
  <CommunicationSettings>
      <PowerSupplyClient ip="127.0.0.1" port="7000" enableConnection="1"/>
  </CommunicationSettings>

</HwDescription>
"""

_CROC_V2_DAQ_CONFIG = b"""<?xml version="1.0" encoding="UTF-8"?>

<HwDescription>
  <BeBoard Id="0" boardType="RD53" eventType="VR">
    <connection id="cmsinnertracker.crate0.slot0" uri="chtcp-2.0://localhost:10203?target=192.168.1.80:50001" address_table="file://${PH2ACF_BASE_DIR}/settings/address_tables/CMSIT_address_table.xml" />

    <!-- Frontend chip configuration -->
    <OpticalGroup Id="0" FMCId="0">
      <Hybrid Id="0" enable="1">
        <RD53_Files path="./" />

        <CROCv2 Id="15" Lane="0" configfile="CROCv2.toml" RxGroups="6" RxChannels="0" TxGroups="3" TxChannels="0">
          <LaneConfig outputLanes="1234"/>
          <Settings />
        </CROCv2>

        <Global />
      </Hybrid>
    </OpticalGroup>

    <!-- Configuration for backend readout board -->
    <Register name="user">
      <Register name="ctrl_regs">

        <Register name="fast_cmd_reg_2">
          <Register name="trigger_source"> 2 </Register>
          <Register name="HitOr_enable_l12"> 0 </Register>
        </Register>

        <Register name="ext_tlu_reg1">
          <Register name="dio5_ch1_thr"> 128 </Register>
          <Register name="dio5_ch2_thr"> 128 </Register>
        </Register>

        <Register name="ext_tlu_reg2">
          <Register name="dio5_ch3_thr"> 128 </Register>
          <Register name="dio5_ch4_thr"> 128 </Register>
          <Register name="dio5_ch5_thr"> 128 </Register>

          <Register name="ext_clk_en"> 0 </Register>
        </Register>

        <Register name="fast_cmd_reg_3">
          <Register name="triggers_to_accept"> 10 </Register>
        </Register>

        <Register name="gtx_drp">
          <Register name="aurora_speed"> 0 </Register>
        </Register>

      </Register>
    </Register>

  </BeBoard>

  <Settings>
    <Setting name="RD53BToolsConfigFile">CROCv2Tools.toml</Setting>
  </Settings>

  <!-- === Communication parameters === -->
  <CommunicationSettings>
      <PowerSupplyClient ip="127.0.0.1" port="7000" enableConnection="1"/>
  </CommunicationSettings>

</HwDescription>
"""

###############
# CROCv1 chip #
###############

DAQ_CONFIGS[CROC_V1]["chip"] = copy.deepcopy(_BASE_CHIP_CONFIG)
DAQ_CONFIGS[CROC_V1]["chip"]["Registers"].update(
	{
		#configured by the DAQ
		#"ActiveLanes": 0b1111,
		#"SER_EN_LANE": 0b01010101,
		#"CML_CONFIG" = 0b1111,
		"EnCoreCol": 0b111111111111111111111111111111111111111111111111111111,
		"CCWait": 26,
		"DAC_PREAMP_L_LIN": 300,
		"DAC_PREAMP_R_LIN": 300,
		"DAC_PREAMP_TL_LIN": 300,
		"DAC_PREAMP_TR_LIN": 300,
		"DAC_PREAMP_T_LIN": 300,
		"DAC_PREAMP_M_LIN": 300,
		"DAC_FC_LIN": 20,
		"DAC_KRUM_CURR_LIN": 70,
		"DAC_REF_KRUM_LIN": 360,
		"DAC_COMP_LIN": 110,
		"DAC_COMP_TA_LIN": 110,
		"DAC_GDAC_L_LIN": 465,
		"DAC_GDAC_R_LIN": 465,
		"DAC_GDAC_M_LIN": 465,
		"DAC_LDAC_LIN": 140,
		"DAC_PREAMP_M_DIFF": 895,
		"DAC_PREAMP_L_DIFF": 895,
		"DAC_PREAMP_R_DIFF": 895,
		"DAC_PREAMP_T_DIFF": 895,
		"DAC_PREAMP_TL_DIFF": 895,
		"DAC_PREAMP_TR_DIFF": 895,
		"DAC_PRECOMP_DIFF": 350,
		"DAC_COMP_DIFF": 523,
		"DAC_VFF_DIFF": 160,
		"DAC_TH1_M_DIFF": 260,
		"DAC_TH1_L_DIFF": 260,
		"DAC_TH1_R_DIFF": 260,
		"DAC_TH2_DIFF": 0,
		"DAC_LCC_DIFF": 200,
		"LEACKAGE_FEEDBACK": 0,
		"AnalogInjectionMode": 0,
		"VCAL_HIGH": 1400,
		"VCAL_MED": 300,
		"PIX_MODE": 10
	}
)
DAQ_CONFIGS[CROC_V1]["tools"] = copy.deepcopy(_BASE_TOOLS_CONFIG)
DAQ_CONFIGS[CROC_V1]["daq"] = copy.deepcopy(_BASE_DAQ_CONFIG)
DAQ_CONFIGS[CROC_V1]["chip"]["Pixels"] = {
	"enable": "CROCv1Enable.csv",
	"tdac": "CROCv1TDAC.csv",
}

#################
# ITkPixV1 chip #
#################

DAQ_CONFIGS[ITKPIX_V1]["chip"] = copy.deepcopy(_BASE_CHIP_CONFIG)
DAQ_CONFIGS[ITKPIX_V1]["chip"].update(
	{
		#configured by the DAQ
		#"ActiveLanes": 0b1111,
		#"SER_EN_LANE": 0b01010101,
		#"CML_CONFIG" = 0b1111,
		"EnCoreCol": 0b11111111111111111111111111111111111111111111111111,
		"CCWait": 26,
		"TRIM_VREFA": 8,
		"TRIM_VREFD": 8,
		"DAC_PREAMP_M_DIFF": 895,
		"DAC_PREAMP_L_DIFF": 895,
		"DAC_PREAMP_R_DIFF": 895,
		"DAC_PREAMP_T_DIFF": 895,
		"DAC_PREAMP_TL_DIFF": 895,
		"DAC_PREAMP_TR_DIFF": 895,
		"DAC_PRECOMP_DIFF": 300,
		"DAC_COMP_DIFF": 523,
		"DAC_VFF_DIFF": 160,
		"DAC_TH1_M_DIFF": 220,
		"DAC_TH1_L_DIFF": 220,
		"DAC_TH1_R_DIFF": 220,
		"DAC_TH2_DIFF": 0,
		"DAC_LCC_DIFF": 200,
		"DAC_PREAMP_L_LIN": 300,
		"DAC_PREAMP_R_LIN": 300,
		"DAC_PREAMP_TL_LIN": 300,
		"DAC_PREAMP_TR_LIN": 300,
		"DAC_PREAMP_T_LIN": 300,
		"DAC_PREAMP_M_LIN": 300,
		"DAC_FC_LIN": 20,
		"DAC_KRUM_CURR_LIN": 70,
		"DAC_REF_KRUM_LIN": 360,
		"DAC_COMP_LIN": 110,
		"DAC_COMP_TA_LIN": 110,
		"DAC_GDAC_L_LIN": 465,
		"DAC_GDAC_R_LIN": 465,
		"DAC_GDAC_M_LIN": 465,
		"DAC_LDAC_LIN": 140,
		"LEACKAGE_FEEDBACK": 0,
		"VCAL_HIGH": 1400,
		"VCAL_MED": 300
	}

)
DAQ_CONFIGS[ITKPIX_V1]["tools"] = copy.deepcopy(_BASE_TOOLS_CONFIG)
DAQ_CONFIGS[ITKPIX_V1]["daq"] = copy.deepcopy(_BASE_DAQ_CONFIG)
DAQ_CONFIGS[ITKPIX_V1]["chip"]["Pixels"] = {
	"enable": "ITkPixV1Enable.csv",
	"tdac": "ITkPixV1TDAC.csv",
}

#################
# ITkPixV2 chip #
#################

DAQ_CONFIGS[ITKPIX_V2]["chip"] = copy.deepcopy(_BASE_CHIP_CONFIG)
DAQ_CONFIGS[ITKPIX_V2]["chip"]["Registers"].update(
	{
		#configured by the DAQ
		#"ActiveLanes": 0b1111,
		#"SER_EN_LANE": 0b01010101,
		#"CML_CONFIG" = 0b1111,
		"EnCoreCol": 0b11111111111111111111111111111111111111111111111111,
		"CCWait": 26,
		"TRIM_VREFA": 8,
		"TRIM_VREFD": 8,
		"DAC_PREAMP_M_DIFF": 895,
		"DAC_PREAMP_L_DIFF": 895,
		"DAC_PREAMP_R_DIFF": 895,
		"DAC_PREAMP_T_DIFF": 895,
		"DAC_PREAMP_TL_DIFF": 895,
		"DAC_PREAMP_TR_DIFF": 895,
		"DAC_PRECOMP_DIFF": 350,
		"DAC_COMP_DIFF": 523,
		"DAC_VFF_DIFF": 160,
		"DAC_TH1_M_DIFF": 260,
		"DAC_TH1_L_DIFF": 260,
		"DAC_TH1_R_DIFF": 260,
		"DAC_TH2_DIFF": 0,
		"DAC_LCC_DIFF": 200,
		"DAC_PREAMP_L_LIN": 300,
		"DAC_PREAMP_R_LIN": 300,
		"DAC_PREAMP_TL_LIN": 300,
		"DAC_PREAMP_TR_LIN": 300,
		"DAC_PREAMP_T_LIN": 300,
		"DAC_PREAMP_M_LIN": 300,
		"DAC_FC_LIN": 20,
		"DAC_KRUM_CURR_LIN": 70,
		"DAC_REF_KRUM_LIN": 360,
		"DAC_COMP_LIN": 110,
		"DAC_COMP_TA_LIN": 110,
		"DAC_GDAC_L_LIN": 465,
		"DAC_GDAC_R_LIN": 465,
		"DAC_GDAC_M_LIN": 465,
		"DAC_LDAC_LIN": 140,
		"LEACKAGE_FEEDBACK": 0,
		"VCAL_HIGH": 1400,
		"VCAL_MED": 300
	}
)
DAQ_CONFIGS[ITKPIX_V2]["tools"] = copy.deepcopy(_BASE_TOOLS_CONFIG)
DAQ_CONFIGS[ITKPIX_V2]["daq"] = copy.deepcopy(_ITKPIX_V2_DAQ_CONFIG)
DAQ_CONFIGS[ITKPIX_V2]["chip"]["Pixels"] = {
	"enable": "ITkPixV2Enable.csv",
	"tdac": "ITkPixV2TDAC.csv",
}

###############
# CROCv2 chip #
###############

DAQ_CONFIGS[CROC_V2]["chip"] = copy.deepcopy(_BASE_CHIP_CONFIG)
DAQ_CONFIGS[CROC_V2]["chip"]["Registers"].update(
	{
		#configured by the DAQ
		#"ActiveLanes": 0b1111,
		#"SER_EN_LANE": 0b01010101,
		#"CML_CONFIG" = 0b1111,
		"EnCoreCol": 0b111111111111111111111111111111111111111111111111111111,
		"CCWait": 26,
		"DAC_PREAMP_L_LIN": 300,
		"DAC_PREAMP_R_LIN": 300,
		"DAC_PREAMP_TL_LIN": 300,
		"DAC_PREAMP_TR_LIN": 300,
		"DAC_PREAMP_T_LIN": 300,
		"DAC_PREAMP_M_LIN": 300,
		"DAC_FC_LIN": 20,
		"DAC_KRUM_CURR_LIN": 70,
		"DAC_REF_KRUM_LIN": 360,
		"DAC_COMP_LIN": 110,
		"DAC_COMP_TA_LIN": 110,
		"DAC_GDAC_L_LIN": 465,
		"DAC_GDAC_R_LIN": 465,
		"DAC_GDAC_M_LIN": 465,
		"DAC_LDAC_LIN": 140,
		"DAC_PREAMP_M_DIFF": 895,
		"DAC_PREAMP_L_DIFF": 895,
		"DAC_PREAMP_R_DIFF": 895,
		"DAC_PREAMP_T_DIFF": 895,
		"DAC_PREAMP_TL_DIFF": 895,
		"DAC_PREAMP_TR_DIFF": 895,
		"DAC_PRECOMP_DIFF": 350,
		"DAC_COMP_DIFF": 523,
		"DAC_VFF_DIFF": 160,
		"DAC_TH1_M_DIFF": 260,
		"DAC_TH1_L_DIFF": 260,
		"DAC_TH1_R_DIFF": 260,
		"DAC_TH2_DIFF": 0,
		"DAC_LCC_DIFF": 200,
		"LEACKAGE_FEEDBACK": 0,
		"AnalogInjectionMode": 0,
		"VCAL_HIGH": 1400,
		"VCAL_MED": 300,
		"PIX_MODE": 10
	}
)
DAQ_CONFIGS[CROC_V2]["tools"] = copy.deepcopy(_BASE_TOOLS_CONFIG)
DAQ_CONFIGS[CROC_V2]["daq"] = copy.deepcopy(_CROC_V2_DAQ_CONFIG)
DAQ_CONFIGS[CROC_V2]["chip"]["Pixels"] = {
	"enable": "CROCv2Enable.csv",
	"tdac": "CROCv2TDAC.csv",
}
