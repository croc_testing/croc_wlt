#pylint: disable=C0103,W0611
"""
Main configuration module for the wlt package.

It defines the settings for the WPAC, ChipTester, WaferTester, the instruments 
and more.
"""

import os
import logging

from wlt import PoweringModes

from wlt.sites import ProbingSites

from wlt.duts.chips import ITKPIX_V1, ITKPIX_V2
from wlt.duts.chips import CROC_V1, CROC_V2

from wlt.hardware.probecards import CROC_PROBE_CARD_V1P0
from wlt.hardware.probecards import CROC_PROBE_CARD_V1P1
from wlt.hardware.probecards import CROC_PROBE_CARD_V2P0

from wlt.hardware.probers import CM300xi, TS3000

###############
# directories #
###############

#directory settings taken from the environment variables exported by the
#setup.sh Bash script

#root folder of the package
WLT_DIR = os.getenv("WLT_DIR")
#directory in which the DAQ software can be found
ACF_DIR = os.getenv("ACF_DIR")
#directory in which the Python version DAQ software can be found
SCT_DIR = os.getenv("SCT_DIR")
#directory in which the DAQ executables are run
DAQ_DIR = os.getenv("DAQ_DIR")
#root folder in which the results will be saved
DATA_DIR = os.getenv("DATA_DIR")
#directory that stores local data and configuration
LOCAL_DIR = os.getenv("LOCAL_DIR")

#########
# chips #
#########

#global configuration for the type of the chip under test. Using the chip
#constants is better than typing the value manually
CHIP_TYPE = CROC_V2

#################
# probe station #
#################

#class to be used for probe station handling: can be either CM300xi or TS3000
PROBER_CLS = CM300xi
#GPIB address used for communicating with the probe station
PROBER_ADDR = 22

##############
# probe card #
##############

PROBE_CARD = CROC_PROBE_CARD_V2P0

########
# WPAC #
########

#settings used by ChipTester and by the wpac subpackage
WPAC_CONF = {
	#device file for the serial communication with the WPAC
	"port": "/dev/wpac-serial",
	#baud rate for the communication
	"baud_rate": 230400,
	#termination character for the communication
	"termination": '\n',
	#communication timeout: if no answer is read within that interval, the
	#communication is stopped
	"timeout": 1,
	#type of the chip under test
	"probe_card": PROBE_CARD,
	#list of available temperature sensors
	"temp_sens_ids": [3, 4]
}

##############
# ChipTester #
##############

#list of testing routines to be performed by ChipTester
TESTS_LIST = {
	#perform various measurements related to the power drawn by the chip
	"power_ldo": True,
	#perform the IREF trimming routine
	"iref_trimming": True,
	#check whether communicating with the chip is possible
	"communication": True,
	#test all communication lanes which can be used by the chip
	"lanes_test": False,
	#test that all CHIP_ID values work
	"chip_id": True,
	#efuses programming
	"efuses": True,
	#perform the trimming of the analog and digital VDDs of the chip
	"vdd_trimming": True,
	#perform a measurement of the power drawn by the chips
	"pixel_power": True,
	#perform a R&W test on global chip registers
	"global_registers_test": True,
	#perform a R&W test on pixel chip registers
	"pixel_registers_test": True,
	#perform a test on the circuit for pixel disabling
	"pixel_disable_test": True,
	#perform a test on all ToT and latency memories
	"tot_latency_test": True,
	#perform a data merging test to check data aggregation
	"data_merging": True,
	#perform an additional digital scan in LDO mode for screening purposes
	"digital_scan_ldo": True,
	#perform a digital scan using the DAQ
	"digital_scan_sldo": True,
	#perform an analog scan with the DAQ
	"analog_scan": True,
	#perform a threshold scan with the DAQ
	"threshold_scan": True,
	#perform a threshold trimming procedure
	"threshold_trimming": True,
	#measure the injection capacitance
	"injection_capacitance": True,
	#perform the ADC calibration procedure in LDO
	"adc_calibration_ldo": False,
	#perform the ADC calibration procedure again in SLDO
	"adc_calibration_sldo": True,
	#perform the DAC calibration procedure
	"dac_calibration": True,
	#perform the ring oscillator calibration procedure
	"ringosc_calibration": True,
	#perform a calibration of the on-chip temperature sensors
	"temp_calibration": True,
	#power the chip in shunt mode and measure the power consumption
	"power_shunt": True,
	#perform an IV curve in the shunt powering mode using the default shunt slope
	"shunt_iv_default_slope": True,
	#perform an IV curve in the shunt powering mode using the alternative shunt slope
	"shunt_iv_alternative_slope": False,
	#perform an IV curve with shorted VINA and VIND
	"shunt_iv_shorted": True,
	#perform an IV curve in LDO powering mode
	"ldo_iv": False,
	#turn the chip on in direct power
	"power_direct": True,
	#test the digital chip bottom using the scan chain test
	"scan_chain": True,
	#measure signals from the monitoring mux of the chip
	"chip_mux_monitoring": True
}

#configuration for the testing routines
TESTS_CONF = {
	#configuration for powering up the chip in LDO mode
	"init_power_ldo": {
		"mode": PoweringModes.LDO,
		"vin": 1.6,
		"iin": 2,
		"ovp": 2.1,
		"ocp": 2.1,
	},
	#configuration for the measurements performed at chip startup in LDO
	"init_power_meas_ldo":{
		#cut on contact resistance
		"cres_cut": 40e-3,
	},
	#configuration for powering up the chip in shunt LDO mode
	"init_power_shunt": {
		"mode": PoweringModes.SLDO,
		"vin": 4,
		"iin": 1,
		"ovp": 4.1,
		"ocp": 4.1,
	},
	#configuration for the measurements performed at chip startup in shunt LDO
	"init_power_meas_shunt": {
		#cut on contact resistance
		"cres_cut": 40e-3,
	},
	#configuration for powering up the chip in direct powering mode
	"init_power_direct": {
		"mode": PoweringModes.DIRECT,
		"vdda": 1.25,
		"vddd": 1.25,
		"iin": 4,
		"ovp": 1.6,
		"ocp": 4.1,
	},
	#configuration for IREF trimming
	"iref_trim": {
		#target value of the 4 μA reference current
		"target": 0.5,
		#maximum allowed deviation from the target value
		"tolerance": 0.01,
		#which mux output to use for the trimming
		"mux": "VOFS",
		#value of the resistor
		"resistance": 24.0e3,
		#scale factor
		"scale": 5
	},
	#configuration for the communication routine
	"communication": {
		"n_tries": 3,
		"timeout": 10
	},
	#configuration for the VDD trimming routine
	"vdd_trim": {
		#target value of the trimming for VDD (in volt)
		"target": 1.2,
		#maximum allowed different between the trimmed value and `target`. If
		#this difference is greater than `tolerance`, the trimming is considered
		#as failed
		"tolerance": 0.05,
		#if the measured VDD is above `threshold`, the loop is interrupted.
		#Given that one trim code corresponds to approximately 0,02 V, the
		#threshold should not be set to 1,3 V or above
		"threshold": 1.29
	},
	#configuration for testing the 4 chip lanes
	"lanes_test": {
		#number of times to try to communicate with the chip for each lane
		"n_tries": 3
	},
	#configuration for testing CHIP_ID
	"chip_id": {
		"n_extra_reads": 9
	},
	#configuration for the IV curve in SLDO using the default slope
	"shunt_iv_default_slope": {
		#currents at which the measurements must be performed
		"currents": [0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1],
		#currents at which temperature measurements must be performed
		"currents_temp": [],
		#currents at which soft errors are measured
		"currents_softerrs": [1, 1.2],
		#selects the slope for the IV curve
		"use_default_slope": True,
		#value of the resistor which gives the shunt-LDO slope
		"rext": 580,
		#if set to True, stop the IV curve if a trip condition is detected
		"quit_on_trip": True,
		#run with shorted VINA and VIND?
		"is_shorted": False,
		#soft errors counting time
		"softerrs_delta_s": 2
	},
	#configuration for the IV curve in SLDO using the alternative slope
	"shunt_iv_alternative_slope": {
		#currents at which the measurements must be performed
		"currents": [0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1],
		#currents at which temperature measurements must be performed
		"currents_temp": [],
		#currents at which soft errors are measured
		"currents_softerrs": [],
		#selects the slope for the IV curve
		"use_default_slope": False,
		#value of the resistor which gives the shunt-LDO slope
		"rext": 530,
		#if set to True, stop the IV curve if a trip condition is detected
		"quit_on_trip": True,
		#run with shorted VINA and VIND?
		"is_shorted": False,
		#soft errors counting time
		"softerrs_delta_s": 0
	},
	#configuration for the IV curve in SLDO powering and shorting VINA and VIND
	#together
	"shunt_iv_shorted": {
		#currents at which the measurements must be performed
		"currents": [2.0, 2.5, 3, 3.5, 4],
		#currents at which temperature measurements must be performed
		"currents_temp": [],
		#currents at which soft errors are measured
		"currents_softerrs": [],
		#selects the slope for the IV curve
		"use_default_slope": True,
		#value of the resistor which gives the shunt-LDO slope
		"rext": 580,
		#if set to True, stop the IV curve if a trip condition is detected
		"quit_on_trip": True,
		#run with shorted VINA and VIND?
		"is_shorted": True,
		#soft errors counting time
		"softerrs_delta_s": 0
	},
	#configuration for the IV curve in LDO powering
	"ldo_iv": {
		"voltages": [1.6, 1.5, 1.45, 1.4, 1.35]
	},
	#configuration for the default (coarse) threshold scan
	"threshold_scan": {
		"vcal_med": 300,
		"vcal_high_range": (100, 1100),
		"vcal_high_step": 30
	},
	#threshold equalization configuration
	"threshold_trimming": {
		#number of steps to perform: this parameter has a big effect on the runtime
		"n_steps": 7
	},
	#configuration for the fine threshold scan
	"threshold_scan_fine": {
		"vcal_med": 300,
		"vcal_high_range_width": 50,
		"vcal_high_step": 25
	},
	#configuration for the calibration of the DACs
	"dac_calibration": {
		#"debug": False,
		#"gndsel": 19,
		"update_dacs": True
	},
	#configuration for the calibration of the on-chip temperature sensors
	"temp_calibration": {
		"temp_sens_id": 3,
		"vmux_delay_ms": 0,
	}
	#TODO: add configuration for remaining tests
}

#settings used for ChipTester. The settings marked with * are only used when
#running the chiptester module directly and not through WaferTester
#TODO: move this configuration to command-line parameters
CHIPTESTER_CONF = {
	#probing site
	"probing_site": ProbingSites.TORINO,
	#chip column: goes from 0x0 to 0xF
	"chip_col": 0x0000,
	#chip row: goes from 0x0 to 0xF
	"chip_row": 0x0000,
	#wafer batch identifer obtained from the foundry
	"batch_id": "UNDEF",
	#wafer identifer obtained from the foundry
	"wafer_id": "UNDEF",
	#directory in which the results will be saved (*)
	"data_dir": os.path.join(DATA_DIR, "chips"),
	#verbosity of the logging information
	"log_level": logging.DEBUG,
	#type of the chip under test
	"chip_type": CHIP_TYPE,
	#list of tests to be performed by ChipTester
	"enabled_tests": TESTS_LIST,
	#configuration dictionary for the various tests
	"config_tests": TESTS_CONF
}

###############
# WaferTester #
###############

#waferprobing metadata, such as the operator, the hardware used and so on
#TODO: generate automatically from configuration
METADATA = {
	"probe_card_id": "-1",
	"probe_card_version": "2.0",
	"operator": "",
	"enabled_tests": TESTS_LIST,
	#additional information for the run
	"notes": """"""
}

#settings used when running the main of the wafertester module
#TODO: move this configuration to command-line parameters or separate config
WAFERTESTER_CONF = {
	#probing site
	"probing_site": ProbingSites.TORINO,
	#port to bind to
	"port": 16180,
	#batch identifier from foundry
	"batch_id": "N61F26",
	#wafer identifier from foundry
	"wafer_id": "14G0",
	#wafer type
	"chip_type": CHIP_TYPE,
	#number of chips on which WaferTester will travel to. This does not
	#necessarily mean that they will be tested: this depends on the test_chips
	#attribute of the WaferTester class
	"n_chips_test": 136, #131 for RD53C-ATLAS, 136 for CROCv2
	#column index of the first die to test
	"start_column": 0x8,
	#row index of the first die to test
	"start_row": 0x1,
	#list of dies to test. This overrides the previous settings
	"dies_list": [],
	#verbosity of the logging information
	"log_level": logging.DEBUG,
	#this WaferTester parameter defines whether the chips will be tested or not.
	#If it is set to True, the chips will be tested, otherwise WaferTester will
	#perform the so-called wafer travel
	"test_chips": True,
	#parameter which sets whether the probe station will go to separation height
	#or not after completing the testing of all the chips
	"separate_after_test": True,
	#waferprobing metadata to be saved in the WLT JSON
	"metadata": METADATA,
	#if the contact resistance gets too high, the needles are cleaned
	"autoclean_enabled": True,
	#interval to clean the needles automatically
	"autoclean_interval": 20,
	#overtravel to be used
	"overtravel_um": 60,
	#enable autocontact
	"use_autocontact": False,
	#starting height for the autocontact procedure
	"autocontact_z_start_um": 0,
	#maximum height for the autocontact procedure
	"autocontact_z_max_um": 0,
	#step size for the autocontact procedure
	"autocontact_step_um": 5,
	#number of steps for the autocontact procedure
	"autocontact_n_steps": 30,
	#maximum probe card asymmetry for the autocontact procedure
	"autocontact_max_asymm_um": 15
}

##################
# power supplies #
##################

#configuration for the different instruments
DEVICES = {
	#probe card power supply
	"PC": {
		"device_file": "/dev/tti-pc",
		"baud_rate": 9600,
		"device_name": "PC",
		"device_vendor": "TTi",
		"device_model": "QL355TP",
		"read_termination": "\r\n",
		"write_termination": "\n",
		"timeout": 1,
		"xonxoff": 1
	},
	#power supply for direct powering
	"VDD": {
		"device_file": "/dev/tti-aux",
		"baud_rate": 9600,
		"device_name": "VDD",
		"device_vendor": "TTi",
		"device_model": "QL355TP",
		"read_termination": "\r\n",
		"write_termination": "\n",
		"timeout": 1,
		"xonxoff": 1
	},
	#source-meter unit
	"METER": {
		#"port": 18, #for GPIB communication using the Keithley 2450
		"device_file": "/dev/keithley",
		"baud_rate": 19200,
		"device_name": "METER",
		"device_vendor": "Keithley",
		"device_model": "2400",
		"read_termination": "\r",
		"write_termination": "\r",
		"timeout": 1,
		"xonxoff": 0
	}
}

#mapping for the instrument channels. The 'VINA', 'VIND', 'AUX1' and 'AUX2' keys
#must be specified
SIGNALS = {
	"VINA": {
		"power_supply": "PC",
		"channel": "1"
	},
	"VIND": {
		"power_supply": "PC",
		"channel": "2"
	},
	 "VDDA": {
		"power_supply": "VDD",
		"channel": "1"
	},
	"VDDD": {
		"power_supply": "VDD",
		"channel": "2"
	}
}
