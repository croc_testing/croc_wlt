#pylint: disable = C0301, C0302, W1401, W1515, W0108
"""
Default plotting configuration showing the example syntax.

CONFIG contains the list of individual plots that will be produced.
"""

from wlt.config.analyzer_v1.analyzer_config import CONFIG as config_main

CONFIG = {
	# Global parameters applied to each plot
	'GLOBAL': {
		'out_path_template': 'wafers/{name}_{wafer_id}.pdf',
		'out_path_template_aggregate': '{name}.pdf',
		'legend.loc': 'lower right',
		'overlay.histogram': {
			'counts': True,
			'stats': ('MAX', 'MIN', 'MED', 'AVG', 'RMS'),
		},
	}
}

# Adding good chip counts
CONFIG.update({
	'green_chips': {
		'input': 'chip_counts/0',
		'title': 'Goog chips;# of green chips;Wafers',
		'regions': [100, 140],
		'bins': 1,
		'group': -1,
	},
	'red_chips': {
		'input': 'chip_counts/2',
		'title': 'Failed chips;# of red chips;Wafers',
		'regions': [0, 40],
		'bins': 1,
		'group': -1,
	},
	'tested_chips': {
		'input': 'n_chips_test',
		'title': 'Tested chips;# of chips tested;Wafers',
		'regions': [100, 140],
		'bins': 1,
		'group': -1,
	},
	'overtravel': {
		'input': 'overtravel',
		'title': 'Probe station overtravel;Overtravel [$\mu$m];Wafers',
		'regions': [20, 80],
		'bins': 1,
		'group': -1,
	},

})

# Adding all aggregated histograms from the standard config
STATS_TO_PLOT = {'med': 'MED', 'rms': 'RMS'}
for name, config in config_main.items():
	if 'aggregate' not in config or config['aggregate'] != 'histogram':
		continue
	if 'skip' in config and config['skip']:
		continue
	config_new = {
		'input': {
			'med': f'statistics/{name}/MED',
			'rms': f'statistics/{name}/RMS',
		},
		'output': {
			'value': lambda i: i['med'],
			'error': lambda i: i['rms']
		}
	}
	# Copying values from the original config
	for key in ['group', 'aggregate', 'skip', 'priority']:
		config_new[key] = config[key]
	# Updating the title
	if 'title' in config:
		titles = config['title'].split(';')
		config_new['title'] = f'{titles[0]};{titles[1]};Wafer Index'
	else:
		config_new['title'] = f';{name};Wafer Index'
	# Using the default regions
	config_new['regions'] = config['regions']
	# # Setting the graph aggregation
	config_new['aggregate'] = 'graph_ordered'
	# Storing updated config to the output dictionary
	CONFIG[name] = config_new


# Adding missing default parameter values for completeness
for cname, config in CONFIG.items():
	if 'priority' not in config:
		config['priority'] = 0
	if 'group' not in config:
		config['group'] = -1
	if 'skip' not in config:
		config['skip'] = False
	if 'aggregate' not in config:
		config['aggregate'] = 'histogram'



############################### Command-line functionality
if __name__ == '__main__':

	import pdb
	from wlt.analysis.config_handler import args, handle

	if args.debug:
		pdb.set_trace()

	handle(CONFIG)
