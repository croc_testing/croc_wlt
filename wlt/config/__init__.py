"""Subpackage for handling software and hardware configurations."""

from .main_config import PROBER_CLS, PROBER_ADDR
from .main_config import WPAC_CONF
from .main_config import CHIPTESTER_CONF, WAFERTESTER_CONF
from .main_config import DATA_DIR
from .main_config import LOCAL_DIR
