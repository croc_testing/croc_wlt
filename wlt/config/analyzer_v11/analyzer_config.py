#pylint: disable = C0301, C0302, W1401, W1515, W0108
"""
Default plotting configuration showing the example syntax.

CONFIG contains the list of individual plots that will be produced.
"""

import numpy as np

from wlt.analysis.data_processors import fit_pol, fit_gauss_root
from wlt.analysis.data_processors import th2_to_arr, arr_to_th1
from wlt.analysis.data_processors import occup_to_arr, tot_to_arr
from wlt.analysis.data_processors import count_matches_in_file, count_pixel_register_diffs
from wlt.analysis.configuration import load_regions

version = 11

PLOTS_WAFER = {}
PLOTS_CHIP = {}

REGIONS = load_regions(version)

CONFIG = {
	# Global parameters applied to each plot
	'GLOBAL': {
		'out_path_template': 'chips/{name}_{chip_column:X}{chip_row:X}.pdf',
		'out_path_template_aggregate': 'wafer/{name}.pdf',
		'legend.loc': 'lower right',
		'overlay.histogram': {
			'counts': True,
			'stats': ('MAX', 'MIN', 'MED', 'AVG', 'RMS'),
		},
		'version': f'{version:d}',
		'group_names': {
			1: 'power_ldo',
			2: 'iref_trimming',
			3: 'communication',
			4: 'efuses',
			5: 'vdd_trimming',
			6: 'global_registers_test',
			7: 'capacitance_inj',
			8: 'adc_calibration',
			9: 'dac_calibration',
			10: 'ringosc_calibration',
			11: 'temp_calibration',
			12: 'cb_matrix_current',
			13: 'scan_chain',
			14: 'power_shunt',
			15: 'integrated_mon_mux',
			16: 'shunt_iv_default_slope',
			17: 'shunt_iv_default_slope_shorted',
			18: 'lanes_test',
			19: 'chip_id',
			20: 'pixel_registers_test',
			21: 'data_merging',
			22: 'digital_scan',
			23: 'analog_scan',
			24: 'threshold_scan',
			25: 'threshold_trimming',
			26: 'threshold_scan_post_trim',
			27: 'daq_issue',
			28: 'operator_veto',
		},
		'low_priority_filenames': {
			-1: 'dac_calibrations',
			-2: 'ring_oscillators',
			-3: 'temp_calibrations',
			-4: 'monitoring_mux',
		},
		'summary_text': [
			('batch_id', 'wafer_id', 'chip_type'),
			('n_chips_test', 'start_column', 'start_row'),
			('probe_card_id', 'probe_card_version', 'overtravel'),
			('start_time', 'stop_time'),
			('operator', 'notes'),
		],
	}
}

# Defining common binnings to be shared between multiple configurations
COMMON_BINNINGS = {
	'iina_trim': 0.02,
	'iind_trim': 0.02,
	'iin_shunt': 0.02,
	'vin_vdd': 0.05,
	'slope_vdd': 0.25,
	'offset_vdd': 0.01,
	'offset_vin': 0.01,
	'slope_vin': 0.005,
	'k_vin': 10,
	'iref_trim_bit': 1,
	'iref': 0.02,
	'vdd_iref': 0.01,
	'drop_vin': 5,
	'chi2_iv': 0.1,
	'occupancy_scan': 5e-4,
	'thr_thr': 5,
	'noise_thr': 0.5,
}

# Defining common input shared between multiple configurations
COMMON_INPUTS = {
	'lanes': dict({f'l{l}_t{t}': f'lanes_test/lanes/{l}/try_{t}/success' for l in range(4) for t in range(3)}, **{
			'nt': 'lanes_test/n_tries',
			'lanes': 'lanes_test/lanes',
		}),
	'iv_shuntldo': {
		'REXT':      'shunt_iv_default_slope/rext',
		'IINA':      'shunt_iv_default_slope/IINA',
		'IIND':      'shunt_iv_default_slope/IIND',
		'VINA_SNS':  'shunt_iv_default_slope/VINA_SNS',
		'VIND_SNS':  'shunt_iv_default_slope/VIND_SNS',
		'VINA_PC':   'shunt_iv_default_slope/VINA_PC',
		'VIND_PC':   'shunt_iv_default_slope/VIND_PC',
		'VDDA':      'shunt_iv_default_slope/VDDA',
		'VDDD':      'shunt_iv_default_slope/VDDD',
		'VOFS':      'shunt_iv_default_slope/VOFS',
		'VOFS_LP':   'shunt_iv_default_slope/VOFS_LP',
		'GNDA_REF1': 'shunt_iv_default_slope/GNDA_REF1',
		'GNDA_REF2': 'shunt_iv_default_slope/GNDA_REF2',
		'GNDD_REF':  'shunt_iv_default_slope/GNDD_REF',
		'+erra':     13e-4,
		'+errd':     13e-4,
	},
	'iv_shuntldo_shorted': {
		'REXT':      'shunt_iv_default_slope_shorted/rext',
		'IINA':      'shunt_iv_default_slope_shorted/IINA',
		'IIND':      'shunt_iv_default_slope_shorted/IIND',
		'VINA_SNS':  'shunt_iv_default_slope_shorted/VINA_SNS',
		'VIND_SNS':  'shunt_iv_default_slope_shorted/VIND_SNS',
		'VDDA':      'shunt_iv_default_slope_shorted/VDDA',
		'VDDD':      'shunt_iv_default_slope_shorted/VDDD',
		'VOFS':      'shunt_iv_default_slope_shorted/VOFS',
		'VOFS_LP':   'shunt_iv_default_slope_shorted/VOFS_LP',
		'GNDA_REF1': 'shunt_iv_default_slope_shorted/GNDA_REF1',
		'GNDA_REF2': 'shunt_iv_default_slope_shorted/GNDA_REF2',
		'GNDD_REF':  'shunt_iv_default_slope_shorted/GNDD_REF',
		'+erra':     13e-4,
		'+errd':     13e-4,
	},
}

# Defining common outputs shared between multiple configurations
COMMON_OUTPUTS = {
	'vdd_trim': {
		'output': {
			'+fit': lambda i,o: fit_pol(i, sigma=0.001),
		},
		'output_sum': {
			'offset': lambda o: o['+fit'][0][0],
			'slope':  lambda o: o['+fit'][0][1],
			'chi2':   lambda o: o['+fit'][1],
		}
	},
	'iv_shuntldo': {
		'output': {
			'VINA':  lambda i: {iin: i['VINA_PC'][p]  for p, iin in enumerate(i['IINA'])},
			'VIND':  lambda i: {iin: i['VIND_PC'][p]  for p, iin in enumerate(i['IIND'])},
			'VDDA':  lambda i: {iin: i['VDDA'][p] for p, iin in enumerate(i['IINA'])},
			'VDDD':  lambda i: {iin: i['VDDD'][p] for p, iin in enumerate(i['IIND'])},
			'VOFS':  lambda i: {iin: i['VOFS'][p] for p, iin in enumerate((i['IINA'] + i['IIND'])*0.5)},
			'+REXT': lambda i, o: i['REXT'],
			'+fita': lambda i, o: fit_pol({k: v for p, (k, v) in enumerate(o['VINA'].items()) if p < 6},
			                               sigma=i['+erra'], fit_range=((0.84, 2.1), (0.5, 2.0))),
			'+fitd': lambda i, o: fit_pol({k: v for p, (k, v) in enumerate(o['VIND'].items()) if p < 6},
			                               sigma=i['+errd'], fit_range=((0.84, 2.1), (0.5, 2.0))),
		},
		'output_saturation': {
			'VINA':  lambda i: {iin: i['VINA_SNS'][p]  for p, iin in enumerate(i['IINA'])},
			'VIND':  lambda i: {iin: i['VIND_SNS'][p]  for p, iin in enumerate(i['IIND'])},
			'+fita': lambda i, o: fit_pol({k: v for p, (k, v) in enumerate(o['VINA'].items()) if p >= len(o['VINA'])-2},
			                               sigma=i['+erra']),
			'+fitd': lambda i, o: fit_pol({k: v for p, (k, v) in enumerate(o['VIND'].items()) if p >= len(o['VIND'])-2},
			                               sigma=i['+errd']),
		},
		'output_sum': {
			'VINA_offset': lambda o: o['+fita'][0][0],
			'VINA_slope':  lambda o: o['+fita'][0][1],
			'VINA_chi2':   lambda o: o['+fita'][1],
			'VIND_offset': lambda o: o['+fitd'][0][0],
			'VIND_slope':  lambda o: o['+fitd'][0][1],
			'VIND_chi2':   lambda o: o['+fitd'][1],
			'erra':        lambda i, o: i['+erra'],
			'errd':        lambda i, o: i['+errd'],
		},
		'text': [
			'VINA:  O: {VINA_offset:.3f}  S: {VINA_slope:.3f}  $\chi^2$: {VINA_chi2:.3f}  $\Delta$: {erra:.1e}',
			'VIND:  O: {VIND_offset:.3f}  S: {VIND_slope:.3f}  $\chi^2$: {VIND_chi2:.3f}  $\Delta$: {erra:.1e}',
		],
	},
	'iv_shuntldo_shorted': {
		'output': {
			'VINA':     lambda i: {iin: i['VINA_SNS'][p]  for p, iin in enumerate(i['IINA'])},
			'VIND':     lambda i: {iin: i['VIND_SNS'][p]  for p, iin in enumerate(i['IIND'])},
			'VDDA':     lambda i: {iin: i['VDDA'][p] for p, iin in enumerate(i['IINA'])},
			'VDDD':     lambda i: {iin: i['VDDD'][p] for p, iin in enumerate(i['IIND'])},
			'VREF_OVP': lambda i: {iin: i['VREF_OVP'][p] for p, iin in enumerate(i['IIND'])},
			'VOFS':     lambda i: {iin: i['VOFS'][p] for p, iin in enumerate((i['IINA'] + i['IIND'])*0.5)},
			'+REXT':    lambda i, o: i['REXT'],
			'+fita':    lambda i, o: fit_pol({k: v for p, (k, v) in enumerate(o['VINA'].items())},
			                               sigma=i['+erra'], fit_range=((1.95, 3.05), (1.0, 3.0))),
			'+fitd':    lambda i, o: fit_pol({k: v for p, (k, v) in enumerate(o['VIND'].items())},
			                               sigma=i['+errd'], fit_range=((1.95, 3.05), (1.0, 3.0))),
		},
		'output_saturation': {
			'VINA':  lambda i: {iin: i['VINA_SNS'][p]  for p, iin in enumerate(i['IINA'])},
			'VIND':  lambda i: {iin: i['VIND_SNS'][p]  for p, iin in enumerate(i['IIND'])},
			'+fita': lambda i, o: fit_pol({k: v for p, (k, v) in enumerate(o['VINA_SNS'].items()) if p >= len(o['VINA_SNS'])-2},
			                               sigma=i['+erra']),
			'+fitd': lambda i, o: fit_pol({k: v for p, (k, v) in enumerate(o['VIND_SNS'].items()) if p >= len(o['VIND_SNS'])-2},
			                               sigma=i['+errd']),
		},
		'output_sum': {
			'VINA_offset': lambda o: o['+fita'][0][0],
			'VINA_slope':  lambda o: o['+fita'][0][1],
			'VINA_chi2':   lambda o: o['+fita'][1],
			'VIND_offset': lambda o: o['+fitd'][0][0],
			'VIND_slope':  lambda o: o['+fitd'][0][1],
			'VIND_chi2':   lambda o: o['+fitd'][1],
			'erra':        lambda i, o: i['+erra'],
			'errd':        lambda i, o: i['+errd'],
		},
		'text': [
			'VINA:  O: {VINA_offset:.3f}  S: {VINA_slope:.3f}  $\chi^2$: {VINA_chi2:.3f}  $\Delta$: {erra:.1e}',
			'VIND:  O: {VIND_offset:.3f}  S: {VIND_slope:.3f}  $\chi^2$: {VIND_chi2:.3f}  $\Delta$: {erra:.1e}',
		],
	},
	'drop_iv_shuntldo': {
		'input': {
			'IINA':      'shunt_iv_default_slope/IINA',
			'IIND':      'shunt_iv_default_slope/IIND',
			'VIND_SNS':  'shunt_iv_default_slope/VIND_SNS',
			'VINA_SNS':  'shunt_iv_default_slope/VINA_SNS',
			'VIND_PC':   'shunt_iv_default_slope/VIND_PC',
			'VINA_PC':   'shunt_iv_default_slope/VINA_PC',
			'VOFS_LP':   'shunt_iv_default_slope/VOFS_LP',
			'GNDA_REF1': 'shunt_iv_default_slope/GNDA_REF1',
			'GNDA_REF2': 'shunt_iv_default_slope/GNDA_REF2',
			'GNDD_REF':  'shunt_iv_default_slope/GNDD_REF',

			'+err':     3,
			'+err_gnd': 0.3,
			},

		'output': {
			'VIND':   lambda i: {iin: 1e3*(i['VIND_PC'][p] - i['VIND_SNS'][p]) for p, iin in enumerate(i['IIND'])},
			'VINA':   lambda i: {iin: 1e3*(i['VINA_PC'][p] - i['VINA_SNS'][p]) for p, iin in enumerate(i['IINA'])},
			'GND':    lambda i: {iin: 1e3*i['VOFS_LP'][p] for p, iin in enumerate((i['IINA'] + i['IIND'])*0.5)},
			'GNDA1':  lambda i: {iin: 1e3*i['GNDA_REF1'][p] for p, iin in enumerate((i['IINA'] + i['IIND'])*0.5)},
			'GNDA2':  lambda i: {iin: 1e3*i['GNDA_REF2'][p] for p, iin in enumerate((i['IINA'] + i['IIND'])*0.5)},
			'GNDD':   lambda i: {iin: 1e3*i['GNDD_REF'][p] for p, iin in enumerate((i['IINA'] + i['IIND'])*0.5)},
		},
	},
	'vdd_trimming': {
		'input': {
			'+err': 0.001,
		},
		'output': {
			'vdd': 'vdd',
			'+fit': lambda i,o: fit_pol(i['vdd'], sigma=i['+err'])
		},
	},
	'adc_calibration': {
		'input': {
			'v':   'adc_calibration_ldo/volt',
			'a':   'adc_calibration_ldo/conv',
			've':  'adc_calibration_ldo/volt_err',
			'ae':  'adc_calibration_ldo/conv_err',
			'gnd': 'adc_calibration_ldo/gnd',
			'+err': 0.25e-3,
		},
		'output': {
			'adc':  lambda i: {a: i['v'][p] - i['gnd'] for p, a in enumerate(i['a'])},
			'+fit': lambda i, o: fit_pol(o['adc'], sigma=i['+err'])
		},
		'output_sum': {
			'offset': lambda o: o['+fit'][0][0]*1e3,
			'slope':  lambda o: o['+fit'][0][1]*1e3,
			'chi2':   lambda o: o['+fit'][1],
			'err':    lambda i,o: i['+err']*1e3,
		},
	},
	'iref_trim_fit': {
		'input': {
			'bits':       'iref_trim_ldo/data/trim_codes',
			'VOFS':       'iref_trim_ldo/data/VOFS',
			'IREF':       'iref_trim_ldo/data/IREF',
			'trim_value': 'iref_trim_ldo/trim_value',
			'+err': 5e-4,
		},
		'output': {
			'data': lambda i: {i['bits'][p]: i['VOFS'][p] for p in range(len(i['bits']))},
			'+fit': lambda i,o: fit_pol(o['data'], sigma=i['+err'])
		},
		'output_sum': {
			'offset': lambda o: o['+fit'][0][0],
			'slope':  lambda o: o['+fit'][0][1]*1e3,
			'chi2':   lambda o: o['+fit'][1],
			'err':    lambda i,o: i['+err']*1e3,
		},
	},
	'occup_scand': {
		'*arr_occup_scand': lambda i: occup_to_arr(i['h2']),
	},
	'tot_scand': {
		'*arr_tot_scand': lambda i: tot_to_arr(i['h1']),
	},
	'occup_scana': {
		'*arr_occup_scana': lambda i: occup_to_arr(i['h2']),
	},
	#'tot_scana': {
	#	'*arr_tot_scana': lambda i: tot_to_arr(i['h1']),
	#},
	'thr_coarse_thr': {
		'*arr_thr_coarse_thr': lambda i: th2_to_arr(i['h2']),
		'+fit': lambda i, o, binning=(1000/COMMON_BINNINGS['thr_thr'], 0, 1000): fit_gauss_root(arr_to_th1(o['*arr_thr_coarse_thr'], binning)),
	},
	'noise_coarse_thr': {
		'*arr_noise_coarse_thr': lambda i: th2_to_arr(i['h2']),
		'+fit': lambda i, o, binning=(100/COMMON_BINNINGS['noise_thr'], 0, 100): fit_gauss_root(arr_to_th1(o['*arr_noise_coarse_thr'], binning)),
	},
	'thr_fine_thr': {
		'*arr_thr_fine_thr': lambda i: th2_to_arr(i['h2']),
		'+fit': lambda i, o, binning=(1000/COMMON_BINNINGS['thr_thr'], 0, 1000): fit_gauss_root(arr_to_th1(o['*arr_thr_fine_thr'], binning)),
	},
	'noise_fine_thr': {
		'*arr_noise_fine_thr': lambda i: th2_to_arr(i['h2']),
		'+fit': lambda i, o, binning=(100/COMMON_BINNINGS['noise_thr'], 0, 100): fit_gauss_root(arr_to_th1(o['*arr_noise_fine_thr'], binning)),
	},
	'tdac_thr': {
		'*arr_tdac_thr': lambda i: th2_to_arr(i['h2']),
		'+fit': lambda i, o, binning=(32, 0, 32): fit_gauss_root(arr_to_th1(o['*arr_tdac_thr'], binning))
	},
}


# Defining common binnings to be shared between multiple configurations
COMMON_RANGES = {
	'vdd_def_iv_shuntldo': ((0.6, 1.4), (1.3, 2.0)),
	'vdd_alt_iv_shuntldo': ((0.2, 1.0), (1.3, 2.0)),
	'vin_def_iv_shuntldo': ((0.6, 1.5), (1.1, 1.6)),
	'vin_alt_iv_shuntldo': ((0.5, 1.2), (1.1, 1.6)),
	'iv_shuntldo': ((0.7, 2.5), (0.4, 2.2)),
	'iv_shuntldo_shorted': ((1.5, 4.1), (0.4, 2.2))
}

DAQ_MSG_IGNORE = [
	"Optical link tx slow control status",
	"Optical link rx slow control status",
	"Chip is neither an RD53A nor an RD53B",
	"ReadChipFuseID",
	"config test failed"
]


#########################################
# Collection of summary PER-WAFER plots #
#########################################

PLOTS_WAFER.update({

	#################
	# Operator veto #
	#################

	'operator_veto': {
		'input': 'operator_veto/value',
	},

	################
	# DAQ messages #
	################

	'daq_errors': {
		'input': {
			'file': '@FILE_DAQ.log:',
		},
		'aggregate.item': lambda o, ig=DAQ_MSG_IGNORE: count_matches_in_file(o['file'], '.*ERROR.*', ignore=ig)
	},

	'daq_warnings': {
		'input': {
			'file': '@FILE_DAQ.log:'
		},
		'aggregate.item': lambda o, ig=DAQ_MSG_IGNORE: count_matches_in_file(o['file'], '.*WARNING.*', ignore=ig)
	},
	

	###############
	# Startup LDO #
	###############

	'iina_ldo_start': {
		'input': 'power_startup_ldo/IINA',
	},

	'iind_ldo_start': {
		'input': 'power_startup_ldo/IIND',
	},

	'v_bgr_ldo_start': {
		'input': {
			'v':   'power_startup_ldo/R_IREF',
			'g':   'power_startup_ldo/GNDA_REF1',
			'ovp': 'power_startup_ldo/VREF_OVP',
		},
		'output': lambda i: i['v'],
		'db': {
			'VREF_OVP_V': lambda i,o: i['ovp'],
			'BANDGAP_V': lambda o: o
		}
	},

	'ana_r_ldo_start': {
		'input': 'power_startup_ldo/R_ANA',
		'db': {
			'R_LDO vector': lambda o: {-1: o}
		}
	},

	'dig_r_ldo_start': {
		'input': 'power_startup_ldo/R_DIG',
		'db': {
			'R_LDO vector': lambda o: {1: o}
		}
	},

	'gnd_r_ldo_start': {
		'input': 'power_startup_ldo/R_GND',
		'db': {
			'R_LDO vector': lambda o: {0: o}
		}

	},

	'gndd_r_ldo_start': {
		'input': 'power_startup_ldo/R_GND_DIG',
		'db': {
			'R_LDO vector': lambda o: {2: o}
		}
	},

	'gnda1_r_ldo_start': {
		'input': 'power_startup_ldo/R_GND_ANA',
		'db': {
			'R_LDO vector': lambda o: {-2: o}
		}
	},

	'gnda2_r_ldo_start': {
		'input': {
			'v': 'power_startup_ldo/GNDA_REF2',
			'i': 'power_startup_ldo/IINA',
		},
		'output': lambda i: i['v'] / i['i'],
		'db': {
			'R_LDO vector': lambda o: {-3: o}
		}
	},


	#####################
	# Iref trimming LDO #
	#####################

	'iina_ldo_trim': {
		'input': 'iref_trim_ldo/measurements/IINA',
		'db': 'IANA_DEFAULT_A'
	},

	'iind_ldo_trim': {
		'input': 'iref_trim_ldo/measurements/IIND',
		'db': 'IDIG_DEFAULT_A'
	},

	'iref_trim_ldo': {
		'input': 'iref_trim_ldo/trim_value',
		'bins.int': True,
		'db': 'IREF_TRIM_CODE'
	},

	'iref_ldo': {
		'input': COMMON_OUTPUTS['iref_trim_fit']['input'],
		'output': dict(COMMON_OUTPUTS['iref_trim_fit']['output'], **{
			'+x': lambda i, o: o['data'][i['trim_value']],
		}),
		'aggregate.item': lambda o: 1e3*o['+x'],
		'db': {
			'VOFS_V': lambda o: o/1e3,
			'IREF_A': lambda i,o: {int(i['bits'][p]): i['IREF'][p] for p in range(len(i['bits']))}[i['trim_value']],
			'VOFS vector': lambda i,o: {int(i['bits'][p]): i['VOFS'][p] for p in range(len(i['bits']))},
			'IREF vector': lambda i,o: {int(i['bits'][p]): i['IREF'][p] for p in range(len(i['bits']))}
		}
	},

	'chi2_iref_ldo': {
		'input': COMMON_OUTPUTS['iref_trim_fit']['input'],
		'output': COMMON_OUTPUTS['iref_trim_fit']['output'],
		'aggregate.item': lambda o: o['+fit'][1],
	},

	'offset_iref_ldo': {
		'input': COMMON_OUTPUTS['iref_trim_fit']['input'],
		'output': COMMON_OUTPUTS['iref_trim_fit']['output'],
		'aggregate.item': lambda o: 1e3*o['+fit'][0][0],
	},

	'slope_iref_ldo': {
		'input': COMMON_OUTPUTS['iref_trim_fit']['input'],
		'output': COMMON_OUTPUTS['iref_trim_fit']['output'],
		'aggregate.item': lambda o: 1e3*o['+fit'][0][1],
	},

	'vdda_iref_ldo': {
		'input': {
			'v': 'iref_trim_ldo/measurements/VDDA',
			#'ref': 'iref_trim_ldo/measurements/GNDA_REF2',
		},
		'output': lambda i: i['v'],
		'db': 'VDDA_DEFAULT_V',
	},

	'vddd_iref_ldo': {
		'input': {
			'v': 'iref_trim_ldo/measurements/VDDD',
			#'ref': 'iref_trim_ldo/measurements/GNDD_REF',
		},
		'output': lambda i: i['v'],
		'db': 'VDDD_DEFAULT_V',
	},

	'bit7_iref_ldo': {
		'input': COMMON_OUTPUTS['iref_trim_fit']['input'],
		'output': {
			'data': COMMON_OUTPUTS['iref_trim_fit']['output']['data'],
			'+v': lambda i, o: o['data'][7],
		},
		'aggregate.item': lambda o: 1e3*o['+v'],
	},

	'bit8_iref_ldo': {
		'input': COMMON_OUTPUTS['iref_trim_fit']['input'],
		'output': {
			'data': COMMON_OUTPUTS['iref_trim_fit']['output']['data'],
			'+v': lambda i, o: o['data'][8],
		},
		'aggregate.item': lambda o: 1e3*o['+v'],
	},

	'addressed_chip_id': {
		'input': {
			'tot': 'chip_id/tot_reads_per_reg',
			'val': 'chip_id/addressed_efficiency',
		},
		'output': {
			'out': lambda i: np.count_nonzero(i['val'] == i['tot']),
		},
		'bins.int': True,
	},

	'broadcast_chip_id': {
		'input': {
			'tot': 'chip_id/tot_reads_per_reg',
			'val': 'chip_id/broadcast_efficiency',
		},
		'output': {
			'out': lambda i: np.count_nonzero(i['val'] == (i['tot']*16)),
		},
		'bins.int': True,
	},

	'contamination_chip_id': {
		'input': {
			'val': 'chip_id/contamination',
		},
		'output': {
			'out': lambda i: np.count_nonzero(i['val'] == 0),
		},
		'bins.int': True,
	},


	#######################
	# Communication tests #
	#######################

	'fail_comm': {
		'input': 'communication_test_ldo/is_chip_responding',
		'output': lambda i: 1 - i,
		'bins.int': {0: 'YES', 1: 'NO'},
	},


	#####################
	# VDDD trimming LDO #
	#####################

	'vddd_trim_ldo': {
		'input': 'vdd_trim_ldo/VDDD/trim_value',
		'bins.int': True,
		'db': 'VDDD_TRIM_CODE'
	},

	'vddd_ldo': {
		'input': {
			'data': 'vdd_trim_ldo/VDDD/data',
			'trim_value': 'vdd_trim_ldo/VDDD/trim_value'
		},
		'output': lambda i: i['data'][i['trim_value']],
		'db': {
			'VDDD_TRIM_V': lambda o: o,
			'VDDD vector': lambda i,o: i['data'],
		}
	},

	'slope_vddd_ldo': {
		'input': dict(COMMON_OUTPUTS['vdd_trimming']['input'], **{
			'vdd': 'vdd_trim_ldo/VDDD/data',
		}),
		'output': COMMON_OUTPUTS['vdd_trimming']['output'],
		'aggregate.item': lambda o: 1e3*o['+fit'][0][1],
	},

	'offset_vddd_ldo': {
		'input': dict(COMMON_OUTPUTS['vdd_trimming']['input'], **{
			'vdd': 'vdd_trim_ldo/VDDD/data',
		}),
		'output': COMMON_OUTPUTS['vdd_trimming']['output'],
		'aggregate.item': lambda o: o['+fit'][0][0],
	},

	'iind_vddd_ldo': {
		'input': 'vdd_trim_ldo/measurements/IIND',
		'db': 'IDIG_CONFIG_A'
	},


	#####################
	# VDDA trimming LDO #
	#####################

	'vdda_ldo': {
		'input': {
			'data': 'vdd_trim_ldo/VDDA/data',
			'trim_value': 'vdd_trim_ldo/VDDA/trim_value'
		},
		'output': lambda i: i['data'][i['trim_value']],
		'db': {
			'VDDA_TRIM_V': lambda o: o,
			'VDDA vector': lambda i,o: i['data']
		}
	},

	'vdda_trim_ldo': {
		'input': 'vdd_trim_ldo/VDDA/trim_value',
		'bins.int': True,
		'db': 'VDDA_TRIM_CODE'
	},

	'slope_vdda_ldo': {
		'input': dict(COMMON_OUTPUTS['vdd_trimming']['input'], **{
			'vdd': 'vdd_trim_ldo/VDDA/data',
		}),
		'output': COMMON_OUTPUTS['vdd_trimming']['output'],
		'aggregate.item': lambda o: 1e3*o['+fit'][0][1],
	},

	'offset_vdda_ldo': {
		'input': dict(COMMON_OUTPUTS['vdd_trimming']['input'], **{
			'vdd': 'vdd_trim_ldo/VDDA/data',
		}),
		'output': COMMON_OUTPUTS['vdd_trimming']['output'],
		'aggregate.item': lambda o: o['+fit'][0][0],
	},

	'iina_vdda_ldo': {
		'input': 'vdd_trim_ldo/measurements/IINA',
		'db': 'IANA_CONFIG_A'
	},


	####################
	# VDD trimming LDO #
	####################

	'vofs_vdd_ldo': {
		'input': {
			'v': 'vdd_trim_ldo/measurements/VOFS',
			#'ref': 'vdd_trim_ldo/measurements/VOFS_LP',
		},
		'output': lambda i: i['v'],
	},

	'vref_vdd_ldo': {
		'input': {
			'v': 'vdd_trim_ldo/measurements/VREF_ADC',
			#'ref': 'vdd_trim_ldo/measurements/VOFS_LP',
		},
		'output': lambda i: i['v'],
		'db': 'VREF_ADC_V'
	},

	'pre_vdd_ldo': {
		'input': {
			'v': 'vdd_trim_ldo/measurements/VDD_PRE',
			#'ref': 'vdd_trim_ldo/measurements/VOFS_LP',
		},
		'output': lambda i: i['v'],
		'db': 'VREF_PRE_V'
	},


	####################
	# Startup ShuntLDO #
	####################

	'iina_shuntldo_start': {
		'input': 'power_startup_shunt/IINA',
	},

	'iind_shuntldo_start': {
		'input': 'power_startup_shunt/IIND',
	},

	'vina_shuntldo_start': {
		'input': 'power_startup_shunt/VINA_SNS',
		'db': 'VINA_START_V'
	},

	'vind_shuntldo_start': {
		'input': 'power_startup_shunt/VIND_SNS',
		'db': 'VIND_START_V'
	},

	'vref_ovp_shuntldo_start': {
		'input': {'v': 'power_startup_shunt/VREF_OVP'},
		'output': lambda i: i['v'],
	},

	# These values are only stored to DB, without producing plots
	'ana_r_shuntldo_start': {
		'input': 'power_startup_shunt/R_ANA',
		'db': {
			'R_SLDO vector': lambda o: {-1: o}
		}
	},

	'dig_r_shuntldo_start': {
		'input': 'power_startup_shunt/R_DIG',
		'db': {
			'R_SLDO vector': lambda o: {1: o}
		}
	},

	'gnd_r_shuntldo_start': {
		'input': 'power_startup_shunt/R_GND',
		'db': {
			'R_SLDO vector': lambda o: {0: o}
		}

	},

	'gndd_r_shuntldo_start': {
		'input': 'power_startup_shunt/R_GND_DIG',
		'db': {
			'R_SLDO vector': lambda o: {2: o}
		}
	},

	'gnda1_r_shuntldo_start': {
		'input': 'power_startup_shunt/R_GND_ANA',
		'db': {
			'R_SLDO vector': lambda o: {-2: o}
		}
	},

	'gnda2_r_shuntldo_start': {
		'input': {
			'v': 'power_startup_shunt/GNDA_REF2',
			'i': 'power_startup_shunt/IINA',
		},
		'output': lambda i: i['v'] / i['i'],
		'db': {
			'R_SLDO vector': lambda o: {-3: o}
		}
	},




	##########################
	# VDDD trimming ShuntLDO #
	##########################

	'vddd_trim_shuntldo': {
		'input': 'vdd_trim_shunt/VDDD/trim_value',
		'bins.int': True,
		'skip': True,
	},

	'vddd_shuntldo': {
		'input': {
			'data': 'vdd_trim_shunt/VDDD/data',
			'trim_value': 'vdd_trim_shunt/VDDD/trim_value'
		},
		'output': lambda i: i['data'][i['trim_value']],
		'skip': True,
	},

	'slope_vddd_shuntldo': {
		'input': 'vdd_trim_shunt/VDDD/data',
		'output': COMMON_OUTPUTS['vdd_trimming']['output'],
		'aggregate.item': lambda o: 1e3*o['+fit'][0][1],
		'skip': True,
	},

	'offset_vddd_shuntldo': {
		'input': 'vdd_trim_shunt/VDDD/data',
		'output': COMMON_OUTPUTS['vdd_trimming']['output'],
		'aggregate.item': lambda o: o['+fit'][0][0],
		'skip': True,
	},

	'vind_vddd_shuntldo': {
		'input': {
			'v': 'vdd_trim_shunt/measurements/VIND_SNS',
			#'o': 'vdd_trim_shunt/measurements/VOFS_LP',
		},
		'output': lambda i: i['v'],
		'skip': True,
	},


	##########################
	# VDDA trimming ShuntLDO #
	##########################

	'vdda_trim_shuntldo': {
		'input': 'vdd_trim_shunt/VDDA/trim_value',
		'bins.int': True,
		'skip': True,
	},

	'vdda_shuntldo': {
		'input': {
			'data': 'vdd_trim_shunt/VDDA/data',
			'trim_value': 'vdd_trim_shunt/VDDA/trim_value'
		},
		'output': lambda i: i['data'][i['trim_value']],
		'skip': True,
	},

	'slope_vdda_shuntldo': {
		'input': dict(COMMON_OUTPUTS['vdd_trimming']['input'], **{
			'vdd': 'vdd_trim_shunt/VDDA/data',
		}),
		'output': COMMON_OUTPUTS['vdd_trimming']['output'],
		'aggregate.item': lambda o: 1e3*o['+fit'][0][1],
		'skip': True,
	},

	'offset_vdda_shuntldo': {
		'input': dict(COMMON_OUTPUTS['vdd_trimming']['input'], **{
			'vdd': 'vdd_trim_shunt/VDDA/data',
		}),
		'output': COMMON_OUTPUTS['vdd_trimming']['output'],
		'aggregate.item': lambda o: o['+fit'][0][0],
		'skip': True,
	},

	'vina_vdda_shuntldo': {
		'input': {
			'v': 'vdd_trim_shunt/measurements/VINA_SNS',
			#'o': 'vdd_trim_shunt/measurements/VOFS_LP',
		},
		'output': lambda i: i['v'],
		'skip': True,
	},


	###############################
	# Trimming in LDO vs ShuntLDO #
	###############################

	'vdda_trim_diff': {
		'input': {
			'bit_l': 'vdd_trim_ldo/VDDA/trim_value',
			'bit_s': 'vdd_trim_shunt/VDDA/trim_value',
		},
		'output': lambda i: i['bit_l'] - i['bit_s'],
		'bins.int': True,
		'skip': True,
	},

	'vddd_trim_diff': {
		'input': {
			'bit_l': 'vdd_trim_ldo/VDDD/trim_value',
			'bit_s': 'vdd_trim_shunt/VDDD/trim_value',
		},
		'output': lambda i: i['bit_l'] - i['bit_s'],
		'bins.int': True,
		'skip': True,
	},

	#########################
	# Injection capacitance #
	#########################

	'capacitance_inj': {
		'input': 'inj_capacitance/capacitance',
		'db': {
			# Converting from fF to F
			'INJ_CAPACIT_F': lambda i,o: i*1e-12
		}
	},

	########################
	# Analog/Digital scans #
	########################

	'completed_scana': {
		'input': 'analog_scan_shunt/completed',
		'output': {
			'x': lambda i: int(i)
		},
		'bins.int': {0: 'NO', 1: 'YES'},
	},

	'completed_scand': {
		'input': 'digital_scan_shunt/completed',
		'output': {
			'x': lambda i: int(i)
		},
		'bins.int': {0: 'NO', 1: 'YES'},
	},

	'occup_enabled_scana': {
		'input': 'analog_scan_shunt/occupancy_enabled',
		'db': 'OCCUP_ANA'
	},

	'occup_enabled_scand': {
		'input': 'digital_scan_shunt/occupancy_enabled',
		'db': 'OCCUP_DIG'
	},

	'completed_scand_ldo': {
		'input': 'digital_scan_ldo/completed',
		'output': {
			'x': lambda i: int(i)
		},
		'bins.int': {0: 'NO', 1: 'YES'},
	},

	'occup_enabled_scand_ldo': {
		'input': 'digital_scan_ldo/occupancy_enabled',
		# 'db': 'OCCUP_DIG_LDO'  #FIXME: uncomment after implemented in the DB
	},

	'high_occup_scana': {
		'input': {
			'h2': '@FILE_AnalogScan_shunt.root:Chip*/Occupancy Map;2',
		},
		'output': COMMON_OUTPUTS['occup_scana'],
		'aggregate.item': lambda o: np.count_nonzero(o['*arr_occup_scana'] > 1.0),
	},

	'high_occup_scand': {
		'input': {
			'h2': '@FILE_DigitalScan_shunt.root:Chip*/Occupancy Map;2', #problem here
		},
		'output': COMMON_OUTPUTS['occup_scand'],
		'aggregate.item': lambda o: np.count_nonzero(o['*arr_occup_scand'] > 1.0),
	},

	'high_occup_scand_ldo': {
		'input': {
			'h2': '@FILE_DigitalScan_ldo.root:Chip*/Occupancy Map;2', #problem here
		},
		'output': COMMON_OUTPUTS['occup_scand'],
		'aggregate.item': lambda o: np.count_nonzero(o['*arr_occup_scand'] > 1.0),
	},

	'low_occup_scana': {
		'input': {
			'h2': '@FILE_AnalogScan_shunt.root:Chip*/Occupancy Map;2',
		},
		'output': COMMON_OUTPUTS['occup_scana'],
		'aggregate.item': lambda o: np.count_nonzero(o['*arr_occup_scana'] < 1.0),
	},

	'low_occup_scand': {
		'input': {
			'h2': '@FILE_DigitalScan_shunt.root:Chip*/Occupancy Map;2',
		},
		'output': COMMON_OUTPUTS['occup_scand'],
		'aggregate.item': lambda o: np.count_nonzero(o['*arr_occup_scand'] < 1.0),
	},

	'low_occup_scand_ldo': {
		'input': {
			'h2': '@FILE_DigitalScan_ldo.root:Chip*/Occupancy Map;2',
		},
		'output': COMMON_OUTPUTS['occup_scand'],
		'aggregate.item': lambda o: np.count_nonzero(o['*arr_occup_scand'] < 1.0),
	},

	#'tot_scana': {
	#	'input': {
	#		'h1': '@FILE_AnalogScan.root:Chip*/ToT Distribution;2',
	#	},
	#	'output': COMMON_OUTPUTS['tot_scana'],
	#	'aggregate.item': lambda o: np.count_nonzero(o['*arr_tot_scana'] > 10),
	#},

	'high_tot_scand': {
		'input': 'digital_scan_shunt/n_pixel_bad_tot'
	},

	'high_tot_scand_ldo': {
		'input': 'digital_scan_ldo/n_pixel_bad_tot'
	},

	##########
	# EFUSES #
	##########

	'efuses': {
		'input': {
			'succ':  'efuses/success',
			'value': 'efuses/value_after_prog',
		},
		'output': lambda i: int(i['succ']),
		'bins.int': {0: 'NO', 1: 'YES'},
		'db': {
			'EFUSE_CODE': lambda i,o: i['value']
		}
	},


	##############################
	# Analog/Digital pixel power #
	##############################

	'ua_ia_power_pix': {
		'input': 'pixel_power/I_pix_ana_ua',
	},

	'ua_id_power_pix': {
		'input': 'pixel_power/I_pix_dig_ua',
	},

	'default_ia_power_pix': {
		'input': 'pixel_power/I_ana_default',
	},

	'default_id_power_pix': {
		'input': 'pixel_power/I_dig_default',
	},

	'low_ia_power_pix': {
		'input': 'pixel_power/I_ana_low',
		'output': lambda i: i*1e3,
		'db': {
			'IANA_PERIFERY_A': lambda i,o: i
		}
	},

	'low_id_power_pix': {
		'input': 'pixel_power/I_dig_low',
		'output': lambda i: i*1e3,
		'db': {
			'IDIG_PERIFERY_A': lambda i,o: i
		}
	},


	###################
	# Pixel registers #
	###################

	'pixel_registers': {
		'input': 'pixel_registers_test_(ldo|shunt)/n_failed_regs',
	},

	'pixel_registers_disable': {
		'input': {
			'file': '@FILE_RegTestWLT_pixel.csv:',
		},
		# Considering only bits 7 and 15 (counting from the left)
		'aggregate.item': lambda o: count_pixel_register_diffs(o['file'], bitmask=0x0101)
	},

	'pixel_disable': {
		'input': 'pixel_disable/n_disabled_high_occupancy'
	},



	'global_registers': {
		'input': 'global_registers_test_ldo/n_failed_regs',
		'bins.int': True,
	},


	###################
	# ADC Calibration #
	###################

	'offset_adc_calibration': {
		'input': COMMON_OUTPUTS['adc_calibration']['input'],
		'output': COMMON_OUTPUTS['adc_calibration']['output'],
		'aggregate.item': lambda o: o['+fit'][0][0]*1e3,
		'db': {
			'ADC_OFF_V': lambda o: o/1e3
		}
	},

	'slope_adc_calibration': {
		'input': COMMON_OUTPUTS['adc_calibration']['input'],
		'output': COMMON_OUTPUTS['adc_calibration']['output'],
		'aggregate.item': lambda o: o['+fit'][0][1]*1e3,
		'db': {
			'ADC_SLO': lambda o: o/1e3
		}
	},

	'chi2_adc_calibration': {
		'input': COMMON_OUTPUTS['adc_calibration']['input'],
		'output': COMMON_OUTPUTS['adc_calibration']['output'],
		'aggregate.item': lambda o: o['+fit'][1],
	},

	################
	# Data merging #
	################

	'data_merging': {
		'input': 'data_merging/success',
		'output': {
			'x': lambda i: int(i)
		},
		'bins.int': {0: 'NO', 1: 'YES'},
	},

	###################
	# Scan chain test #
	###################

	'scan_chain': {
		'input': 'scan_chain/success',
		'output': {
			'x': lambda i: int(i)
		},
		'bins.int': {0: 'NO', 1: 'YES'},
	},

	################
	# I-V ShuntLDO #
	################


	'slope_vina_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output'],
		'aggregate.item': lambda o: o['+fita'][0][1],
	},

	'offset_vina_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output'],
		'aggregate.item': lambda o: o['+fita'][0][0],
		'db': 'IV_ANA_OFF_V',
	},

	'chi2_vina_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output'],
		'aggregate.item': lambda o: o['+fita'][1],
	},

	'k_vina_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output'],
		'aggregate.item': lambda o: o['+REXT'] / o['+fita'][0][1] if o['+fita'][1] > 0 else 0.0,
		'db': 'IV_ANA_K',
	},

	'drop_vina_shuntldo': {
		'input': COMMON_OUTPUTS['drop_iv_shuntldo']['input'],
		'output': dict(COMMON_OUTPUTS['drop_iv_shuntldo']['output'], **{
			'+fit': lambda i, o: fit_pol(o['VINA'], sigma=i['+err'])
		}),
		'aggregate.item': lambda o: o['+fit'][0][1],
	},

	'slope_sat_vina_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output_saturation'],
		'aggregate.item': lambda o: o['+fita'][0][1],
	},

	'max_sat_vina_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output_saturation'],
		'aggregate.item': lambda o: max(o['VINA'].values()),
	},

	'ovp_sat_vina_shuntldo': {
		'input': dict(COMMON_INPUTS['iv_shuntldo'], **{
			'ovp': 'power_startup_shunt/VREF_OVP'
		}),
		'output': dict(COMMON_OUTPUTS['iv_shuntldo']['output_saturation'], **{
			'ovp': lambda i: i['ovp']
		}),
		'aggregate.item': lambda o: max(o['VINA'].values()) / o['ovp'],
	},

	'slope_vind_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output'],
		'aggregate.item': lambda o: o['+fitd'][0][1],
	},

	'offset_vind_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output'],
		'aggregate.item': lambda o: o['+fitd'][0][0],
		'db': 'IV_DIG_OFF_V',
	},

	'chi2_vind_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output'],
		'aggregate.item': lambda o: o['+fitd'][1],
	},

	'k_vind_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output'],
		'aggregate.item': lambda o: o['+REXT'] / o['+fitd'][0][1] if o['+fitd'][1] > 0 else 0.0,
		'db': 'IV_DIG_K',
	},

	'drop_vind_shuntldo': {
		'input': COMMON_OUTPUTS['drop_iv_shuntldo']['input'],
		'output': dict(COMMON_OUTPUTS['drop_iv_shuntldo']['output'], **{
			'+fit': lambda i, o: fit_pol(o['VIND'], sigma=i['+err'])
		}),
		'aggregate.item': lambda o: o['+fit'][0][1],
	},

	'slope_sat_vind_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output_saturation'],
		'aggregate.item': lambda o: o['+fitd'][0][1],
	},

	'max_sat_vind_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output_saturation'],
		'aggregate.item': lambda o: max(o['VIND'].values()),
	},

	'ovp_sat_vind_shuntldo': {
		'input': dict(COMMON_INPUTS['iv_shuntldo'], **{
			'ovp': 'power_startup_shunt/VREF_OVP'
		}),
		'output': dict(COMMON_OUTPUTS['iv_shuntldo']['output_saturation'], **{
			'ovp': lambda i: i['ovp']
		}),
		'aggregate.item': lambda o: max(o['VIND'].values()) / o['ovp'],
	},

	# 'drop_gnd_shuntldo': {
	# 	'input': COMMON_OUTPUTS['drop_iv_shuntldo']['input'],
	# 	'output': dict(COMMON_OUTPUTS['drop_iv_shuntldo']['output'], **{
	# 		'+fit': lambda i, o: fit_pol(o[f'GND'], sigma=i['+err'])
	# 	}),
	# 	'aggregate.item': lambda o: o['+fit'][0][1],
	# },

	'drop_gnda1_shuntldo': {
		'input': COMMON_OUTPUTS['drop_iv_shuntldo']['input'],
		'output': dict(COMMON_OUTPUTS['drop_iv_shuntldo']['output'], **{
			'+fit': lambda i, o: fit_pol(o['GNDA1'], sigma=i['+err'])
		}),
		'aggregate.item': lambda o: o['+fit'][0][1],
	},

	'drop_gnda2_shuntldo': {
		'input': COMMON_OUTPUTS['drop_iv_shuntldo']['input'],
		'output': dict(COMMON_OUTPUTS['drop_iv_shuntldo']['output'], **{
			'+fit': lambda i, o: fit_pol(o['GNDA2'], sigma=i['+err'])
		}),
		'aggregate.item': lambda o: o['+fit'][0][1],
	},

	'drop_gndd_shuntldo': {
		'input': COMMON_OUTPUTS['drop_iv_shuntldo']['input'],
		'output': dict(COMMON_OUTPUTS['drop_iv_shuntldo']['output'], **{
			'+fit': lambda i, o: fit_pol(o['GNDD'], sigma=i['+err'])
		}),
		'aggregate.item': lambda o: o['+fit'][0][1],
	},

	'iina_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output'],
		'aggregate.item': lambda o: np.polynomial.Polynomial([-o['+fita'][0][1], 1.0/o['+fita'][0][0]])(1.55)
	},

	'iind_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output'],
		'aggregate.item': lambda o: np.polynomial.Polynomial([-o['+fitd'][0][1], 1.0/o['+fitd'][0][0]])(1.55)
	},

	'iindiff_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output'],
		'aggregate.item': lambda o: np.polynomial.Polynomial([-o['+fita'][0][1], 1.0/o['+fita'][0][0]])(1.55) - np.polynomial.Polynomial([-o['+fitd'][0][1], 1.0/o['+fitd'][0][0]])(1.55)
	},

	'max_ovp_vina_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo_shorted'],
		'output': COMMON_OUTPUTS['iv_shuntldo_shorted']['output'],
		'aggregate.item': lambda o: max(o['VINA'].values()),
	},

	'max_ovp_vind_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo_shorted'],
		'output': COMMON_OUTPUTS['iv_shuntldo_shorted']['output'],
		'aggregate.item': lambda o: max(o['VIND'].values()),
	},

	'min_ovp_vina_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo_shorted'],
		'output': COMMON_OUTPUTS['iv_shuntldo_shorted']['output'],
		'aggregate.item': lambda o: min(o['VINA'].values()),
	},

	'min_ovp_vind_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo_shorted'],
		'output': COMMON_OUTPUTS['iv_shuntldo_shorted']['output'],
		'aggregate.item': lambda o: min(o['VIND'].values()),
	},

	'slope_ovp_vina_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo_shorted'],
		'output': COMMON_OUTPUTS['iv_shuntldo_shorted']['output'],
		'aggregate.item': lambda o: o['+fita'][0][1],
	},

	'slope_ovp_vind_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo_shorted'],
		'output': COMMON_OUTPUTS['iv_shuntldo_shorted']['output'],
		'aggregate.item': lambda o: o['+fitd'][0][1],
	},

	'offset_ovp_vina_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo_shorted'],
		'output': COMMON_OUTPUTS['iv_shuntldo_shorted']['output'],
		'aggregate.item': lambda o: o['+fita'][0][0],
	},

	'offset_ovp_vind_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo_shorted'],
		'output': COMMON_OUTPUTS['iv_shuntldo_shorted']['output'],
		'aggregate.item': lambda o: o['+fitd'][0][0],
	},

})

#############################
# Threshold scan [ShuntLDO] #
#############################

N_PIXELS = 145152
for granularity in ['coarse', 'fine']:
	binning_thr = (200, 0, 1000)
	binning_noise = (200, 0, 100)
	db_names = {'coarse': 'UNTUNED', 'fine': 'TUNED'}
	################################# WAFER-WISE PLOTS
	PLOTS_WAFER.update({

		f'completed_{granularity}_thr': {
			'input': f'threshold_scan_{granularity}_shunt/completed',
			'output': {
				'x': lambda i: int(i)
			},
			'bins.int': {0: 'NO', 1: 'YES'},
		},

		f'stuck_pixels_{granularity}_thr': {
			'input': f'threshold_scan_{granularity}_shunt/stuck_pixels',
			'db': {
				'PIX_STUCK': lambda o: o/N_PIXELS
			} if granularity == 'fine' else None
		},

		f'mean_thr_{granularity}_thr': {
			'input': {
				'h2': f'@FILE_ThresholdScan_{granularity}.root:Chip*/Threshold & Noise (probit model);1/Threshold Map (probit model);2',
			},
			'output': COMMON_OUTPUTS[f'thr_{granularity}_thr'],
			'aggregate.item': lambda o: o['+fit']['mean'],
			'db': 'THRES_MEAN' if granularity == 'coarse' else None
		},

		f'std_thr_{granularity}_thr': {
			'input': {
				'h2': f'@FILE_ThresholdScan_{granularity}.root:Chip*/Threshold & Noise (probit model);1/Threshold Map (probit model);2',
			},
			'output': COMMON_OUTPUTS[f'thr_{granularity}_thr'],
			'aggregate.item': lambda o: o['+fit']['sigma'],
			'db': f'THRES_RMS_{db_names[granularity].upper()}'
		},

		f'mean_noise_{granularity}_thr': {
			'input': {
				'h2': f'@FILE_ThresholdScan_{granularity}.root:Chip*/Threshold & Noise (probit model);1/Noise Map (probit model);2',
			},
			'output': COMMON_OUTPUTS[f'noise_{granularity}_thr'],
			'aggregate.item': lambda o: o['+fit']['mean'],
			'db': 'NOISE_MEAN' if granularity == 'fine' else None
		},

		f'std_noise_{granularity}_thr': {
			'input': {
				'h2': f'@FILE_ThresholdScan_{granularity}.root:Chip*/Threshold & Noise (probit model);1/Noise Map (probit model);2',
			},
			'output': COMMON_OUTPUTS[f'noise_{granularity}_thr'],
			'aggregate.item': lambda o: o['+fit']['sigma'],
			'db': 'NOISE_RMS' if granularity == 'fine' else None
		},
	})

	################################# CHIP-WISE PLOTS
	PLOTS_CHIP.update({
		f'thr_{granularity}_thr': {
			'input': {
				'h2': f'@FILE_ThresholdScan_{granularity}.root:Chip*/Threshold Map (fitting);2',
			},
			'output': COMMON_OUTPUTS[f'thr_{granularity}_thr'],
			'output_sum': {
				'mean': lambda o: o['+fit']['mean'],
				'sigma': lambda o: o['+fit']['sigma'],
			},
			'text': [
				'mean: {mean:.2f}  $\sigma$: {sigma:.2f}',
			],
			'fit.draw': {'+fit': 4},
			'title': f'Threshold scan [{granularity}] [{{chip_column:X}}{{chip_row:X}}];Threshold [$\Delta$VCAL];Pixels',
			'bins': COMMON_BINNINGS['thr_thr'],
			'axis.range': ((0, 900), (1e-1, 1e6)),
			'logY': True,
		},

		f'noise_{granularity}_thr': {
			'input': {
				'h2': f'@FILE_ThresholdScan_{granularity}.root:Chip*/Noise Map (fitting);2',
			},
			'output': COMMON_OUTPUTS[f'noise_{granularity}_thr'],
			'output_sum': {
				'mean': lambda o: o['+fit']['mean'],
				'sigma': lambda o: o['+fit']['sigma'],
			},
			'text': [
				'mean: {mean:.2f}  $\sigma$: {sigma:.2f}',
			],
			'fit.draw': {'+fit': 4},
			'title': f'Threshold scan [{granularity}] [{{chip_column:X}}{{chip_row:X}}];Noise [$\Delta$VCAL];Pixels',
			'bins': COMMON_BINNINGS['noise_thr'],
			'axis.range': ((0, 100), (1e-1, 1e5)),
			'logY': True,
		},
	})

# Fractions of pixels with high/Low threshold/noise
PLOTS_WAFER.update({
	'high_thr_fine_thr': {
		'input': {
			#'h2': '@FILE_ThresholdScan_fine.root:Chip*/Threshold Map (fitting);2',
			'h2': '@FILE_ThresholdScan_fine.root:Chip*/Threshold & Noise (probit model);1/Threshold Map (probit model);2',
		},
		'output': COMMON_OUTPUTS['thr_fine_thr'],
		'aggregate.item': lambda o: np.count_nonzero(o['*arr_thr_fine_thr'] > o['+fit']['mean'] + 40),
		'db': {
			'THRES_HIGH': lambda o: o/N_PIXELS
		}
	},

	'low_thr_fine_thr': {
		'input': {
			#'h2': '@FILE_ThresholdScan_fine.root:Chip*/Threshold Map (fitting);2',
			'h2': '@FILE_ThresholdScan_fine.root:Chip*/Threshold & Noise (probit model);1/Threshold Map (probit model);2',
		},
		'output': COMMON_OUTPUTS['thr_fine_thr'],
		'aggregate.item': lambda o: np.count_nonzero((o['*arr_thr_fine_thr'] < o['+fit']['mean'] - 40) & (o['*arr_thr_fine_thr'] != 0)),
		'db': {
			'THRES_LOW': lambda o: o/N_PIXELS
		}
	},

	'high_noise_fine_thr': {
		'input': {
			#'h2': '@FILE_ThresholdScan_fine.root:Chip*/Noise Map (fitting);2',
			'h2': '@FILE_ThresholdScan_fine.root:Chip*/Threshold & Noise (probit model);1/Noise Map (probit model);2',
		},
		'output': COMMON_OUTPUTS['noise_fine_thr'],
		'aggregate.item': lambda o: np.count_nonzero(o['*arr_noise_fine_thr'] > 30),
	}
})

# TDAC distributions
PLOTS_WAFER.update({

	f'completed_tuning': {
		'input': f'threshold_trimming/completed',
		'output': {
			'x': lambda i: int(i)
		},
		'bins.int': {0: 'NO', 1: 'YES'},
	},

	'mean_tdac_thr': {
		'input': {
			'h1': '@FILE_ThresholdEqualization.root:Chip*/TDAC Distribution;2',
			'h2': '@FILE_ThresholdEqualization.root:Chip*/TDAC Map;2',
		},
		'output': COMMON_OUTPUTS['tdac_thr'],
		'aggregate.item': lambda o: o['+fit']['mean'],
	},

	'std_tdac_thr': {
		'input': {
			'h1': '@FILE_ThresholdEqualization.root:Chip*/TDAC Distribution;2',
			'h2': '@FILE_ThresholdEqualization.root:Chip*/TDAC Map;2',
		},
		'output': COMMON_OUTPUTS['tdac_thr'],
		'aggregate.item': lambda o: o['+fit']['sigma'],
	},

	'low_tdac_thr': {
		'input': {
			'h1': '@FILE_ThresholdEqualization.root:Chip*/TDAC Distribution;2',
		},
		'output': lambda i: i['h1'].GetBinContent(1)*100,
		'db': {
			'TDAC_LOW': lambda o: o/100.0
		}
	},

	'high_tdac_thr': {
		'input': {
			'h1': '@FILE_ThresholdEqualization.root:Chip*/TDAC Distribution;2',
		},
		'output': lambda i: i['h1'].GetBinContent(32)*100,
		'db': {
			'TDAC_HIGH': lambda o: o/100.0
		}
	},
})

PLOTS_CHIP.update({
	'tdac_thr': {
		'input': {
			'h2': '@FILE_ThresholdEqualization.root:Chip*/TDAC Map;2',
		},
		'output': COMMON_OUTPUTS['tdac_thr'],
		'output_sum': {
			'mean': lambda o: o['+fit']['mean'],
			'sigma': lambda o: o['+fit']['sigma'],
		},
		'text': [
			'mean: {mean:.2f}  $\sigma$: {sigma:.2f}',
		],
		'fit.draw': {'+fit': 4},
		'title': 'Threshold tuning [TDAC] [{chip_column:X}{chip_row:X}];TDAC code;Pixels',
		'bins': 1,
		'bins.int': True,
		'axis.range': ((0, 32), None),
	}
})




#########################################
# Collection of detailed PER-CHIP plots #
#########################################

PLOTS_CHIP.update({
	############################################## ADC Calibration
	'adc_calibration': {
		'input': COMMON_OUTPUTS['adc_calibration']['input'],
		'output': COMMON_OUTPUTS['adc_calibration']['output'],
		'output_sum': COMMON_OUTPUTS['adc_calibration']['output_sum'],
		'title': 'ADC calibration [{chip_column:X}{chip_row:X}];ADC code;Voltage [V]',
		'text': ['O: {offset:.1f} mV', 'S: {slope:.2f} mV/ADC', '$\chi^2$: {chi2:.2f}', '$\Delta$: {err:.2f} mV'],
		'fit.draw': True,
		'axis.range': ((0, 4100), (0, 1)),
		'group': 8,
		'db': {
			'ADC vector': lambda o: o['adc']
		}
	},

	############################################## IRef Trimming
	'iref_vs_trim_bit_ldo': {
		'input': COMMON_OUTPUTS['iref_trim_fit']['input'],
		'output': COMMON_OUTPUTS['iref_trim_fit']['output'],
		'output_sum': COMMON_OUTPUTS['iref_trim_fit']['output_sum'],
		'fit.draw': True,
		'text': ['O: {offset:.2f} V', 'S: {slope:.1f} mV/bit',
		         '$\chi^2$: {chi2:.2f}', '$\Delta$: {err:.2f} mV'],
		'text.loc': 'lower right',
		'title': 'IREF trimming curve [{chip_column:X}{chip_row:X}];Trim code;Voltage [V]',
		'axis.range': ((-1, 16), (0.35, 0.65)),
		'group': 2,
	},


	############################################## VDD Trimming
	'vdda_vs_trim_bit_ldo': {
		'input': dict(COMMON_OUTPUTS['vdd_trimming']['input'], **{
			'vdd': 'vdd_trim_ldo/VDDA/data',
		}),
		'output': COMMON_OUTPUTS['vdd_trimming']['output'],
		'output_sum': {
			'offset': lambda o: o['+fit'][0][0],
			'slope':  lambda o: o['+fit'][0][1],
			'chi2':   lambda o: o['+fit'][1],
			'err':    lambda i,o: i['+err'],
		},
		'fit.draw': True,
		'text': ['O: {offset:.3f} V', 'S: {slope:.3f} V/bit',
		         '$\chi^2$: {chi2:.2f}', '$\Delta$: {err:.1e} V'],
		'text.loc': 'lower right',
		'title': 'VDDA trimming curve [{chip_column:X}{chip_row:X}];Trim code;VDDA [V]',
		'axis.range': ((-1, 16), (0.85, 1.35)),
		'group': 5,
	},

	'vddd_vs_trim_bit_ldo': {
		'input': dict(COMMON_OUTPUTS['vdd_trimming']['input'], **{
			'vdd': 'vdd_trim_ldo/VDDD/data',
		}),
		'output': COMMON_OUTPUTS['vdd_trimming']['output'],
		'output_sum': {
			'offset': lambda o: o['+fit'][0][0],
			'slope':  lambda o: o['+fit'][0][1],
			'chi2':   lambda o: o['+fit'][1],
			'err':    lambda i,o: i['+err'],
		},
		'fit.draw': True,
		'text': ['O: {offset:.3f} V', 'S: {slope:.3f} V/bit',
		         '$\chi^2$: {chi2:.2f}', '$\Delta$: {err:.1e} V'],
		'text.loc': 'lower right',
		'title': 'VDDD trimming curve [{chip_column:X}{chip_row:X}];Trim code;VDDD [V]',
		'axis.range': ((-1, 16), (0.85, 1.35)),
		'group': 5,
	},

	'drop_iv_shuntldo': {
		'input': COMMON_OUTPUTS['drop_iv_shuntldo']['input'],
		'output': {
			key: value for dic in [COMMON_OUTPUTS['drop_iv_shuntldo']['output']] + [
				{f'+fit_{name}':  lambda i, o, n=name: fit_pol(o[n],
				                                               sigma=i['+err_gnd'] if 'GND' in name else '+err')
				} for name in [
					'VIND', 'VINA', 'GND', 'GNDD', 'GNDA1', 'GNDA2',
			]] for key, value in dic.items()
		},
		'output_sum': {
			key: value for dic in [{
				f'o_{name}': lambda o, n=name: o[f'+fit_{n}'][0][0],
				f's_{name}':  lambda o, n=name: o[f'+fit_{n}'][0][1],
				f'c_{name}':   lambda o, n=name: o[f'+fit_{n}'][1],
			} for name in ['VIND', 'VINA', 'GND', 'GNDD', 'GNDA1', 'GNDA2']] for key, value in dic.items()
		},
		'text': [
			f'{name.replace("_", " ")}:  O: {{o_{name}:.3f}}  S: {{s_{name}:.3f}}  $\chi^2$: {{c_{name}:.3f}}' for name in [
				'VIND', 'VINA', 'GND', 'GNDD', 'GNDA1', 'GNDA2',
			]
		],
		'fit.draw': {'+fit_VIND':  0, '+fit_VINA':  1, '+fit_GND':   2, '+fit_GNDD':   3, '+fit_GNDA1':   4, '+fit_GNDA2':   0},
		'title': 'Voltage drop (SLDO) [{chip_column:X}{chip_row:X}];Current [A];Voltage [mV]',
		'axis.range': ((0.8, 2.2), (0.0, 120)),
		'legend': None,
		'legend.loc': 'lower right',
		'group': 16,
	},

	################
	# I-V ShuntLDO #
	################

	'iv_shuntldo': {
		'input': COMMON_INPUTS['iv_shuntldo'],
		'output': COMMON_OUTPUTS['iv_shuntldo']['output'],
		'output_sum': COMMON_OUTPUTS['iv_shuntldo']['output_sum'],
		'text': COMMON_OUTPUTS['iv_shuntldo']['text'],
		'fit.draw': {'+fita': 0, '+fitd': 1},
		'title': 'IV (SLDO) [{chip_column:X}{chip_row:X}];Input current [A];Voltage [V]',
		'axis.range': COMMON_RANGES['iv_shuntldo'],
		'legend': None,
		'legend.loc': 'lower right',
		'group': 16,
		'db': {
			'IV_ANA vector':  lambda o: o['VINA'],
			'IV_DIG vector': lambda o: o['VIND']
		}
	},

	'iv_shuntldo_shorted': {
		'input': COMMON_INPUTS['iv_shuntldo_shorted'],
		'output': COMMON_OUTPUTS['iv_shuntldo_shorted']['output'],
		'output_sum': COMMON_OUTPUTS['iv_shuntldo_shorted']['output_sum'],
		'text': COMMON_OUTPUTS['iv_shuntldo_shorted']['text'],
		'fit.draw': {'+fita': 0, '+fitd': 1},
		'title': 'IV (SLDO) (shorted VINA/D) [{chip_column:X}{chip_row:X}];Input current [A];Voltage [V]',
		'axis.range': COMMON_RANGES['iv_shuntldo_shorted'],
		'legend': None,
		'legend.loc': 'lower right',
		'group': 17,
		'db': {
			'IV_ANA_HIGH vector':  lambda o: o['VINA'],
			'IV_DIG_HIGH vector': lambda o: o['VIND']
		}
	},

	'sat_iv_shuntldo': {
		'input': dict(COMMON_INPUTS['iv_shuntldo'], **{
			'+erra': 2e-4,
			'+errd': 2e-4,
		}),
		'output': COMMON_OUTPUTS['iv_shuntldo']['output_saturation'],
		'output_sum': COMMON_OUTPUTS['iv_shuntldo']['output_sum'],
		'text': COMMON_OUTPUTS['iv_shuntldo']['text'],
		'fit.draw': {'+fita': 0, '+fitd': 1},
		'title': 'IV saturation (SLDO) [{chip_column:X}{chip_row:X}];Input current [A];Voltage [V]',
		'axis.range': (COMMON_RANGES['iv_shuntldo'][0], (1.4, COMMON_RANGES['iv_shuntldo'][1][1])),
		'legend': None,
		'legend.loc': 'lower right',
		'group': 16,
	},

})



###################
# DAC Calibration #
###################

dac_configs = {
	'PREAMP': {
		'names': ['DAC_PREAMP_L_LIN', 'DAC_PREAMP_R_LIN', 'DAC_PREAMP_TL_LIN', 'DAC_PREAMP_TR_LIN', 'DAC_PREAMP_T_LIN', 'DAC_PREAMP_M_LIN'],
		'subpath': 'currents',
		'ytitle': 'Current [$\mu$A]',
		'yscale': 1e6,
		'fit.deg': [1]*6,
		'err': [0.1]*6,
		'fit.range': (0, 1000),
		'axis.range': ((50, 550), (5, 60)),
	},
	'VCAL': {
		'names': ['VCAL_HIGH', 'VCAL_MED'],
		'subpath': 'voltages',
		'ytitle': 'Voltage [mV]',
		'yscale': 1e3,
		'fit.deg': [1]*2,
		'err': [0.5]*2,
		'fit.range': (200, 3000),
		'axis.range': ((0, 4100), (0, 1e3)),
		'db': True
	},
	'OV': {
		'names': ['DAC_GDAC_L_LIN', 'DAC_GDAC_R_LIN', 'DAC_GDAC_M_LIN', 'DAC_REF_KRUM_LIN'],
		'subpath': 'voltages',
		'ytitle': 'Voltage [mV]',
		'yscale': 1e3,
		'fit.deg': [1, 1, 1, 1],
		'err': [3.0, 3.0, 3.0, 0.3],
		'fit.range': (0, 700),
		'axis.range': ((250, 950), (200, 1000)),
	},
	'OI': {
		'names': ['DAC_FC_LIN', 'DAC_KRUM_CURR_LIN', 'DAC_COMP_LIN', 'DAC_COMP_TA_LIN'],
		'subpath': 'currents',
		'ytitle': 'Current [$\mu$A]',
		'yscale': 1e6,
		'fit.deg': [1]*4,
		'err': [0.02]*4,
		'fit.range': (0, 1000),
		'axis.range': ((0, 350), (-1, 35)),
	},
	'LDAC': {
		'names': ['DAC_LDAC_LIN'],
		'subpath': 'currents',
		'ytitle': 'Current [$\mu$A]',
		'yscale': 1e6,
		'fit.deg': [1],
		'err': [0.2],
		'fit.range': (0, 1000),
		'axis.range': ((250, 1000), (25, 100)),
	}
}
DAC_CONFIGS = {'input': {}, 'output': {}}
for group, config in dac_configs.items():
	for iname, name in enumerate(config['names']):
		DAC_CONFIGS['input'].update({
			name: f'dac_calibration/{name}/{config["subpath"]}',
			f'+err_{name}': config['err'][iname]
		})
		DAC_CONFIGS['output'].update({
			name: lambda i, n=name, c=config: {int(k): c['yscale']*v for k, v in i[n].items()},
			f'+fit_{name}': lambda i, o, i_n=iname, n=name, c=config: fit_pol(o[n],
			                                                                  sigma=i[f'+err_{n}'],
			                                                                  fit_range=(c['fit.range'], (-1e6, 1e6)),
			                                                                  deg=c['fit.deg'][i_n])
		})

for group, config in dac_configs.items():
	############################################## Single-chip plots
	names = config['names']
	PLOTS_CHIP.update({
		f'{group}_dac_calibration': {
			'input':  {k: v for k, v in DAC_CONFIGS['input'].items() if any(name in k for name in names)},
			'output': {k: v for k, v in DAC_CONFIGS['output'].items() if any(name in k for name in names)},
			'output_sum': dict(
				**{f'offset_{name}': lambda o, n=name: o[f'+fit_{n}'][0][0] for name in names},
				**{f'slope_{name}':  lambda o, n=name: o[f'+fit_{n}'][0][1] for name in names},
				**{f'chi2_{name}':   lambda o, n=name: o[f'+fit_{n}'][1] for name in names},
				**{f'err_{name}':    lambda i, o, n=name: i[f'+err_{n}'] for name in names},
			),
			'fit.draw': {f'+fit_{name}': idx%5 for idx, name in enumerate(names)},
			'text': [f'{n}:  O: {{offset_{n}:.3f}}  S: {{slope_{n}:.3f}}  $\chi^2$: {{chi2_{n}:.3f}}  $\Delta$: {{err_{n}:.2f}}' for n in names],
			'title': 'DAC calibration [{chip_column:X}{chip_row:X}];DAC code;'+config['ytitle'],
			'legend': None,
			'legend.loc': 'lower right',
			'axis.range': config['axis.range'],
			'group': 9,
		}
	})
	# Storing input data for DB
	if 'db' in config:
		PLOTS_CHIP[f'{group}_dac_calibration']['db'] = {
			f'{name} vector': lambda o, n=name: o[n] for name in names
		}

	############################################## Wafer-wise plots
	for name in config['names']:
		PLOTS_WAFER.update({
			f'{name}_offset_dac_calibration': {
				'input':  {k: v for k, v in DAC_CONFIGS['input'].items() if name in k},
				'output': {k: v for k, v in DAC_CONFIGS['output'].items() if name in k},
				'aggregate.item': lambda o, n=name: o[f'+fit_{n}'][0][0],
				'group': 9,
				'db': f'{name}_OFF' if 'db' in config else None
			},

			f'{name}_slope_dac_calibration': {
				'input':  {k: v for k, v in DAC_CONFIGS['input'].items() if name in k},
				'output': {k: v for k, v in DAC_CONFIGS['output'].items() if name in k},
				'aggregate.item': lambda o, n=name: o[f'+fit_{n}'][0][1],
				'group': 9,
				'db': f'{name}_SLO' if 'db' in config else None
			},
		})

############################################## Wafer-wise plots combining low-range DACs
DAC_NAMES = []
for group, config in dac_configs.items():
	if group not in ['VCAL']:
		DAC_NAMES = DAC_NAMES + config['names']

PLOTS_WAFER.update({
	'offset_dac_calibration': {
		'input': {
			'+st': '@chip_statuses'
		},
		'output': {
			'statuses': lambda i, names=DAC_NAMES: [i['+st'][f'{n}_offset_dac_calibration'] for n in names],
		},
		'aggregate.item': lambda o: o['statuses'].count(0) if -1 not in o else None,
		'dependencies': [f'{n}_offset_dac_calibration' for n in DAC_NAMES],
		'regions.fixed': True,
		'bins.int': True,
		'group': 9,
	},

	'slope_dac_calibration': {
		'input': {
			'+st': '@chip_statuses'
		},
		'output': {
			'statuses': lambda i, names=DAC_NAMES: [i['+st'][f'{n}_slope_dac_calibration'] for n in names],
		},
		'aggregate.item': lambda o: o['statuses'].count(0) if -1 not in o else None,
		'dependencies': [f'{n}_slope_dac_calibration' for n in DAC_NAMES],
		'regions.fixed': True,
		'bins.int': True,
		'group': 9,
	}
})



####################
# Ring Oscillators #
####################

rng_x = (0.97, 1.27)
osc_configs = {
	'A0': {
		'names': [f'A{i}' for i in range(8)],
		'axis.range': (rng_x, (200, 900)),
	},
	'B0': {
		'names': [f'B{i}' for i in range(0, 7+1)],
		'axis.range': (rng_x, (300, 900)),
	},
	'B8': {
		'names': [f'B{i}' for i in range(8, 15+1)],
		'axis.range': (rng_x, (300, 900)),
	},
	'B16': {
		'names': [f'B{i}' for i in range(16, 21+1)],
		'axis.range': (rng_x, (200, 700)),
	},
	'B22': {
		'names': [f'B{i}' for i in range(22, 25+1)],
		'axis.range': (rng_x, (400, 900)),
	},
	'B26': {
		'names': [f'B{i}' for i in range(26, 33+1)],
		'axis.range': (rng_x, (400, 900)),
	},
}

OSC_INPUTS = {
	group: {
		# 'input': dict({n: f'ringosc_calibration/oscillators/bank:{n[0]}&number:{int(n[1:])}/frequency' for n in config['names']}, **{  # old format: before 2022/01/25
		'input': dict({n: f'ringosc_calibration/{n}/frequency' for n in config['names']}, **{
			'+err': 1,
			'VDDD': 'ringosc_calibration/VDDD',
			'VOFS_LP': 'vdd_trim_ldo/measurements/VOFS_LP'
		}),
		'output': dict({name: lambda i, n=name: i[n]*1e-6 for name in config['names']}, **{
			f'+fit_{name}': lambda i, o, n=name: fit_pol(x=o['x'], y=o[n], sigma=i['+err']) for name in config['names']
		}, **{
			'x': lambda i: i['VDDD'] - i['VOFS_LP'],
		})
	} for group, config in osc_configs.items()
}

OSC_NAMES = []
for group, config in osc_configs.items():
	names = config['names']
	OSC_NAMES = OSC_NAMES + names

	############################################## Single-chip plots
	PLOTS_CHIP.update({
		f'{group}_ring_oscillator': {
			'input': OSC_INPUTS[group]['input'],
			'output': OSC_INPUTS[group]['output'],
			'output_sum': dict(
				**{f'offset_{name}': lambda o, n=name: o[f'+fit_{n}'][0][0] for name in names},
				**{f'slope_{name}':  lambda o, n=name: o[f'+fit_{n}'][0][1] for name in names},
				**{f'chi2_{name}':   lambda o, n=name: o[f'+fit_{n}'][1] for name in names},
			),
			'fit.draw': {f'+fit_{name}': idx%5 for idx, name in enumerate(names)},
			'text': [f'{n}:  O: {{offset_{n}:.0f}}  S: {{slope_{n}:.0f}}  $\chi^2$: {{chi2_{n}:.2f}}' for n in names],
			'title': 'Ring oscillators [{chip_column:X}{chip_row:X}];VDDD [V];Frequency [MHz]',
			'legend': None,
			'legend.loc': 'lower right',
			'axis.range': config['axis.range'],
			'group': 10,
		}
	})

	############################################## Chip-overview plots
	for name in names:
		PLOTS_WAFER.update({
			f'{name}_offset_ring_oscillator': {
				'input': OSC_INPUTS[group]['input'],
				'output': OSC_INPUTS[group]['output'],
				'aggregate.item': lambda o, n=name: o[f'+fit_{n}'][0][0],
				'group': 10,
			},

			f'{name}_slope_ring_oscillator': {
				'input': OSC_INPUTS[group]['input'],
				'output': OSC_INPUTS[group]['output'],
				'aggregate.item': lambda o, n=name: o[f'+fit_{n}'][0][1],
				'group': 10,
			}
		})
############################################## Wafer-wise plots
PLOTS_WAFER.update({
	'offset_ring_oscillator': {
		'input': {
			'+st': '@chip_statuses'
		},
		'output': {
			'statuses': lambda i, names=OSC_NAMES: [i['+st'][f'{n}_offset_ring_oscillator'] for n in names],
		},
		'aggregate.item': lambda o: o['statuses'].count(0) if -1 not in o else None,
		'dependencies': [f'{n}_offset_ring_oscillator' for n in OSC_NAMES],
		'regions.fixed': True,
		'bins.int': True,
		'group': 10,
	},

	'slope_ring_oscillator': {
		'input': {
			'+st': '@chip_statuses'
		},
		'output': {
			'statuses': lambda i, names=OSC_NAMES: [i['+st'][f'{n}_slope_ring_oscillator'] for n in names],
		},
		'aggregate.item': lambda o: o['statuses'].count(0) if -1 not in o else None,
		'dependencies': [f'{n}_slope_ring_oscillator' for n in OSC_NAMES],
		'regions.fixed': True,
		'bins.int': True,
		'group': 10,
	}
})



##################################
# Temperature-sensor calibration #
##################################

TEMP_CONFIGS = {}
# Old sensor names from an outdated DAQ software
# temp_names = ['RADSENS_Digital_SLDO', 'RADSENS_Analog_SLDO', 'RADSENS_center',
#               'TEMPSENS_Digital_SLDO', 'TEMPSENS_Analog_SLDO', 'TEMPSENS_center']
temp_names = ['RADSENS_ACB', 'RADSENS_SLDOA', 'RADSENS_SLDOD',
              'TEMPSENS_ACB', 'TEMPSENS_SLDOA', 'TEMPSENS_SLDOD']
for name in temp_names:
	PLOTS_WAFER.update({
		f'{name}_bias_temp': {
			'input': {
				'min': f'temp_calib/{name}/min_bias',
				'max': f'temp_calib/{name}/max_bias',
			},
			'output': {
				'bias': lambda i: 1e2*0.5*(np.mean(i['max']) - np.mean(i['min'])),
			},
		},

		f'{name}_ideality_mean': {
			'input': {
				'min': f'temp_calib/{name}/min_bias',
				'max': f'temp_calib/{name}/max_bias',
				'temp': f'temp_calib/{name}/temperature',
			},
			'output': {
				'std': lambda i: np.mean(i['max']-i['min']) / i['temp'] * 4285.19, # TODO change name from std to mean?
			},
		},

		f'{name}_ideality_std': {
			'input': {
				'min': f'temp_calib/{name}/min_bias',
				'max': f'temp_calib/{name}/max_bias',
				'temp': f'temp_calib/{name}/temperature',
			},
			'output': {
				'std': lambda i: np.std(i['max']-i['min']) / i['temp'] * 4285.19,
			},
		},

		f'{name}_ideality_temp': {
			'input': f'temp_calib/{name}/ideality_factor',
			'db': name,
		},
	})
for domain in ['ACB', 'SLDOA', 'SLDOD']:
	PLOTS_WAFER.update({
		f'{domain}_temp_rad_ratio': {
			'input': {
				'min_temp': f'temp_calib/TEMPSENS_{domain}/min_bias',
				'max_temp': f'temp_calib/TEMPSENS_{domain}/max_bias',
				'min_rad': f'temp_calib/RADSENS_{domain}/min_bias',
				'max_rad': f'temp_calib/RADSENS_{domain}/max_bias',
				'temp_temp': f'temp_calib/TEMPSENS_{domain}/temperature',
				'temp_rad': f'temp_calib/RADSENS_{domain}/temperature',
			},
			'output': {
				'ratio': lambda i: (
					(np.mean(i['max_temp']-i['min_temp']) / i['temp_temp']) /
					(np.mean(i['max_rad' ]-i['min_rad' ]) / i['temp_rad' ])
				),
			},
		},
	})
PLOTS_WAFER['temp_temp'] = {
	'input': f'temp_calib/{temp_names[0]}/temperature',
	'db': {
		'TEMP_DEGC': lambda i: i - 273.15,
	}
}

temp_names = ['Poly_TEMPSENS_top', 'Poly_TEMPSENS_bottom']
for name in temp_names:
	PLOTS_WAFER.update({
		f'{name}_mean_temp': {
			'input': {
				'adc': f'temp_calib/{name}/raw_ADC',
			},
			'output': {
				'mean': lambda i: np.mean(i['adc']),
			},
		},

		f'{name}_std_temp': {
			'input': {
				'adc': f'temp_calib/{name}/raw_ADC',
			},
			'output': {
				'mean': lambda i: np.std(i['adc']),
			},
		}
	})






####################################
# Integrated monitoring mux checks #
####################################

PLOTS_WAFER['int_mux_vdda'] = {
	'input': {
		'sigut': 'chip_mux_monitoring/VMUX/VDDA_HALF',
		'sigref': 'vdd_trim_shunt/measurements/VDDA',
	},
	'output': {
		'ratio': lambda i: i['sigut']*2 / i['sigref'],
	},
}

PLOTS_WAFER['int_mux_vddd'] = {
	'input': {
		'sigut': 'chip_mux_monitoring/VMUX/VDDD_HALF',
		'sigref': 'vdd_trim_shunt/measurements/VDDD',
	},
	'output': {
		'ratio': lambda i: i['sigut']*2 / i['sigref'],
	},
}

PLOTS_WAFER['int_mux_vina'] = {
	'input': {
		'sigut': 'chip_mux_monitoring/VMUX/VINA_QUARTER',
		'sigref': 'vdd_trim_shunt/measurements/VINA_SNS',
	},
	'output': {
		'ratio': lambda i: i['sigut']*4 / i['sigref'],
	},
}

PLOTS_WAFER['int_mux_vind'] = {
	'input': {
		'sigut': 'chip_mux_monitoring/VMUX/VIND_QUARTER',
		'sigref': 'vdd_trim_shunt/measurements/VIND_SNS',
	},
	'output': {
		'ratio': lambda i: i['sigut']*4 / i['sigref'],
	},
}

PLOTS_WAFER['int_mux_iina'] = {
	'input': {
		'sigut': 'chip_mux_monitoring/IMUX/IINA',
		'sigref': 'vdd_trim_shunt/measurements/IINA',
	},
	'output': {
		'ratio': lambda i: i['sigut']*21000 / i['sigref'],
	},
}

PLOTS_WAFER['int_mux_iind'] = {
	'input': {
		'sigut': 'chip_mux_monitoring/IMUX/IIND',
		'sigref': 'vdd_trim_shunt/measurements/IIND',
	},
	'output': {
		'ratio': lambda i: i['sigut']*21000 / i['sigref'],
	},
}

PLOTS_WAFER['int_mux_ishunta'] = {
	'input': {
		'ish': 'chip_mux_monitoring/IMUX/ISHUNTA',
		'ildo': 'pixel_power/I_ana_default',
		'itot': 'vdd_trim_shunt/measurements/IINA',
	},
	'output': {
		'ratio': lambda i: i['ish']*21520 / (i['itot']-i['ildo']),
	},
}

PLOTS_WAFER['int_mux_ishuntd'] = {
	'input': {
		'ish': 'chip_mux_monitoring/IMUX/ISHUNTD',
		'ildo': 'pixel_power/I_dig_default',
		'itot': 'vdd_trim_shunt/measurements/IIND',
	},
	'output': {
		'ratio': lambda i: i['ish']*21520 / (i['itot']-i['ildo']),
	},
}

PLOTS_WAFER['int_mux_vofs'] = {
	'input': {
		'sigut': 'chip_mux_monitoring/VMUX/VOFS_QUARTER',
		'sigref': 'vdd_trim_shunt/measurements/VOFS',
	},
	'output': {
		'ratio': lambda i: (i['sigut']*4) / (i['sigref']*2),
	},
}

#################################
# CONFIGURATION POST-PROCESSING #
#################################

# Overriding regions with externally configured definitions
for name, (regions, bins, group, priority, title) in REGIONS.items():
	if name not in PLOTS_WAFER:
		print(f'WARNING: Region definition for non-existent plot: {name}')
		continue
	config = PLOTS_WAFER[name]
	config['regions'] = regions
	config['bins'] = bins
	config['group'] = group
	config['priority'] = priority
	config['title'] = title
	# Skipping plots with undefined priority
	if priority is None:
		config['skip'] = True
		config['priority'] = 0

# Setting the aggregate type to 'histogram' for all wafer-wise plots
for name, config in PLOTS_WAFER.items():
	config['aggregate'] = 'histogram'

# Adding all the subsets to the global CONFIG object
CONFIG.update(PLOTS_WAFER)
CONFIG.update(PLOTS_CHIP)

# Adding missing default parameter values for completeness
for cname, config in CONFIG.items():
	if 'group' not in config:
		config['group'] = -1
	if 'priority' not in config:
		config['priority'] = 0
	if 'skip' not in config:
		config['skip'] = False



############################### Command-line functionality
if __name__ == '__main__':

	import pdb
	from wlt.analysis.config_handler import args, handle

	if args.debug:
		pdb.set_trace()

	handle(CONFIG)
