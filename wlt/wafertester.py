#!/usr/bin/env python3
#pylint: disable=I1101,C0103
"""
Module used for handling the wafer probing component of the testing.

This module defines the WaferTester class, used to handle the communication with
the probe station, along with Exception classes related to the probe station.

This module imports the probe station library from wlt.hardware.probers.
"""

import os
import sys
import time
import json
import shutil
import socket
import logging

from enum import Enum

import gpib

from wlt import __version__
from wlt.sites import ProbingSites

from wlt.duts.chips import CHIPS
from wlt.duts.wafers import WAFER_MAPS
from wlt.duts import get_waferprobing_id

import wlt.chiptester
from wlt import AbortWaferprobing, SkipChip
from wlt import HighContactResistance

from wlt.config import CHIPTESTER_CONF, WAFERTESTER_CONF, DATA_DIR

from wlt.hardware.devices import get_wpac, get_prober
from wlt.hardware.probers import ProberError, AutomationError, InvalidDieError
from wlt.hardware.probers import CriticalProberError

from wlt.daq import load_sct_vectors

#TODO: current_col and current_row should be a property


class SoftwareDieError(Exception):
	"""Exception raised when a non-existent die on the wafer map is requested
	for probe station movement."""


#TODO: move to probe station library
class CommunicationError(Exception):
	"""Exception raised when it's not possible to communicate with the probe
	station."""


class ProbingError(Exception):
	"""Exception raised when a problem during the probing of the wafer has been 
	detected."""


class ProberStatus(Enum):
	"""Enumeration used to store the probe station status."""
	GOOD = 0
	PROBER_ERR = 1
	IFACE_ERR = 2
	NO_REPLY = 3
	BAD_REPLY = 4


class WpacStatus(Enum):
	"""Enumeration used to store the WPAC station status."""
	GOOD = 0
	ERR = 1
	NO_REPLY = 2
	BAD_REPLY = 3


class WaferTester():
	"""
	Class used for managing the wafer probing component of the testing.

	This class interacts with the probe station in order to move to the chip
	under test, set the chuck to a certain distance from the wafer and check
	that no pattern matching errors occur.
	"""

	def __init__(self, probing_site, batch_id, wafer_id, chip_type,
			n_chips_test = None, start_column = None, start_row = None,
			test_chips = False, log_level = logging.INFO,
			separate_after_test = False, metadata = None,
			autoclean_enabled = False, autoclean_interval = 47, dies_list = None,
			overtravel_um = None, use_autocontact = False,
			autocontact_z_start_um = None, autocontact_z_max_um = None,
			autocontact_step_um = None, autocontact_n_steps = None,
			autocontact_max_asymm_um = None, port = 16180):
		"""
		Initializes the WaferTester object.

		This method sets up the class-level logging, initializes the
		communication with the probe station and performs various sanity checks
		before the testing process begins.

		Parameters
		----------
		probing_site : wlt.ProbingSite
			Waferprobing site at which the testing is performed.
		batch_id : str
			Foundry batch identifier for the wafer under test.
		wafer_id : str
			Foundry identifier for the wafer under test.
		chip_type : str
			Type of the chips under test.
		n_chips_test : int or None, optional
			Number of chips on which WaferTester will travel to.
		start_column : int or None, optional
			Column index of the first die to test.
		start_row : int or None, optional
			Row index of the first die to test.
		test_chips : bool, optional
			Selects whether to test the chips or not. If set to False, 
			WaferTester only performs the so-called wafer travel.
		log_level : int, optional
			Verbosity of the logging information.
		separate_after_test : bool, optional
			Parameter which sets whether the probe station will go to 
			separation height after completing the testing of all the chips or 
			not.
		metadata : dict or None, optional
			Dictionary containing waferprobing metadata, such as the operator, 
			the hardware used and so on.
		autoclean_enabled : bool, optional
			Boolean that configures whether the default cleaning sequence is 
			performed when the contact resistance gets too high.
		autoclean_interval : int, optional
			Configures the interval with which the probe card needles are 
			cleaned automatically.
		dies_list : list of int or None, optional
			List of dies to be tested. Overrides the other configuration.
		overtravel_um : int or None, optional
			Overtravel setting for the probe station.
		find_contact_height : bool, optional
			Find the contact height for each die.
		autocontact_z_start_um : float or int, optional
			Starting height (in micrometres) for finding the optimal contact 
			height.
		autocontact_z_max_um : float or int, optional
			Maximum height (in micrometres) for finding the optimal contact 
			height.
		autocontact_step_um : float or int, optional
			Step size (in micrometres) for finding the optimal contact height.
		autocontact_n_steps : int, optional
			Number of steps for finding the optimal contact height.
		autocontact_max_asymm_um : int or float, optional
			Maximum asymmetry between edge sensors when finding the optimal 
			contact height.
		port : int, optional
			Local port to bind to when running WaferTester. It is used to 
			prevent multiple instances from running at the same time.

		Raises
		------
		ValueError
			Raised when the chip_type parameter is invalid.
		RuntimeError
			If the wafer has not been registered.
		CommunicationError
			Raised when failing to communicate with the probe station after 
			initialization.
		"""

		###########################################
		# makes sure only one instance is running #
		###########################################

		self.port = port
		self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:
			self._socket.bind(("localhost", self.port))
		except OSError:
			raise RuntimeError("WaferTester instance already running!") from None
		except Exception as e:
			raise RuntimeError(f"Failed to bind to port {self.port}!") from e

		################
		# probing site #
		################

		#checking the probing site
		if probing_site not in ProbingSites:
			raise ValueError("Wrong probing site configured!")
		self.probing_site = probing_site

		#########################
		# initial configuration #
		#########################

		#setting the serial number
		self.batch_id = batch_id
		self.wafer_id = wafer_id

		#string representation of wafer information
		self.wafer_str = f"{self.batch_id}-{self.wafer_id}"

		#attribute which sets whether the probe station should separate from
		#the wafer at the end of the testing or not
		self.separate_after_test = separate_after_test

		#variable which stores the init time as string
		init_time = time.strftime("%Y%m%d_%H%M%S")

		#attribute for detecting a stuck scan chain
		self._sct_errors = []

		#################
		# logging setup #
		#################

		#create a folder with the wafer SN (parent of work directory)
		log_dir_par_name = f"wafer_{self.wafer_str}"
		log_dir_par_path = os.path.join(DATA_DIR, log_dir_par_name)
		os.makedirs(log_dir_par_path, exist_ok=True)

		#subfolder with date and time
		log_dir_name = init_time
		log_dir_path = os.path.join(log_dir_par_path, log_dir_name)

		#try to create the folder. If it already exists, try adding an index to
		#the folder name, starting from `i` equal to 2. If this fails up to `i`
		#equal to `n_stop`, raise a RuntimeError exception
		n_stop = 5
		for i in range(2, n_stop + 1):
			try:
				os.makedirs(log_dir_path, exist_ok=False)
				break
			except FileExistsError:
				if i == n_stop:
					raise RuntimeError("Could not create log folder!") from None
				log_dir_name = f"{init_time}-{i}"
				log_dir_path = os.path.join(log_dir_par_path, log_dir_name)

		#setting data_dir attribute
		self.data_dir = log_dir_path

		#create a folder for ChipTester
		log_dir_chips = os.path.join(self.data_dir, "chips")
		os.makedirs(log_dir_chips, exist_ok=False)

		#log level attribute
		self.log_level = log_level

		#main logger
		self.logger = logging.getLogger(f"WaferTester ({self.wafer_id})")
		self.logger.setLevel(self.log_level)

		#file handler
		log_file_name = f"wafer_{self.wafer_str}_{init_time}.log"
		log_file_path = os.path.join(self.data_dir, log_file_name)
		file_handler = logging.FileHandler(log_file_path)
		file_handler.setLevel(self.log_level)

		#console handler
		console_handler = logging.StreamHandler()
		console_handler.setLevel(self.log_level)

		#defining formatter
		log_formatter = logging.Formatter(
			"%(asctime)s | %(name)-20s | %(levelname)-8s | %(message)s"
			#, "%Y-%m-%d %H:%M:%S"
		)

		#setting formatters
		file_handler.setFormatter(log_formatter)
		console_handler.setFormatter(log_formatter)

		#adding handlers
		self.logger.addHandler(file_handler)
		self.logger.addHandler(console_handler)

		###############
		# wafer setup #
		###############

		#logging wafer information
		self.logger.info(f"Probing site: {self.probing_site.name_}")
		self.logger.info(f"Wafer: {self.wafer_id}")
		self.logger.info(f"Batch: {self.batch_id}")

		#check that the wafer has been registered
		waferprobing_id = get_waferprobing_id(self.batch_id, self.wafer_id)
		if waferprobing_id is None:
			raise RuntimeError("Unregistered wafer!")
		self.logger.info(f"Waferprobing ID: {waferprobing_id}")

		#chip type
		if chip_type not in CHIPS:
			message = f"Wrong chip type: {chip_type}!"
			self.logger.error(message)
			raise ValueError(message)
		self.chip_type = chip_type
		self.logger.info(f"Chip type: {self.chip_type}")
		#wafer map
		self.wafer_map = WAFER_MAPS[self.chip_type]["ordered"]
		self.n_chips_wafer = WAFER_MAPS[self.chip_type]["chip_count"]

		#configuration for the dies to test
		self.start_column = start_column
		self.start_row = start_row
		self.n_chips_test = n_chips_test
		self.dies_list = dies_list
		self.configure_dies()

		###########
		# testing #
		###########

		#setting the test_chips data member
		# True: perform chip testing
		# False: only perform "wafer travel"
		self.test_chips = test_chips
		test_status = "enabled" if self.test_chips is True else "disabled"
		self.logger.warning(f"Chip testing is {test_status}")

		#######################
		# probe station setup #
		#######################

		#initialization of probe station object (can raise exception)
		#TODO: user specific prober method
		self.probe_station = get_prober()

		try:
			#getting PS ID
			self.logger.info(self.probe_station.get_id())
		except gpib.GpibError:
			message = "Couldn't communicate with probe station"
			self.logger.error(message)
			raise CommunicationError(message) from None

		self.logger.info("Communication with probe station initialized")

		#setting probe station position
		self.current_col, self.current_row = self.probe_station.get_die()

		##############
		# overtravel #
		##############

		#configuring and logging the overtravel setting
		self.overtravel_um = overtravel_um
		self.logger.info(f"Overtravel: {self.overtravel_um} um")

		#############################
		# automated needle cleaning #
		#############################

		#attribute for controlling whether to clean the needles or not when
		#the contact resistance gets too high
		self.autoclean_enabled = autoclean_enabled

		#interval and counter for the automatic cleaning of the needles every N chips
		try:
			self.autoclean_interval = int(autoclean_interval)
		except TypeError as e:
			self.logger.error(f"Invalid autoclean interval type: {type(autoclean_interval)}!")
			raise RuntimeError("Invalid autoclean configuration!") from e
		self.autoclean_cntr = 0

		#counter and threshold for the number of detected contact resistance errors
		self.res_err_cntr = 0
		self.res_err_thr = 3

		#counter and threshold for the number of needle cleaning procedures performed
		self.clean_cntr = 0
		self.clean_thr = 10

		#logging the configuration for autocleaning
		self.logger.info(f"Autocleaning enabled: {self.autoclean_enabled}")
		self.logger.info(f"Autocleaning interval: {self.autoclean_interval}")

		####################################
		# automated contact height finding #
		####################################

		#configuring the autocontact class attributes
		self.use_autocontact = use_autocontact
		self.autocontact_z_start_um = autocontact_z_start_um
		self.autocontact_z_max_um = autocontact_z_max_um
		self.autocontact_step_um = autocontact_step_um
		self.autocontact_n_steps = autocontact_n_steps
		self.autocontact_max_asymm_um = autocontact_max_asymm_um

		#logging the most important autocontact configuration
		self.logger.info(f"Autocontact enabled: {self.use_autocontact}")
		self.logger.info(f"Autocontact starting height: {self.autocontact_z_start_um} um")
		self.logger.info(f"Autocontact maximum height: {self.autocontact_z_max_um} um")
		self.logger.info(f"Autocontact step size: {self.autocontact_step_um} um")

		###########################################
		# setting up wafer probing data attribute #
		###########################################

		#initialization of dictionary attribute used to store wafer probing data
		self.data = {}
		self.data["batch_id"] = self.batch_id
		self.data["wafer_id"] = self.wafer_id
		self.data["probing_site"] = self.probing_site.name_
		self.data["chip_type"] = self.chip_type
		self.data["n_chips_test"] = self.n_chips_test
		self.data["start_column"] = self.start_column
		self.data["start_row"] = self.start_row
		self.data["current_col"] = self.current_col
		self.data["current_row"] = self.current_row
		if metadata is not None:
			try:
				self.data.update(metadata)
			except Exception:
				self.logger.warning("Could not add input metadata to "
					"waferprobing data")

		#initialization of results file
		data_file_name = f"wafer_{self.wafer_str}_{init_time}.json"
		self.data_file = os.path.join(self.data_dir, data_file_name)

		#################################
		# checking available disk space #
		#################################

		#checking the available storage space (in GB) in the data folder
		free_space_gb = shutil.disk_usage(self.data_dir).free / 1e9
		self.logger.info(f"Available storage space: {free_space_gb:.2f} GB")
		#if less than 5 GB are available, stop right away
		if free_space_gb < 5:
			self.logger.error("Very low storage space for storing wafer data!")
			raise RuntimeError("Very low storage space!")
		#if there are between 5 and 10 GB available, go on but print a warning
		elif 5 <= free_space_gb < 10:
			self.logger.warning("Low storage space!")

		####################
		# software version #
		####################

		self.logger.info(f"Software version: {__version__}")

	def configure_dies(self):
		"""Initialises the configuration for the dies to test."""

		#if a non-empty list has been provided for the die coordinates, use that
		#for testing
		if (self.dies_list is not None) and (len(self.dies_list) != 0):
			self.n_chips_test = len(self.dies_list)
			self.start_column = (self.dies_list[0] & 0xF0) >> 4
			self.start_row = self.dies_list[0] & 0x0F
			#the input list uses integers, so they must be converted to tuples
			self.dies_list = [((x & 0xF0) >> 4, x & 0x0F) for x in self.dies_list]

		#if the number of chips to test has not been provided, set the whole
		#wafer for testing
		if self.n_chips_test is None:
			self.n_chips_test = self.n_chips_wafer

		#if the number of chips to test is not valid, do not test at all
		if self.n_chips_test not in range(0, self.n_chips_wafer + 1):
			self.logger.error("Invalid number of chips to test: "
				f"{self.n_chips_test}. Setting it to 0")
			self.n_chips_test = 0

		#check if both start column and row are defined, otherwise set the first
		#die in the wafer map as the starting die
		if (self.start_column is None) or (self.start_row is None):
			self.start_column = self.wafer_map[0][0]
			self.start_row = self.wafer_map[0][1]

		#checking that the starting column and row are in the wafer map
		if (self.start_column, self.start_row) not in self.wafer_map:
			self.logger.warning("Invalid starting position: "
				f"{self.start_column}-{self.start_row}")
			self.start_column = self.wafer_map[0][0]
			self.start_row = self.wafer_map[0][1]
			self.n_chips_test = 0

		#initialise the list of dies to test if not already done
		if (self.dies_list is None) or (len(self.dies_list) == 0):
			index_start = self.wafer_map.index((self.start_column, self.start_row))
			index_stop = index_start + self.n_chips_test
			self.dies_list = self.wafer_map[index_start:index_stop]

	def close(self):
		"""Safely terminates the execution of the wafer probing process."""

		#closing the socket
		self._socket.close()

		#closing and removing log handlers
		for handler in self.logger.handlers:
			handler.close()
		self.logger.handlers.clear()

	def move_to_chip(self, col, row):
		"""
		Moves the probe station to (col, row).

		It checks whether the probe station is already on the selected die and,
		in that case, no further actions are taken.

		The current_col and current_row attributes are updated and the absolute
		position of the probe station chuck is printed in the log.

		Parameters
		----------
		col : int
			Column index of the die to move to.
		row : int
			Row index of the die to move to.

		Raises
		------
		SoftwareDieError
			Raised in case (col, row) is not present in the wafer map.
		"""

		#check if (col, row) is in the wafer map
		if (col, row) not in self.wafer_map:
			message = f"Requested movement to invalid position: {col:X}{row:X}"
			self.logger.error(message)
			raise SoftwareDieError(message)

		#check if already on that chip
		self.current_col, self.current_row = self.probe_station.get_die()
		if (self.current_col, self.current_row) == (col, row):
			self.logger.debug(f"Already on chip {col:X}{row:X}")
			return

		#going manually to separation height
		if self.probe_station.is_in_contact() is True:
			self.logger.info("Going to separation height before moving")
			self.probe_station.separate()

		#performing the requested movement
		self.logger.info(f"Moving to die {col:X}{row:X}")
		try:
			self.probe_station.goto_die(col, row)
		except ProberError:
			raise

		#updating position
		self.current_col, self.current_row = self.probe_station.get_die()

		#printing absolute position
		x_pos, y_pos, z_pos = self.probe_station.get_position()
		self.logger.debug(f"x = {x_pos}, y = {y_pos}, z = {z_pos}")

	def move_to_first_chip(self):
		"""
		Moves the probe station to the first die in the wafer.

		After the movement, the probe station reply is inspected to search for
		pattern matching errors. If none are found, the current_col and
		current_row attributes are updated and the absolute position of the
		probe station chuck is printed in the log.
		"""

		#performing the requested movement
		self.logger.info("Moving to the first die")
		try:
			self.probe_station.goto_first_die()
		except ProberError:
			self.logger.exception("Could not go to the first die!")
			raise

		#updating position
		self.current_col, self.current_row = self.probe_station.get_die()

		#printing absolute position
		x_pos, y_pos, z_pos = self.probe_station.get_position()
		self.logger.debug(f"x = {x_pos}, y = {y_pos}, z = {z_pos}")

	def move_to_next_chip(self):
		"""
		Moves the probe station to the next die in the wafer.

		After the movement, the probe station reply is inspected to search for
		pattern matching errors. If none are found, the current_col and
		current_row attributes are updated and the absolute position of the
		probe station chuck is printed in the log.
		"""

		#performing the requested movement
		self.logger.info("Moving to the next die")
		try:
			self.probe_station.goto_next_die()
		except ProberError:
			self.logger.exception("Failed to go to the next die!")
			raise

		#updating position
		self.current_col, self.current_row = self.probe_station.get_die()

		#printing absolute position
		x_pos, y_pos, z_pos = self.probe_station.get_position()
		self.logger.debug(f"x = {x_pos}, y = {y_pos}, z = {z_pos}")

	def verify_prober_status(self):
		"""
		Checks that the probe station is in a good state.

		Returns
		-------
		status : ProberStatus
			Prober status code.
		"""

		self.logger.info("Verifying probe station status")

		#checking the communication with the probe station
		try:
			idn = self.probe_station.get_id()
		except ProberError:
			self.logger.exception("Prober error when checking prober status!")
			return ProberStatus.PROBER_ERR
		except gpib.GpibError:
			self.logger.exception("GPIB error when checking prober status!")
			return ProberStatus.IFACE_ERR
		#TODO: handle generic exception?

		#checking that the reply is not empty
		if idn == "":
			self.logger.error("Empty prober reply when checking prober status!")
			return ProberStatus.NO_REPLY

		self.logger.info("Probe station is in a good state")
		return ProberStatus.GOOD

	def verify_wpac_status(self):
		"""
		Checks that the WPAC and the probe card are in a good state.

		Returns
		-------
		status : int
			WPAC status code.
		"""

		self.logger.info("Verifying WPAC status")

		wpac = get_wpac()

		#read all WPAC registers
		try:
			wpac.read_all_registers()
		except Exception:
			self.logger.exception("Failed to get WPAC status!")
			return WpacStatus.ERR
		#TODO: handle more specific exceptions?

		self.logger.info("Completed verification of WPAC status")
		return WpacStatus.GOOD

	def is_es_state_good(self, expected=False):
		"""
		Checks that the edge sensors are in the expected state.

		Parameters
		----------
		expected : bool
			The expected edge sensors status.

		Returns
		-------
		bool
			True if the edge sensors are in the expected state, False otherwise.
		"""

		#expected statuses to be compared to actual measurements
		expected_status = [expected, expected]

		wpac = get_wpac()

		#checking both the edge sensors status via I2C and via microcontroller
		es_status_i2c = wpac.read_edge_sensors()
		es_status_uc = wpac.read_edge_sensors_uc()
		self.logger.info(f"Edge sensors state (I2C): {es_status_i2c}")
		self.logger.info(f"Edge sensors state (microcontroller): {es_status_uc}")

		#flags used to check the status
		is_es_i2c_good = (es_status_i2c == expected_status)
		is_es_uc_good = (es_status_uc == expected_status)
		is_es_equal = (es_status_i2c == es_status_uc)

		#returning True only if both readings are consistent with the expected one
		if is_es_i2c_good and is_es_uc_good:
			self.logger.info("Good edge sensors state")
			return True

		#logging if the I2C state is bad
		if is_es_i2c_good is False:
			self.logger.error("Unexpected edge sensors state (I2C)!")

		#logging if the microcontroller state is bad
		if is_es_uc_good is False:
			self.logger.error("Unexpected edge sensors state (microcontroller)!")

		#logging if inconsistent edge sensor readings between two methods
		if is_es_equal is False:
			self.logger.error("Inconsistent edge sensor readings!")

		return False

	def dump_data(self):
		"""Saves the wafer probing data to a json file."""

		#saving data to file
		try:
			with open(self.data_file, "w") as file_:
				json_data = json.dumps(self.data, indent=4)
				file_.write(json_data)
		except Exception:
			self.logger.warning("Failed to write data to results file "
				f"{self.data_file}")

	def test_chip(self, go_to_separation = True):
		"""
		Manages the testing of the chips on the wafer.

		This method creates an instance of a `ChipTester` object and uses it in 
		order to test the current die.

		Parameters
		----------
		go_to_separation : bool, optional
			Go to separation height at the end of the testing.

		Raises
		------
		AbortWaferprobing
			If the wafer-level testing procedure has to be stopped.
		HighContactResistance
			If the contact resistance is too high.
		BadContact
			If the correct contact height could not be found.
		"""

		#initial logging
		chip_column, chip_row = self.probe_station.get_die()
		chip_str = f"{self.wafer_str}-{chip_column:X}{chip_row:X}"
		self.logger.info(f"Testing chip {chip_str}")

		#finding the optimal contact height if enabled
		if self.use_autocontact is True:
			try:
				height = self.autocontact()
			except ValueError as e:
				self.logger.exception("Wrong autocontact configuration!")
				raise AbortWaferprobing("Wrong autocontact configuration!") from e
			except Exception as e:
				self.logger.exception("Unexpected exception from autocontact "
					"routine!")
				raise SkipChip("Failed autocontact, skipping chip") from e
			#checking if the contact height has been found
			if height is None:
				self.logger.error(f"Could not find contact height for chip {chip_str}!")
				raise SkipChip("Contact height not found, skipping chip")

		#checking the overtravel height
		if self.overtravel_um is not None:
			self.logger.info(f"Setting overtravel to {self.overtravel_um} um")
			self.probe_station.set_overtravel(self.overtravel_um)

		#going to contact height if not already there
		if self.probe_station.is_in_contact() is True:
			self.logger.info("Probe station already at contact height")
		else:
			self.logger.info("Moving chuck to contact height")
			try:
				self.probe_station.contact()
			except ProberError:
				self.logger.exception("Failed to get to contact height!")
				raise AbortWaferprobing("Aborting waferprobing!")

		#check that it's in contact
		if self.probe_station.is_in_contact() is False:
			raise ProbingError("The probe station is not at contact height!")

		#getting the position of the chuck
		x, y, z = self.probe_station.get_position_from_zero() #pylint: disable=C0103

		#data dir for ChipTester
		data_dir_chiptester = os.path.join(self.data_dir, "chips")

		#initializing an instance of ChipTester
		chip_tester = wlt.chiptester.ChipTester(
			probing_site=self.probing_site,
			chip_type=self.chip_type,
			chip_col=chip_column,
			chip_row=chip_row,
			batch_id=self.batch_id,
			wafer_id=self.wafer_id,
			enabled_tests=CHIPTESTER_CONF["enabled_tests"],
			config_tests=CHIPTESTER_CONF["config_tests"],
			data_dir=data_dir_chiptester,
			log_level=CHIPTESTER_CONF["log_level"],
			chuck_pos=(x, y, z)
		)

		#get the file handler from self.logger. There should be only **one**
		#FileHandler in self.logger.handlers
		file_handler = [fh for fh in self.logger.handlers
			if isinstance(fh, logging.FileHandler)][0]

		#add WaferTester file handler to the ChipTester logger in order to get
		#the CT log entries also in the WT log
		chip_tester.logger.addHandler(file_handler)

		#start testing
		try:
			chip_tester.main()
		except KeyboardInterrupt:
			self.logger.warning("Caught a keyboard interrupt, stopping the "
				"testing")
			raise
		except HighContactResistance:
			raise
		except AbortWaferprobing:
			raise
		#the `finally` block gets executed **before** the exception is raised
		finally:
			#storing the last scan chain errors
			self._sct_errors.append(chip_tester.get_sct_errors())
			#explicitly remove log handler from chiptester
			chip_tester.logger.removeHandler(file_handler)
			chip_tester.close()
			if go_to_separation is True:
				self.logger.info("Moving wafer chuck to separation height")
				try:
					self.probe_station.separate()
				except ProberError:
					self.logger.exception("Failed to get to separation height!")
					raise AbortWaferprobing("Aborting waferprobing!")

		self.logger.info("Testing completed")

	def cres_handler(self):
		"""
		Method used to handle the case in which testing has been aborted 
		for a chip due to high contact resistance.

		Returns
		-------
		bool
			Specifies whether testing can continue or not.
		"""

		#increase the counter of contact resistance problems
		self.res_err_cntr += 1

		#if the counter is less than the threshold, go on with testing without
		#doing anything else
		if self.res_err_cntr < self.res_err_thr:
			return True

		#log the number of errors if the threshold has been crossed
		self.logger.warning(f"Catched {self.res_err_thr} contact "
			"resistance errors!")

		#if the errors counter is greater or equal to the threshold but
		#needle cleaning is disabled, just stop testing
		if self.autoclean_enabled is False:
			self.logger.error("Waferprobing has been aborted due "
				"to high contact resistance!")
			return False

		#if needle cleaning is enabled but it has been performed too
		#many times, abort testing
		if self.clean_cntr >= self.clean_thr:
			self.logger.error("Cannot clean needles anymore, aborting "
				"testing!")
			return False

		#if needle cleaning is enabled and it has not been performed too
		#many times, perform the cleaning procedure and continue
		self.logger.warning("Performing a cleaning procedure on "
			"the needles!")

		#Running the cleaning procedure. If, for any reason, it fails, testing
		#is halted altogether
		try:
			self.probe_station.clean_needles()
		except Exception:
			self.logger.exception("Could not clean the needles!")
			return False

		#update the counters and continue testing
		self.clean_cntr += 1
		self.res_err_cntr = 0
		return True

	def autocontact(self):
		"""
		Runs the algorithm to find the wafer contact height.

		Raises
		------
		ValueError
			If the provided configuration is incorrect.
		"""

		#checking the height
		wrong_z_start_conds = [
			self.autocontact_z_start_um >= self.autocontact_z_max_um,
			self.autocontact_z_start_um < 0
		]
		if any(wrong_z_start_conds):
			raise ValueError("Invalid starting height detected!")

		#checking the step
		if (self.autocontact_step_um > 20) or (self.autocontact_step_um < 1):
			raise ValueError("Invalid step size detected!")

		#checking the number of steps
		if (self.autocontact_n_steps > 200) or (self.autocontact_n_steps < 0):
			raise ValueError("Invalid number of steps detected!")

		#getting the Wpac object
		wpac = get_wpac()

		#boolean for starting the count for the contact asymmetry
		do_count_asymm = False

		#z distance between edge sensors (absolute value in um)
		asymm_um = 0

		#boolean to check if the contact height has been found
		found = False

		#final value to be returned for the contact height
		height_result_um = None

		#disabling overtravel
		self.logger.info("Disabling overtravel")
		self.probe_station.disable_overtravel()

		#disabling soft contact if available for this probe station
		self.logger.info("Disabling soft contact if available")
		try:
			self.probe_station.disable_soft_contact()
		except NotImplementedError:
			self.logger.warning("Soft contact unavailable for this probe station")

		#looping on contact heights
		height_um = self.autocontact_z_start_um
		for _ in range(self.autocontact_n_steps + 1):
			#checking that the new contact value is not greater than the maximum
			if height_um >= self.autocontact_z_max_um:
				self.logger.error("Maximum contact height reached!")
				height_result_um = None
				found = False
				break

			#setting the new height without moving to it yet
			self.logger.info(f"Setting chuck height to {height_um} um")
			self.probe_station.set_contact_height(height_um)

			#moving to the new contact height
			self.logger.info("Moving chuck to contact height")
			self.probe_station.contact()

			#getting the status of the edge sensors
			es1, es2 = wpac.read_edge_sensors()
			self.logger.info(f"Edge sensor 1: {es1}")
			self.logger.info(f"Edge sensor 2: {es2}")

			#if only one edge sensor is triggered, increment the asymmetry counter
			#if counting is already enabled
			if (es1 ^ es2) and (do_count_asymm is True):
				asymm_um += self.autocontact_step_um

			#if only one edge sensor is triggered, but asymmetry counting is
			#disabled, enable it
			if (es1 ^ es2) and (do_count_asymm is False):
				do_count_asymm = True

			#if the asymmetry has crossed the threshold, stop
			if asymm_um >= self.autocontact_max_asymm_um:
				self.logger.error(f"Maximum asymmetry ({self.autocontact_max_asymm_um} um) "
				"between edge sensors reached!")
				height_result_um = None
				found = False
				break

			#if both edge sensors are on, the procedure has been completed
			if (es1 is True and es2 is True):
				self.logger.info(f"Found contact height: {height_um} um")
				height_result_um = height_um
				found = True
				break

			#applying the step to the current contact height before the next
			#iteration
			height_um += self.autocontact_step_um

		#no height has been found after all steps
		if found is False:
			self.logger.error("Found no correct contact height!")

		#going back to separation height in order to break the oxide layer more
		#easily after re-enabling the overtravel
		self.logger.info("Going back to separation height")
		self.probe_station.separate()

		#enabling overtravel again
		self.logger.info("Enabling overtravel again")
		self.probe_station.enable_overtravel()

		#enabling soft contact again if available for the probe station
		self.logger.info("Enabling soft contact again if available")
		try:
			self.probe_station.enable_soft_contact()
		except NotImplementedError:
			self.logger.warning("Soft contact unavailable for this probe station")

		return height_result_um

	def main(self):
		"""
		Wafer probing main: loops on all requested dies and performs the enabled
		tests.

		If the `test_chips` attribute is True, an instance of ChipTester is 
		created for the chip under test. If test_chips is False, no chip-level 
		testing is performed and this method performs a so-called wafer travel.

		The logging information from ChipTester is added to the WaferTester 
		logger.
		"""

		#logging
		current_col, current_row = self.probe_station.get_die()
		self.logger.info(f"Starting testing of wafer {self.wafer_str}")
		self.logger.info(f"Chip type: {self.chip_type}")
		self.logger.info(f"Chip count: {self.n_chips_wafer}")
		self.logger.info(f"Chips to test: {self.n_chips_test}")
		self.logger.info(f"Starting die: {self.start_column}-{self.start_row}")
		self.logger.info(f"Current position: {current_col}-{current_row}")

		#store the starting time in the `data` attribute
		time_start = time.time()
		time_fmt_str = "%Y-%m-%d %H:%M:%S"
		start_time_struct = time.localtime(time_start)
		self.data["start_time"] = time.strftime(time_fmt_str, start_time_struct)
		self.data["stop_time"] = None
		self.data["elapsed_time"] = 0

		#writing `data` attribute to JSON summary file
		self.dump_data()

		#loop on requested dies
		for col, row in self.dies_list:
			#getting the status of the probe station before doing anything else
			try:
				status_prober = self.verify_prober_status()
			except Exception:
				self.logger.exception("Unexpected problem when checking prober status!")
				self.logger.warning(f"Skipping testing of chip 0x{col:X}{row:X}")
				continue

			#checking the prober status
			if status_prober is not ProberStatus.GOOD:
				self.logger.error(f"Probe station in bad state ({status_prober.name})!")
				self.logger.warning(f"Skipping testing of chip 0x{col:X}{row:X}")
				continue

			#getting the status of the WPAC
			try:
				status_wpac = self.verify_wpac_status()
			except Exception:
				self.logger.exception("Unexpected problem when checking WPAC status!")
				self.logger.warning(f"Skipping testing of chip 0x{col:X}{row:X}")
				continue

			#checking the WPAC status
			if status_wpac is not WpacStatus.GOOD:
				self.logger.error(f"WPAC in bad state ({status_wpac.name})!")
				self.logger.warning(f"Skipping testing of chip 0x{col:X}{row:X}")
				continue

			#checking for stuck edge sensors
			if self.probe_station.is_in_contact() is False:
				#checking the edge sensors status: they should be **off**
				if self.is_es_state_good() is False:
					self.logger.error("Edge sensors are signalling contact in "
					"separation, assuming stuck edge sensor(s)!")
					#performing a needle cleaning procedure to fix it
					self.logger.warning("Cleaning needles to fix the stuck edge sensor!")
					try:
						self.probe_station.clean_needles()
						self.autoclean_cntr = 0
						self.clean_cntr += 1
					except Exception:
						self.logger.exception("Could not clean the needles!")
						break

			#moving the probe station to the correct die
			try:
				self.move_to_chip(col, row)
			except AutomationError:
				self.logger.error(f"Pattern matching failed for chip 0x{col:x}{row:x}!")
				continue
			except InvalidDieError:
				self.logger.error(f"The requested die (0x{col:x}{row:x}) is not valid! ")
				continue
			except SoftwareDieError:
				self.logger.error("Provided die coordinates are not valid "
					f"(0x{col:X}{row:X})!")
				continue
			except CriticalProberError:
				self.logger.exception("Critical prober error, aborting testing!")
				break
			except ProberError:
				self.logger.exception("Skipping chip due to unexpected probe station error!")
				continue
			except Exception:
				self.logger.exception("Unexpected movement error!")
				continue
			except KeyboardInterrupt:
				self.logger.error("Received a keyboard interrupt whilst moving "
					"the chuck!")
				raise

			#getting the current die
			chip_column, chip_row = self.probe_station.get_die()
			self.logger.info(f"Position: chip {chip_column:X}{chip_row:X}")

			#booleans to control whether to go to separation height after
			#testing a chip or not
			is_last_die = ((col, row) == self.dies_list[-1])
			is_separation_enabled = ((not is_last_die) or
				(is_last_die and self.separate_after_test))

			#store the current die in the wafer JSON
			self.data["current_col"] = self.current_col
			self.data["current_row"] = self.current_row
			self.dump_data()

			#if chip testing is disabled, just log it and go to the next die
			if self.test_chips is False:
				self.logger.info("Skipping testing of chip "
					f"{chip_column:X}{chip_row:X}")
				continue

			################
			# autocleaning #
			################

			#if the autoclean counter interval has been reached and testing is
			#enabled, clean the needles before testing the chip
			cleaning_conditions = [
				self.autoclean_cntr >= self.autoclean_interval,
				self.autoclean_enabled
			]
			if all(cleaning_conditions):
				self.logger.info("Performing periodic cleaning of the needles")
				try:
					self.probe_station.clean_needles()
					self.autoclean_cntr = 0
					self.clean_cntr += 1
				except Exception:
					self.logger.exception("Could not clean the needles!")
					break

			################
			# chip testing #
			################

			#chip testing is performed here if it is enabled
			try:
				#test the chip
				self.test_chip(go_to_separation=is_separation_enabled)
				#increase the autoclean counter
				self.autoclean_cntr += 1
				#update the elapsed time in the JSON
				elapsed_time_part_s = time.time() - time_start
				self.data["elapsed_time"] = float(f"{elapsed_time_part_s:.3f}")
				self.dump_data()
			#checking for a die-skipping condition
			except SkipChip:
				self.logger.warning("Skipping die")
				continue
			#checking for high contact resistance
			except HighContactResistance:
				do_continue_testing = self.cres_handler()
				if do_continue_testing is False:
					self.logger.error("Testing has been aborted due to high "
						"contact resistance!")
					break
			#checking for a probing error
			except ProbingError:
				self.logger.error("A probing error has been detected, skipping "
					"chip!")
			#checking for other problems
			except (AbortWaferprobing, KeyboardInterrupt):
				self.logger.error("Waferprobing has been aborted!")
				break

			#######################################
			# checking the scan chain test status #
			#######################################

			#nota bene: IndexError is raised if [i] is used on an empty list
			if len(self._sct_errors) < 2:
				self.logger.info("Not enough data to verify scan chain status")
				continue

			#if either of the last chips has no data, skip
			if self._sct_errors[-1] is None or self._sct_errors[-2] is None:
				self.logger.info("Missing scan chain results: cannot verify status")
				continue

			#conditions to identify a failing scan chain test
			stuck_sct_conds = [
				len(self._sct_errors[-1]) != 0,
				len(self._sct_errors[-2]) != 0,
				self._sct_errors[-1] == self._sct_errors[-2]
			]

			#check if all conditions have been triggered and reload vectors
			if all(stuck_sct_conds) is True:
				self.logger.error("Stuck scan chain detected!")
				self.logger.info("Reloading the scan chain test vectors")
				#reloading the scan chain test vectors
				load_sct_vectors()

		#get the elapsed time
		time_stop = time.time()
		elapsed_time_s = time_stop - time_start
		elapsed_time_h = elapsed_time_s / 3600.

		#store the elapsed time
		stop_time_struct = time.localtime(time_stop)
		self.data["stop_time"] = time.strftime(time_fmt_str, stop_time_struct)
		self.data["elapsed_time"] = float(f"{elapsed_time_s:.3f}")
		self.dump_data()

		#final logging
		self.logger.info(f"Completed testing of wafer {self.wafer_str}")
		self.logger.info(f"Elapsed time [h]: {elapsed_time_h:2.2f}")


if __name__ == "__main__":
	#getting the configuration for the starting position and n. of chips to test
	starting_die = None
	n_chips_test = None
	if len(WAFERTESTER_CONF["dies_list"]) == 0:
		starting_col = WAFERTESTER_CONF["start_column"]
		starting_row = WAFERTESTER_CONF["start_row"]
		n_chips_test = WAFERTESTER_CONF["n_chips_test"]
	else:
		starting_col = (WAFERTESTER_CONF["dies_list"][0] & 0xF0) >> 4
		starting_row = (WAFERTESTER_CONF["dies_list"][0] & 0x0F)
		n_chips_test = len(WAFERTESTER_CONF["dies_list"])
	starting_die = f"0x{starting_col:X}{starting_row:X}"

	#getting wafer information
	batch_id = WAFERTESTER_CONF["batch_id"]
	wafer_id = WAFERTESTER_CONF["wafer_id"]

	#getting the disabled tests
	disabled_tests = []
	expected_disabled = ["lanes_test", "adc_calibration_ldo",
		"shunt_iv_alternative_slope", "ldo_iv"]
	for test_name, is_enabled in CHIPTESTER_CONF["enabled_tests"].items():
		if is_enabled is True:
			continue
		disabled_tests.append(test_name)

	#summarising configuration before starting
	separator_len = 30
	print("\n#####################")
	print("# CROC waferprobing #")
	print("#####################\n")
	print("Please review testing configuration before continuing")
	print("-" * separator_len)
	print("* Waferprobing site:", WAFERTESTER_CONF["probing_site"].value)
	print("-" * separator_len)
	print("* ASIC:", WAFERTESTER_CONF["chip_type"])
	print("* Batch ID:", batch_id)
	print("* Wafer ID:", wafer_id)
	print("* Starting position:", starting_die)
	print("* Number of chips to test:", n_chips_test)
	print("-" * separator_len)
	print("* Autoclean:", WAFERTESTER_CONF["autoclean_enabled"])
	print("* Autoclean interval:", WAFERTESTER_CONF["autoclean_interval"])
	print("-" * separator_len)
	print("* Overtravel [um]:", WAFERTESTER_CONF["overtravel_um"])
	print("* Autocontact:", WAFERTESTER_CONF["use_autocontact"])
	print("* Autocontact starting height [um]:", WAFERTESTER_CONF["autocontact_z_start_um"])
	print("* Autocontact step [um]:", WAFERTESTER_CONF["autocontact_step_um"])
	print("-" * separator_len)
	print("* Operator:", WAFERTESTER_CONF["metadata"]["operator"])
	print("* Notes:", WAFERTESTER_CONF["metadata"]["notes"])
	print("-" * separator_len)
	print("* Output directory:", DATA_DIR)
	print("-" * separator_len)
	print("* Disabled tests:", len(disabled_tests))

	#checking that all important tests are enabled
	if disabled_tests != expected_disabled:
		print("* WARNING: NOT ALL NEEDED TESTS ARE ENABLED!")
	print("-" * separator_len)

	#asking to continue
	if input("Do you want to continue? [yN]:\n>") not in ["y", "Y"]:
		sys.exit("Aborting waferprobing run")

	#checking whether the results folder already exists (wafer already tested)
	wafer_dir = os.path.join(DATA_DIR, f"wafer_{batch_id}-{wafer_id}")
	if os.path.isdir(wafer_dir) is True:
		print(f"Warning! The output folder for wafer {batch_id}-{wafer_id} "
			"already exists! This wafer might have been tested already")
		if input("Do you want to continue? [yN]:\n>") not in ["y", "Y"]:
			sys.exit("Aborting waferprobing run")

	#WaferTester object initialization
	try:
		wafer_tester = WaferTester(**WAFERTESTER_CONF)
	except Exception as e:
		sys.exit("An exception has occurred while trying to initialize a "
			f"WaferTester instance. Error: {e}")

	#running the main() method
	try:
		wafer_tester.main()
	except Exception as e:
		print(f"Caught exception from main(): {e}")
	except KeyboardInterrupt:
		print("Caught a keyboard interrupt, aborting testing!")
	finally:
		#try closing the WaferTester object whether the main method raised an
		#exception or not
		try:
			wafer_tester.close()
		except Exception as e:
			sys.exit("An exception has occurred while trying to close a "
				f"ChipTester instance. Error: {e}")
