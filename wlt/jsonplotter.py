#!/usr/bin/env python3
#pylint: disable = E1133, E1135, E1136, E1137, E1101, W0621, R0201, C0121
"""
Module for plotting data from the JSON log files produced by chiptester.

The JsonPlotter class processes sequentially all JSON inputs and plots
distributions of specific fields according to the input configuration.

The module can be executed from the command line providing the list of
input JSON files and a plotting configuration file.

"""


import json
import os
import sys
import math
import shutil

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.ticker as mticker

import ROOT as R

from PyPDF2 import PdfFileMerger
from PyPDF2 import PdfFileReader

from cycler import cycler

import wlt.config
from wlt.duts import get_wafer
from wlt.analysis.utils import parse_filename, parse_json_in
from wlt.analysis.utils import is_iterable, dict_data_by_path, obj_by_path, optimal_binsize
from wlt.analysis.configuration import load_config


class JsonPlotter:
	"""Performs plotting of input JSON data according to the configuration dictionary"""

	CHOICES_AGGR = [
		'overlay',          # X:Y graphs overlaid on top of each other
		'histogram',        # single values plotted in a histogram along the X axis
		'histograms',       # multiple sets of values plotted as histograms along the X axis
		'map',              # wafer map with one value per chip
		'graph_ordered',    # single values plotted in a graph ordered along the Y axis
	]

	COLORS = ['#0f4b5e', '#118ab2', '#06d6a0', '#ffd166', '#ef476f']
	COLORS_COUNTS = {-1: 'grey', 0: '#06d6a0', 1: '#ffd166', 2: '#ef476f', 3: '#ef476f'}
	REGION_COLORS = {-1: 'grey', 0: 'white', 1: 'gold', 2: 'lightcoral'}
	STATUS_NAMES = {-1: 'grey', 0: 'green', 1: 'yellow', 2: 'red'}
	MARKERS = ['o', 's', 'D', 'v', '^']
	HATCHES = ['///', '\\\\\\', '//', '\\\\', 'o']
	DEFAULT_COLOR = COLORS[0]


	def __init__(self, config=None, data=None, out_dir=None, verbose=0, dry_run=False):
		self.config = config
		self.data = data if data is not None else {}
		self.out_dir = './' if out_dir is None else out_dir
		self.n_plotted = {
			'chip': 0,
			'wafer': 0
		}
		self.verbose = verbose
		self.dry_run = dry_run

		# Defining the plotting objects
		self.figsize = (6, 6)
		self.fig = None
		self.ax = None
		self.cycler_hist = None
		self.cycler_graph = None

		# Setting the data structures for plotting
		self.reset_plot_data()
		self.chip_statuses = None
		self.cache = {}
		self.output_cache = {}
		self.stat_regions = None
		self.stat_data = {}
		self.db_data = {}

		# Defining the plot ordering structures
		self.order_waferwise = {0: [], 1: []}
		self.order_chipwise = []
		self.order_low_priority = {}
		self.configs_sorted = False

		# Hiding ROOT outputs
		R.gROOT.SetBatch(True)
		if not verbose:
			R.gErrorIgnoreLevel = R.kWarning


	def output_path(self, name, file_id=None, extension=None, root=True):
		"""Generates the path where to store the output plot"""

		out_path_template = self.config['GLOBAL']['out_path_template_aggregate']
		if file_id is not None:
			out_path_template = self.config['GLOBAL']['out_path_template']
		if root:
			out_path_template = '{name}.pdf'
		config = self.plot_data['config']
		if config is not None and 'out_path_template' in config:
			out_path_template = config['out_path_template']
		if file_id is not None:
			scan_info = self.data[file_id]
			out_path = out_path_template.format(name=name, chip_row=scan_info['chip'][1],
			                                    chip_column=scan_info['chip'][0])
		else:
			out_path = out_path_template.format(name=name)

		# Replacing the file extension
		if extension is not None:
			out_path = os.path.splitext(out_path)[0] + extension

		return os.path.join(self.out_dir, out_path)


	def load_config(self, config_path, regions_path=None):
		"""Loads the plotting configuration from a Python file"""

		wlt.analysis.configuration.REGIONS_FILE = regions_path
		# Interpreting path as configuration ID if such file doesn't exist
		if isinstance(config_path, str) and '/' in config_path:
			self.config = load_config(config_path=config_path)
		else:
			self.config = load_config(version=config_path)


	def load_data(self, file_in):
		"""Loads JSON data from a single input file"""

		if not os.path.isfile(file_in):
			print(f'ERROR: No input file found: {file_in}')
			sys.exit(1)
		# Extracting scan information from the filename
		scan_info = parse_filename(file_in)
		if not scan_info:
			sys.exit(1)
		self.data[file_in] = scan_info
		# Adding test data of the scan
		with open(file_in, encoding='utf-8') as json_in:
			json_data = json.load(json_in, object_hook=parse_json_in)
			self.data[file_in].update(json_data)


	def reset_plot_data(self):
		"""Clears the data structure for plotting"""

		self.plot_data = {
			'name': None,
			'config': None,
			'file_id': None,
			'data_in': None,
			'data': None,
			'data_sum': None,
			'stats': None
		}


	def sort_configs(self):
		"""Orders configurations according to their group ID and name"""
		# Doing nothing if configs are already sorted
		if self.configs_sorted:
			return
		for name, config in self.config.items():
			if name == 'GLOBAL':
				continue
			if 'skip' in config and config['skip']:
				continue
			if 'aggregate' in config:
				if 'regions' not in config:
					continue
				prt = config['priority']
				if prt >= 0:
					self.order_waferwise[0].append(name)
					if prt >= 1:
						self.order_waferwise[1].append(name)
				else:
					if prt not in self.order_low_priority:
						self.order_low_priority[prt] = []
					self.order_low_priority[prt].append(name)
			else:
				self.order_chipwise.append(name)
		# Sorting configurations: positive groups first
		for prt, order in self.order_waferwise.items():
			order.sort(key=lambda n: [
			               int(self.config[n]['group'] < 0),
			               self.config[n]['group']
			               ] + n.split('_')[::-1])
			# Protecting the order from future changes
			self.order_waferwise[prt] = tuple(order)

			# Assigning order index to the configurations
			for idx, name in enumerate(self.order_waferwise[prt]):
				if prt == 0:
					self.config[name]['idx'] = idx
				else:
					self.config[name][f'idx{prt}'] = idx
		# Marking configs as sorted to not repeat this process
		self.configs_sorted = True


	def process_stats(self, intermediate=False):
		"""Produces the statistical information about all the files used for the plot"""

		# Unpacking the relevant information
		config, data, data_sum = (self.plot_data[n] for n in ['config', 'data', 'data_sum'])

		# Creating the overview dictionaries
		config_overlay = None
		chip_data = {}
		chip_status = {}

		# Collecting extra summary information
		if not 'aggregate' in config or config['aggregate'] not in ['histogram', 'graph_ordered']:
			return chip_data, chip_status
		chip_data = dict(data.items())
		# Ensuring that chip data has a single number per chip
		for k, v in chip_data.items():
			if isinstance(v, dict) and 'value' in v:
				chip_data[k] = v['value']

		# Calculating the statistics
		# Excluding unphysical values
		values = np.array([v for v in chip_data.values() if v not in [None, {}, []] and np.isfinite(v)])

		# Calculating the stats and storing to the global dictionary
		stats = {}
		if len(values) > 0:
			stats.update({
				'MIN': min(values),
				'MAX': max(values),
				'AVG': np.mean(values),
				'MED': np.median(values),
				'RMS': np.std(values),
			})
			# Updating statistical values by rejecting underflows and overflows
			if 'regions' in config:
				regions = config['regions']
				values = values[(values >= regions[0]) & (values <= regions[-1])]
				if len(values) > 0:
					stats.update({
							'AVG': np.mean(values),
							'MED': np.median(values),
							'RMS': np.std(values),
						})
			# Updating statistical values by rejecting outliers (outside of 5 sigma)
			n_cycles = 5
			n_sig = 5
			sel = np.ones(len(values), dtype=bool)
			for _ in range(n_cycles):
				n_before = np.count_nonzero(sel)
				sel = (values > (stats['MED'] - n_sig*stats['RMS'])) & (values < (stats['MED'] + n_sig*stats['RMS']))
				n_after = np.count_nonzero(sel)
				# Updating the stats
				if any(sel):
					stats.update({
						'AVG': np.mean(values[sel]),
						'MED': np.median(values[sel]),
						'RMS': np.std(values[sel]),
					})
				# Stopping if no outliers rejected in the last cycle
				if n_after == n_before:
					break
			# Calculating the relative RMS
			stats['ROM'] = stats['RMS'] / stats['MED'] * 100 if stats['MED'] != 0.0 else 0.0
			stats['ROM'] = abs(stats['ROM'])
			# Converting stats to rounded float values
			stats = {name: round(float(value), 6) for name, value in stats.items()}
			# Storing stats globally to the plotting data
			self.plot_data['stats'] = stats
			self.stat_data[self.plot_data['name']] = stats

		# No further drawing if only intermediate statistics is needed
		if intermediate:
			return chip_data, chip_status

		if 'overlay.histogram' in self.config['GLOBAL']:
			config_overlay = self.config['GLOBAL']['overlay.histogram']

		# Counting the numbers of chips in each region
		if 'regions' in config:
			regions = config['regions']
			if len(regions) != 6:
				print('ERROR: The `regions` should have exactly 6 boundaries')
				sys.exit(3)
			for k, v in chip_data.items():
				# Finding the region corresponding to the plotted data value of the chip
				region_idx = -1
				# Marking chip as 'no data' by default
				if v is None:
					chip_status[k] = region_idx
					continue
				for idx, v_low in enumerate(regions[:-1]):
					if v_low <= v < regions[idx+1]:
						region_idx = idx
						break

				# Converting the index to status ID
				# -1 - grey; 0 - green; 1 - yellow; 2 - red; 3 - outside;
				region_idx = abs(region_idx - 2)
				chip_status[k] = region_idx

			# Counting occurances of each region
			counts = {}
			for status in self.COLORS_COUNTS:
				counts[status] = list(chip_status.values()).count(status)
			# Showing outlier counts only if there is at least one
			if 3 in counts and counts[3] < 1:
				del counts[3]
			# Storing chip counts to summary data
			data_sum['counts'] = counts
			stats['counts'] = counts

			# Displaying the chip-counts
			if config_overlay and 'counts' in config_overlay and config_overlay['counts']:
				for status, v in counts.items():
					line_offset = status if status >= 0 else max(counts.keys()) + abs(status)
					txt = str(v) if status < 3 else '+'+str(v)
					if status == 3 and v < 1:
						continue
					txt = [] + ['\n']*line_offset + [txt]
					txt = ''.join(txt)
					self.ax.text(1.005, 1.0, txt,
					             horizontalalignment='left', verticalalignment='top',
					             transform=self.ax.transAxes, family='monospace', weight='bold',
					             color=self.COLORS_COUNTS[min(status, 2)])

		# Drawing the statistics box
		if config_overlay and 'AVG' in stats and 'stats' in config_overlay and config_overlay['stats']:
			txt = ''
			for key in ['MIN', 'MAX', 'AVG', 'MED']:
				if key in config_overlay['stats']:
					txt += f'{key}: {stats[key]:.3f}\n'
			if len(values) > 0 and 'RMS' in config_overlay['stats']:
				txt += f'RMS: {stats["RMS"]:.3f}\n'
				if 'ROM' in stats:
					txt += f'     {stats["ROM"]:.1f}%'
			self.ax.text(0.01, 0.99, txt, family='monospace', fontsize=9,
						 horizontalalignment='left', verticalalignment='top', transform=self.ax.transAxes)

		return chip_data, chip_status


	def set_stat_regions(self, regions):
		"""Sets the boundaries of regions in multiples of RMS"""
		if not regions:
			return
		# Validating the specified boundaries
		if len(regions) not in [2,4,6]:
			raise RuntimeError('ERROR: --stat_regions must have 2, 4 or 6 boundaries')
		print('WARNING: Forcing all regions to be defined based on RMS: ', regions)
		print('         Definitions from the configuration file will be ignored...')
		self.stat_regions = regions


	def calc_stat_regions(self):
		"""Calculates region boundaries based on statistical distributions"""

		self.process_stats(intermediate=True)
		config, stats = (self.plot_data[n] for n in ['config', 'stats'])

		# Doing nothing if the region is fixed
		if 'regions.fixed' in config and config['regions.fixed']:
			return

		if 'aggregate' in config and config['aggregate'] == 'histogram':
			if stats is not None:
				med, rms = stats['MED'], stats['RMS']
				if rms == 0.0:
					rms = 1.0
				# Defining the boundaries based on the defined RMS multiples
				regions = [med + rms * mult for mult in self.stat_regions]
				# Optimising the boundaries and binning
				config['bins'] = optimal_binsize(regions)
				for i_v, v in enumerate(regions):
					# Deciding in which direction to round the boundary position
					shift = 1
					if v < 0:
						shift *= -1
					if i_v < len(regions) / 2:
						shift *= -1
					# Rounding the boundary value to the binning
					if shift > 0:
						regions[i_v] = math.ceil(v / config['bins']) * config['bins']
					else:
						regions[i_v] = math.floor(v / config['bins']) * config['bins']
				# Updating region definitions globally
				config['regions'] = regions


	def pre_plotting(self):
		"""Apply additional processing of the data before plotting (extended in WaferAnalyzer)"""
		# Redefining region boundaries based on statistical distributions
		if self.stat_regions:
			self.calc_stat_regions()

		# Unpacking the relevant information
		config = self.plot_data['config']

		# Optimising the binning if it's not set
		if 'bins' not in config and 'regions' in config:
			config['bins'] = optimal_binsize(config['regions'])

		# Ensuring that only real regions are defined
		if 'regions' in config:
			if config['regions'] is None:
				config.pop('regions')
			# Ensuring that all regions have the right dimensionality
			reg = config['regions']
			# 2 boundaries: green only
			if len(reg) == 2:
				config['regions'] = (reg[0], reg[0], reg[0], reg[1], reg[1], reg[1])
			# 4 boundaries: green + red
			elif len(reg) == 4:
				config['regions'] = (reg[0], reg[1], reg[1], reg[2], reg[2], reg[3])
			elif len(reg) != 6:
				raise RuntimeError(f'WARNING: Wrong regions definition: {reg}')

		# Generating X bins if configured
		if 'bins' in config:
			if config['bins'] is None:
				config.pop('bins')
			else:
				bins = config['bins']
				if not is_iterable(bins):
					if 'regions' in config:
						regions = config['regions']
						bins = np.arange(min(regions), max(regions)+0.5*bins, bins).astype(np.float32)
						# Building a log-scale 
						if 'logX' in config and config['logX']:
							bins_log = []
							if 0 in bins:
								bins_log.append(0)
							nonzero_min = min(bins[bins > 0])
							for p in np.logspace(-7, 7, 15):
								for n in np.arange(1, 10):
									v = n*p
									if v < nonzero_min and (abs(v/nonzero_min) < 0.999):
										continue
									if v > bins[-1]:
										break
									bins_log.append(v)
							bins = np.array(bins_log, dtype=np.float32)
						config['binning'] = bins
					elif 'axis.range' in config:
						rng = config['axis.range'][0]
						bins = np.arange(rng[0], rng[1]+0.5*bins, bins).astype(np.float32)
						config['binning'] = bins
					else:
						config.pop('bins')

		# Adding statistical information if not done yet
		self.process_stats()


	def apply_styles(self):
		"""Apply global styling to all the plots"""

		plt.style.use('seaborn-muted')
		self.fig = plt.figure(figsize=self.figsize, clear=True)
		self.ax = self.fig.add_subplot()
		# Setting axes parameters
		self.ax.set_autoscaley_on(True)
		self.ax.set_axisbelow(True)
		# Setting the axis label to scientific notation
		ax_formatter = mticker.ScalarFormatter(useMathText=True)
		ax_formatter.set_powerlimits((-2,2))
		self.ax.yaxis.set_major_formatter(ax_formatter)
		self.ax.xaxis.set_major_formatter(ax_formatter)


		# Building a cycler
		self.cycler_hist = (cycler(color=self.COLORS) +
		                    cycler(marker=self.MARKERS[0:1]*len(self.COLORS)) +
		                    cycler(hatch=self.HATCHES))
		self.cycler_graph = (cycler(color=self.COLORS) +
		                     cycler(marker=self.MARKERS[0:1]*len(self.COLORS)))
		for i in range(1, len(self.COLORS)):
			self.cycler_hist = self.cycler_hist.concat(cycler(color=self.COLORS) +
			                                           cycler(marker=self.MARKERS[i:i+1]*len(self.COLORS)) +
			                                           cycler(hatch=self.HATCHES))
			self.cycler_graph = self.cycler_graph.concat(cycler(color=self.COLORS) +
			                                             cycler(marker=self.MARKERS[i:i+1]*len(self.COLORS)))

		# Configuring the legend position
		if 'legend.loc' in self.config['GLOBAL']:
			plt.rcParams.update({'legend.loc': self.config['GLOBAL']['legend.loc']})


	def merge_summary(self):
		"""Generates summary PDFs merging together the plots from individual tests"""

		out_dir = os.path.join(self.out_dir, 'summary')
		if not os.path.isdir(out_dir):
			os.makedirs(out_dir, exist_ok=True)

		# Processing the wafer-wise summary plot
		if self.n_plotted['wafer'] > 0:
			in_dir = self.out_dir
			obj_merged = PdfFileMerger()
			names = [name for name, config in self.config.items()
			              if 'aggregate' in config
			              and config['aggregate'] in ['histogram', 'graph_ordered']]
			names.sort(key=lambda s: [self.config[s]['group']] + s.split('_')[::-1])

			for name in names:
				file_path = os.path.join(in_dir, f'{name}.pdf')
				if os.path.isfile(file_path):
					obj_merged.append(PdfFileReader(file_path, 'rb'))

			print(f'Merging wafer-wise plots: {len(names)}')
			obj_merged.write(os.path.join(out_dir, 'wafer_wise.pdf'))
			obj_merged.close()

		# Processing the per-chip plots
		if self.n_plotted['chip'] > 0:
			in_dir = os.path.join(self.out_dir, 'chips')
			obj_merged = PdfFileMerger()
			names = [name for name, config in self.config.items()
			              if 'aggregate' not in config or config['aggregate'] != 'histogram']
			names.sort(key=lambda s: s.split('_'))

			print(f'Merging chip-wise plots: {len(names)}')
			for name in names:
				file_path = os.path.join(in_dir, f'{name}.pdf')
				if os.path.isfile(file_path):
					obj_merged.append(PdfFileReader(file_path, 'rb'))

			obj_merged.write(os.path.join(out_dir, 'chip_wise.pdf'))
			obj_merged.close()


	def scan_info_dict(self, file_id, summary_data=None):
		"""Returns a dictionary with scan information for a file to be used in string formatting"""

		if file_id not in self.data:
			print(f'ERROR: No scan information available for file: {file_id}')
			sys.exit(1)
		scan_info = {
			'batch': self.data[file_id]['batch'],
			'wafer': self.data[file_id]['wafer'],
			'chip_row': self.data[file_id]['chip'][1],
			'chip_column': self.data[file_id]['chip'][0],
			'scan_time': self.data[file_id]['scan_time'],
		}
		# Overriding some information with data from GLOBAL configuration
		if 'override.scan_info' in self.config['GLOBAL']:
			scan_info.update(self.config['GLOBAL']['override.scan_info'])
		# Adding summary data for this file
		if summary_data and file_id in summary_data:
			for k, v in summary_data[file_id].items():
				if k in scan_info:
					if self.verbose:
						print(f'WARNING: summary name `{k}` reserved for the input file identification.')
						print('Ignoring...')
					continue
				scan_info[k] = v

		return scan_info


	def fill_db_data(self, data=None):
		"""Filling the chip data into the dictionary of data for DB"""

		chip_id = self.plot_data['chip_id']
		scan_info = self.scan_info_dict(self.plot_data['file_id'])
		# Constructing the label of the chip
		wafer = get_wafer(scan_info['batch'], scan_info['wafer'])
		if not wafer:
			raise RuntimeError(f"Failed retrieving wafer ID from file: {self.plot_data['file_id']}")
		# Label format: <BatchN>_<WaferID>_<ChipColumn><ChipRow>
		label = f"{wafer.label()}_{chip_id[0]:X}{chip_id[1]:X}"
		# Using chip ID as key in the DB data dictionary
		if chip_id not in self.db_data:
			self.db_data[chip_id] = {
				'CHIP_COL': chip_id[0],
				'CHIP_ROW': chip_id[1],
				'LABEL': label,
				'TEST_START': scan_info['scan_time'].strftime('%Y/%m/%d %H:%M:%S'),
			}
		if not data:
			return
		# Adding the input to the DB dictionary
		for k, v in data.items():
			if isinstance(v, dict):
				# Filtering out undefined points of the dictionary
				v_filtered = {kk: vv for kk, vv in v.items() if None not in [kk, vv]}
				if not v_filtered:
					continue
				if k in self.db_data[chip_id]:
					self.db_data[chip_id][k].update(v_filtered)
				else:
					self.db_data[chip_id][k] = v_filtered
			else:
				self.db_data[chip_id][k] = v


	def write_db_data(self):
		"""Writing a JSON file with data for uploading to DB"""

		out_dir = self.output_path('db', extension='')
		if not os.path.isdir(out_dir):
			os.makedirs(out_dir, exist_ok=True)

		for chip_id, data in self.db_data.items():
			out_path = self.output_path('db/chip_{0:X}{1:X}'.format(*chip_id), extension='.json')
			# Ensuring that all data for DB has appropriate format
			for k, v in data.items():
				if isinstance(v, float) and not math.isfinite(v):
					data[k] = None
			with open(out_path, 'w', encoding='utf-8') as f_out:
				json.dump(data, f_out, indent='\t')
		print(f'DB data saved to: {out_dir}')


	def write_regions(self):
		"""Writing statistics-based region definitions to JSON"""

		# Building a dictionary of available region definitions
		regions = {}
		for name, config in self.config.items():
			if 'skip' in config and config['skip']:
				continue
			if 'regions' not in config:
				continue
			regions[name] = config['regions']
		# Building an ordered list of config names
		names = list(self.order_waferwise[0])
		for _, names_lp in self.order_low_priority.items():
			names += names_lp
		# Writing out available regions for the defined names
		group_id_pre = None
		# Python format
		out_path = self.output_path('_region_definitions', extension='.py')
		out_py = open(out_path, 'w', encoding='utf-8')
		print('# "test name": ([region thresholds], <bin size>,  <group id>,  <priority>,  "Title;X-title;Y-title")', file=out_py)
		# CSV format
		out_path = self.output_path('_region_definitions', extension='.csv')
		out_csv = open(out_path, 'w', encoding='utf-8')
		print('REGIONS = {', file=out_py)
		print('Test name, Red <, Yellow <, Green <, > Green, > Yellow, > Red, Bin size, Group, Priority, "Title;X-title;Y-title", Green, Yellow, Red', file=out_csv)
		for name in names:
			if name not in regions:
				continue
			region = regions[name]
			strs = [f'{v:.5G}' for v in region]
			# Switching to scientific notation if standard is too long or ambiguous
			if any((len(v) > 6 for v in strs)):
				strs = [np.format_float_scientific(v, precision=1,
				                                   exp_digits=1) for v in region]
			strs = ', '.join(strs)
			name_str = f'"{name}":'
			group_id = self.config[name]['group']
			priority_id = self.config[name]['priority']
			bin_size = self.config[name]['bins']
			title = "" if 'title' not in self.config[name] else self.config[name]['title']
			if is_iterable(bin_size):
				bin_size = bin_size[1] - bin_size[0]
			# Separating groups by a blank line
			if group_id_pre is not None and group_id != group_id_pre:
				print(file=out_py)
			group_id_pre = group_id
			n_red, n_yellow, n_green = 0, 0, 0
			if name in self.stat_data and 'counts' in self.stat_data[name]:
				n_red = self.stat_data[name]['counts'][2]
				n_yellow = self.stat_data[name]['counts'][1]
				n_green = self.stat_data[name]['counts'][0]
			# Writing the region definition
			print(f'\t{name_str:<45} ([{strs:<48}],  {bin_size:>5.0e},  {group_id:>2d}, {priority_id:>2d}, "{title}"),', file=out_py)
			print(f'{name}, {strs}, {bin_size:.0e}, {group_id:d}, {priority_id:d}, "{title}", {n_green}, {n_yellow}, {n_red}', file=out_csv)
		print('}', file=out_py)

		# Closing output files
		out_py.close()
		out_csv.close()
		print('Region definitions saved to:')
		print(f'\t{out_py.name}')
		print(f'\t{out_csv.name}')


	def is_input_cached(self, out_config):
		"""Checks if all the data for calculating output is stored in cache"""
		# Simple outputs are never cached
		if not isinstance(out_config, dict):
			return False
		# Checking if at least one key is not cached
		for key in out_config:
			if key.startswith('+'):
				continue
			if key.startswith('*'):
				ckey = (self.plot_data['file_id'], key)
				if ckey not in self.cache:
					return False
			else:
				return False
		return True


	def calc_output_data(self, data_in, out_config, data_in_raw=None):
		"""Converts the input data dictionary to an output dictionary
		   applying the configured transformation functions"""

		output = {}

		# Enforcing the dictionary format of the configuration
		if not isinstance(out_config, dict):
			out_config = {'x': out_config}
		# Building the output dictionary according to the configuration
		for key, config in out_config.items():
			# Skipping the non-plottable elements created after constructing the output data
			if key.startswith('+'):
				continue
			# Recovering the item from cache if present
			ckey = (self.plot_data['file_id'], key)
			if key.startswith('*') and ckey in self.cache:
				output[key] = self.cache[ckey]
				continue
			if isinstance(config, type(lambda:0)):
				# Treating the parameter as a lambda function executed on output (and input) arrays
				try:
					# Trying with just output array
					output[key] = config(data_in)
				except TypeError:
					# Repeating with input and output array
					try:
						output[key] = config(data_in_raw, data_in)
					except:
						if self.verbose:
							print(f'ERROR: Failed calculation for `{key}` in the output dictionary')
						continue
				except KeyboardInterrupt:
					sys.exit(1)
				except:
					if self.verbose:
						print(f'ERROR: Failed calculation for `{key}` in the output dictionary')
			elif isinstance(config, str):
				# Treating the parameter as a key from the input dictionary
				if config not in data_in:
					print(f'ERROR: No `{config}` key in the input dictionary')
					sys.exit(2)
				output[key] = data_in[config]
			else:
				# Assigning the parameter directly as a value
				output[key] = config
			# Caching the calculated value if necessary
			if key.startswith('*') and ckey not in self.cache:
				self.cache[ckey] = output[key]

		# Converting all iterable elements to arrays if it contains a common 'x' value
		if 'x' in output:
			for key, value in output.items():
				# Keeping keys starting with `+` without changes
				if key.startswith('+'):
					continue
				if is_iterable(value):
					output[key] = np.array(list(value))

		# Performing one more step only with non-plottable items
		for k, v in out_config.items():
			if k.startswith('+'):
				if isinstance(v, type(lambda:0)):
					try:
						output[k] = v(data_in, output)
					except KeyboardInterrupt:
						sys.exit(1)
					except:
						if self.verbose:
							print(f'ERROR: Failed calculation for `{k}` in the output dictionary')
				else:
					output[k] = v

		# Inserting input data as output data and returning it as is
		if 'x' not in output:
			return output

		# Constructing the dictionaries with X:Y as key:value pairs
		arr_x = output['x']
		if not is_iterable(arr_x):
			return arr_x

		n_points = len(arr_x)
		data_out = {}
		for key, arr_y in output.items():
			# Skipping the X values that should be the keys
			if key == 'x':
				continue
			# Not a plottable entry: inserting data as is
			if key.startswith('+'):
				data_out[key] = arr_y
				continue

			# Normal X-Y dictionaries
			data_out[key] = {}
			for i in range(n_points):
				data_out[key][arr_x[i]] = arr_y[i]

		return data_out


	def draw_plot(self, skip_if_exists=False):
		"""Draws a single plot with the input data"""

		self.ax.clear()

		# Performing optional pre-processing
		self.pre_plotting()

		# Extracting the plotting data
		file_id, config, data, data_sum = (self.plot_data[n] for n in [
		                                   'file_id', 'config', 'data', 'data_sum'])
		# Checking if the output file already exists
		out_path = self.output_path(self.plot_data['name'], file_id, root=False)
		if os.path.isfile(out_path) and skip_if_exists:
			return
		out_dir = os.path.split(out_path)[0]
		if not os.path.isdir(out_dir):
			os.makedirs(out_dir, exist_ok=True)
		
		# Setting the scaling of X/Y values
		scale = (1.0, 1.0) if 'scale' not in config else config['scale']

		# Setting the log scale if configured
		if 'logY' in config and config['logY']:
			self.ax.set_yscale('log')
		else:
			self.ax.set_yscale('linear')
		if 'logX' in config and config['logX']:
			self.ax.set_xscale('symlog')
			# Setting linear part until the lowest non-zero bin value
			if 'bins' in config:
				if isinstance(config['bins'], (float, int)):
					linthr = config['bins']
					self.ax.set_xscale('symlog', linthresh=linthr)
					self.ax.get_xaxis().set_minor_locator(mticker.SymmetricalLogLocator(subs=range(1, 10), base=10, linthresh=linthr))
				else:
					print(f'WARNING: Wrong bin size defined for a logX linear threshold: {config["bins"]}')

		# Determining the legend styling
		legend_loc = plt.rcParams['legend.loc']
		if 'legend.loc' in config:
			legend_loc = config['legend.loc']
		legend_ncol = 1
		if 'legend.ncol' in config:
			legend_ncol = config['legend.ncol']
		legend = None

		# Determining the binning
		bins = None if 'binning' not in config else config['binning']
		# Setting integer and centered bin labels
		if 'bins.int' in config and config['bins.int'] and bins is not None and 'logX' not in config:
			ticks = [x + 0.5*(bins[i+1] - x) for i, x in enumerate(bins[:-1])]
			labels = [str(int(i)) for i in bins[:-1]]
			# Setting custom labels if defined
			bin_config = config['bins.int']
			nbins = len(bins) - 1
			if isinstance(bin_config, dict):
				for i in range(len(bins)-1):
					if nbins > 20 and i%2 != 0:
						labels[i] = ''
						continue
					v = int(bins[i])
					if v in bin_config:
						labels[i] = bin_config[v]
					else:
						labels[i] = ''
			elif nbins > 20:
				# Removing odd labels if there are too many bins
				del ticks[1::2]
				del labels[1::2]
			self.ax.set_xticks(ticks, labels)

		# Adding axis labels
		labels = ['', '', ''] if 'title' not in config else config['title'].split(';')
		info = None if file_id is None else self.scan_info_dict(file_id)
		plt.title(labels[0])
		if info is not None:
			plt.title(labels[0].format(**info))
		plt.xlabel(labels[1])
		plt.ylabel(labels[2])

		# Checking the structure of data to be plotted
		if isinstance(data, (list, np.ndarray)):
			# Drawing a single histogram
			self.ax.set_prop_cycle(self.cycler_hist)
			x = []
			none_value = None
			if 'missing_value' in config:
				none_value = config['missing_value']
			for v in data:
				if v is None:
					x.append(none_value)
				else:
					x.append(v*scale[0])
			# Plotting the histogram
			plt.hist(x, bins=bins, color=self.DEFAULT_COLOR, edgecolor='white')

		elif isinstance(data, dict):

			if all(isinstance(k, str) for k in data.keys()):
				if 'aggregate' in config and config['aggregate'] == 'histogram':
					# Drawing a summary histogram
					x = np.array([v for v in data.values() if v not in [None, {}, []] and not np.isnan(v)])
					# Applying the X scale
					x =  x*scale[0]
					# Plotting the histogram
					plt.hist(x, bins=bins, color=self.DEFAULT_COLOR, edgecolor='white')

				elif 'aggregate' in config and config['aggregate'] == 'graph_ordered':
					# Drawing a graph with points ordered along Y axis
					self.ax.set_prop_cycle(self.cycler_graph)
					x, y, xerr = [], [], []
					for i_k, (k, v) in enumerate(data.items()):
						if v is None:
							continue
						y.append(i_k*scale[1])
						if isinstance(v, dict):
							x.append(v['value']*scale[0])
							xerr.append(v['error']*scale[0])
						else:
							x.append(v*scale[0])
					if not xerr:
						plt.plot(x, y)
					else:
						plt.errorbar(x, y, xerr=xerr)
					plt.ylim([-1, len(data)+1])

				elif any(isinstance(k, R.TH1) for k in data.values()):
					# Drawing TH1 histograms
					canvas = R.TCanvas('canv', '', self.figsize[0]*100, self.figsize[1]*100)
					if self.ax.get_yscale() == 'log':
						canvas.SetLogy(True)
					R.gStyle.SetOptStat(1111)
					h_id = 0
					for _, histo in data.items():
						if not isinstance(histo, R.TH1):
							continue
						drawopt = '' if h_id == 0 else 'same'
						# Adding titles
						if h_id == 0:
							histo.SetTitle(self.ax.get_title())
							histo.GetXaxis().SetTitle(self.ax.get_xlabel())
							histo.GetYaxis().SetTitle(self.ax.get_ylabel())
						histo.SetLineWidth(1)
						histo.SetLineColor(h_id+1)
						histo.Draw(drawopt)
						# Drawing the associated fit function if present
						fits = histo.GetListOfFunctions()
						if fits.GetEntries() > 0:
							fit = fits.At(0)
							fit.SetLineColor(2)
							fit.SetNpx(300)
							fit.Draw('same')
						h_id += 1
					# Saving the plot
					canvas.SaveAs(out_path)
					return

				elif all(isinstance(v, dict) for k, v in data.items() if not k.startswith('+')):
					# Drawing overlaid graphs
					self.ax.set_prop_cycle(self.cycler_graph)
					leg_handles, leg_labels = [], []
					for g_id, g_data in data.items():
						# Skipping non-plottable items
						if g_id.startswith('+'):
							continue
						scan_id = g_id if file_id is None else file_id
						x, y = [], []
						leg_data = self.scan_info_dict(scan_id, data_sum)
						if g_data is not None:
							for k, v in g_data.items():
								x.append(k*scale[0])
								y.append(v*scale[1])
							graph, = plt.plot(x, y)
							leg_handles.append(graph)
						else:
							leg_handles.append(mpatches.Patch(color='red', hatch='////', fill=False))

						# Setting legend entry if there is data present
						if 'legend' in config:
							if config['legend']:
								leg = config['legend'].format(**leg_data)
								leg_labels.append(leg)
							else:
								leg_labels.append(g_id)

					# Drawing the fit line if configured
					if 'fit.draw' in config:
						for fit_name, fit_data in data.items():
							if not fit_name.startswith('+fit'):
								continue
							# Extracting the fit data
							pars, chi2, p_x, p_y = fit_data
							p_x = [ix*scale[0] for ix in p_x]
							p_y = [iy*scale[1] for iy in p_y]
							# If the fit worked: positive Chi2
							if chi2 > 0.0:
								pfit = np.polynomial.Polynomial(pars)
								# Creating the straight line represeting the fit
								step = (p_x[-1] - p_x[0]) / 200
								x = list(np.arange(p_x[0]*scale[0], p_x[-1]*scale[0] + step, step))
								y = [pfit(ix)*scale[1] for ix in x]
								# Getting the fit color from configuration if set up
								color_idx = 0
								if isinstance(config['fit.draw'], dict):
									color_idx = config['fit.draw'][fit_name]
								plt.plot(x,y, color=self.COLORS[color_idx],
								         marker='', linestyle='--',
								         scaley=False, scalex=False, zorder=1)
								# Drawing white dots over the points actually used in the fit
								plt.plot(p_x, p_y, color='white', linestyle='',
								         marker='.', markersize=1.2)

					if leg_labels:
						legend = plt.legend(leg_handles, leg_labels, loc=legend_loc, ncol=legend_ncol)
						plt.gca().add_artist(legend)

				elif all(isinstance(v, (list, np.ndarray)) for k, v in data.items() if not k.startswith('+')):
					# Drawing overlaid histograms
					self.ax.set_prop_cycle(self.cycler_hist)
					for h_id, h_data in data.items():
						# Skipping non-drawable items
						if h_id.startswith('+'):
							continue
						arr_data = np.array(h_data)*scale[0]
						scan_id = h_id if file_id is None else file_id
						label = scan_id
						if 'legend' in config and config['legend']:
							leg_data = self.scan_info_dict(h_id, data_sum)
							label = config['legend'].format(**leg_data)
						plt.hist(arr_data, bins=bins, label=label, fill=True,
						         histtype='bar', edgecolor='white', alpha=1.0)
					# Drawing fit curves
					if 'fit.draw' in config and config['fit.draw']:
						self.ax.set_prop_cycle(None)
						for fit_name, fit_data in data.items():
							if not fit_name.startswith('+fit'):
								continue
							# Extracting the fit data
							p_x = [ix*scale[0] for ix in fit_data['v_x']]
							p_y = [iy*scale[1] for iy in fit_data['v_y']]
							# If the fit worked: positive Chi2
							if fit_data['chi2'] > 0.0:
								# Setting up the Gaussian parameters
								ffit = R.TF1('fitf', 'gaus', p_x[0], p_x[1])
								ffit.SetParameter(0, fit_data['const'])
								ffit.SetParameter(1, fit_data['mean'])
								ffit.SetParameter(2, fit_data['sigma'])
								# Calculating points of the fit curve
								step = (p_x[-1] - p_x[0]) / 200
								x = list(np.arange(p_x[0]*scale[0], p_x[-1]*scale[0] + step, step))
								y = [ffit.Eval(ix)*scale[1] for ix in x]
								# Getting the fit color from configuration if set up
								color_idx = 0
								if isinstance(config['fit.draw'], dict):
									color_idx = config['fit.draw'][fit_name]
								plt.plot(x, y, color=self.COLORS[color_idx],
								         marker='', linestyle='-',
								         scaley=False, scalex=False, zorder=1)

					if 'legend' in config:
						legend = plt.legend(loc=legend_loc, ncol=legend_ncol)
						plt.gca().add_artist(legend)

			else:
				# Drawing a single graph
				self.ax.set_prop_cycle(self.cycler_graph)
				x, y = [], []
				for k, v in data.items():
					x.append(k*scale[0])
					y.append(v*scale[1])
				plt.plot(x, y)

		# Drawing coloured acceptance regions
		if 'regions' in config and config['regions'] and len(config['regions']) > 2:
			regions = config['regions']
			if len(regions) != 6:
				print('ERROR: The `regions` should have exactly 6 boundaries')
				sys.exit(3)

			# Defining the boundaries for each region
			boundaries = {
				'r': None,
				'y': None,
			}
			boundaries['r'] = (regions[:2], regions[-2:])
			boundaries['y'] = (regions[1:3], regions[-3:-1])

			# Drawing the yellow and red regions while preserving the old Y axis range
			self.ax.relim()
			ylim = self.ax.get_ylim()
			for region, color_code in {'y': 1, 'r': 2}.items():
				color = self.REGION_COLORS[color_code]
				if boundaries[region] is None:
					continue
				for boundary in boundaries[region]:
					rect = mpatches.Rectangle(xy=(boundary[0], -1e6),
					                          width=boundary[1]-boundary[0], height=2e6, alpha=0.35,
					                          fill=True, color=color, linewidth=0)
					self.ax.add_patch(rect)
			plt.xlim((regions[0], regions[-1]))
			plt.ylim(ylim)

		# Overriding axis ranges if defined
		if 'binning' in config:
			plt.xlim(config['binning'][0], config['binning'][-1])
		if 'axis.range' in config:
			x_range, y_range = config['axis.range']
			if x_range:
				plt.xlim(x_range)
			if y_range:
				plt.ylim(y_range)

		# Drawing grid
		plt.grid(True, color='gainsboro', linestyle='--', zorder=0)
		# Reducing the margins
		plt.subplots_adjust(left=0.12, right=0.94, top=0.94, bottom=0.1)

		# Adding a text box with additional information
		if data is not None and 'text' in config and config['text']:
			texts = config['text'] if isinstance(config['text'], list) else [config['text']]
			handles = [None] * len(texts)
			text_data = {}
			if file_id is not None:
				text_data.update(self.scan_info_dict(file_id, data_sum))
			text_data.update(data_sum)
			labels = [text.format(**text_data) for text in texts]
			handles = [mpatches.Patch(color='white', fill=False)] * len(texts)
			text_loc = 'upper left'
			if 'text.loc' in config:
				text_loc = config['text.loc']
			textbox = plt.legend(handles, labels, handlelength=0, handletextpad=0,
			                     loc=text_loc, fontsize='small', framealpha=0.4, fancybox=False)
			plt.gca().add_artist(textbox)

		# Adding a group-ID label if defined
		if 'group' in config:
			self.ax.text(1.0, 1.005, config['group'], transform=self.ax.transAxes,
			             horizontalalignment='right', verticalalignment='bottom')


		# Saving the plot
		plt.savefig(out_path)
		if file_id is None:
			self.n_plotted['wafer'] += 1
		else:
			self.n_plotted['chip'] += 1


	def get_data_in(self, file_id, chip_data, config):
		"""Extract input data according to the plot configuration"""
		data_in = {}
		if isinstance(config, str):
			# Directly using an object for plotting as is
			data_in = dict_data_by_path(chip_data, config)
			if data_in is None:
				if self.verbose:
					print(f"WARNING: Data path `{config}` not found in file {file_id}")
					print( '         Skipping file...')
				return None

		elif isinstance(config, dict):
			# Constructing a configurable set of output values
			for key, path in config.items():
				# Skipping elements prefixed with '+' in the first pass
				if key.startswith('+'):
					continue
				# Extracting the data from a different file
				if path.startswith('@FILE'):
					data_el = obj_by_path(file_id, path)
				# Extracting the data according to the defined path in the chip_data
				else:
					data_el = dict_data_by_path(chip_data, path)
				# Validating the extracted data
				if data_el is None:
					if self.verbose:
						print(f'WARNING: Data path `{path}` not found in file {file_id}')
						print( '         Skipping file...')
					return None
				if isinstance(data_el, list):
					# Converting list to an array
					data_in[key] = np.array(data_el)
				else:
					# Storing as is (single value or dictionary)
					data_in[key] = data_el
			# Second pass for post-processing keys starting with '+'
			for key, val in config.items():
				if not key.startswith('+'):
					continue
				if isinstance(val, type(lambda:0)):
					data_in[key] = val(data_in)
				elif isinstance(val, str):
					if val == '@chip_statuses':
						if self.chip_statuses is not None:
							chip_id = self.plot_data['chip_id']
							data_in[key] = self.chip_statuses[chip_id]
						else:
							raise ValueError('Failed getting chip status')
					elif val == '@chip_name':
						data_in[key] = ''.join([f'{v:X}' for v in self.plot_data['chip_id']])

				else:
					data_in[key] = val
		return data_in


	def process_file(self, file_id, chip_data, config):
		"""Process a single file according to a single test configuration"""

		data_in = {}
		data_out = {}
		data_out_sum = {}

		# Storing the ID of the file that is currently processed
		self.plot_data['file_id'] = file_id

		# Getting the ID of the chip being processed
		scan_info = parse_filename(file_id)
		chip_id = scan_info['chip']
		self.plot_data['chip_id'] = chip_id

		# Storing the base information to the DB dictionary
		self.fill_db_data()

		# Getting input data according to the configuration
		if 'output' not in config or not self.is_input_cached(config['output']):
			try:
				data_in = self.get_data_in(file_id, chip_data, config['input'])
			except ValueError:
				return False
		if data_in is None:
			return True

		# Constructing the output data according to the output configuration
		if 'output' in config:
			data_out = self.calc_output_data(data_in, config['output'])
		else:
			if isinstance(config['input'], dict) and 'x' in config['input'] and 'y' in config['input']:
				# Assuming single graph with exactly two paths defined for the x and y values
				if None not in data_in.values():
					data_out = dict(zip(data_in['x'], data_in['y']))
			else:
				# Assuming multiple histograms passed directly to the output
				data_out = data_in

		# Constructing the summary output depending on the presence of output data
		if 'output_sum' in config:
			if 'output' in config:
				data_out_sum = self.calc_output_data(data_out, config['output_sum'], data_in)
			else:
				data_out_sum = self.calc_output_data(data_in, config['output_sum'])

		# Storing the data for this specific file
		self.plot_data['data_in'] = data_in
		self.plot_data['data'] = data_out
		self.plot_data['data_sum'] = data_out_sum

		return True


	def run(self):
		"""Processes all the input data"""

		if not self.config:
			print('ERROR: No plotting configuration provided')
			sys.exit(2)

		if not self.dry_run and not self.data:
			print('ERROR: No JSON data provided for plotting')
			sys.exit(2)

		self.sort_configs()
		self.apply_styles()

		# Loop over plot configurations
		for name, config in self.config.items():
			if name == 'GLOBAL':
				continue
			if 'skip' in config and config['skip']:
				continue
			print(f'Processing: {name}')

			# Processing data from each JSON file to extract the data to be plotted
			data_aggregated = {}
			data_aggregated_sum = {}
			n_files_drawn = 0
			n_chips_updated = 0
			n_files = len(self.data)

			if self.dry_run:
				continue

			for file_id, chip_data in self.data.items():
				n_files_drawn += 1
				print(f'\r{n_files_drawn}/{n_files}', end='', flush=True)
				# Storing data of the plots to the plotting data
				self.reset_plot_data()
				self.plot_data['name'] = name
				self.plot_data['config'] = config
				# Processing a single input file if its output is not in cache
				cached_output = None
				if (name, file_id) in self.output_cache:
					cached_output = self.output_cache[(name, file_id)]
					if ('regions' in config) and ('regions' in cached_output):
						# Ignoring the cache if region definitions don't match
						if config['regions'] != cached_output['regions']:
							cached_output = None
				if cached_output is None:
					file_good = self.process_file(file_id, chip_data, config)

					# Skipping the file if processing failed
					if not file_good:
						continue

					data = self.plot_data['data']
					# Applying aggregation transformation if defined
					if 'aggregate.item' in config:
						func = config['aggregate.item']
						try:
							data = func(data)
						except Exception as exc:
							data = None
							if self.verbose > 0:
								print(f"WARNING: Aggregation item failed: {exc}")
					self.plot_data['data'] = data

					# Caching the output from this file
					self.output_cache[(name, file_id)] = {
						n: self.plot_data[n] for n in ['data', 'data_sum', 'file_id', 'chip_id']
					}
					# Caching the region definitions used for this output
					if 'regions' in config:
						self.output_cache[(name, file_id)]['regions'] = config['regions']
					n_chips_updated += 1
				else:
					# Getting output from the cache
					for n in ['data', 'data_sum', 'file_id', 'chip_id']:
						self.plot_data.update(cached_output)

				# Storing data to the DB dictionary
				if 'db' in config:
					db_data = None
					if isinstance(config['db'], dict):
						db_data = self.calc_output_data(self.plot_data['data'], config['db'],
														self.plot_data['data_in'])
					elif isinstance(config['db'], str):
						db_data = {config['db']: self.plot_data['data']}
					if db_data:
						self.fill_db_data(db_data)

				# Drawing the plot for this file if no aggregation should be done
				if 'aggregate' not in config:
					try:
						self.draw_plot(skip_if_exists=True)
					except KeyboardInterrupt:
						sys.exit(1)
					except Exception as e:
						if self.verbose:
							print(f'ERROR: Failed drawing for file: {file_id}')
					continue

				# Storing the aggregated version of the data
				data_aggregated[file_id] = self.plot_data['data']
				data_aggregated_sum[file_id] = self.plot_data['data_sum']
				data = self.plot_data['data']

				if config['aggregate'] == 'histograms':
					# Grouping single values by keys from the data structure
					for key, val in data.items():
						if key not in data_aggregated:
							data_aggregated[key] = []
						data_aggregated[key].append(val)

				elif is_iterable(data) and len(data) == 1:
					# Grouping single values by files
					data_aggregated[file_id] = data[next(iter(data))]
			# Clearing the file-counter line
			print('\r', end='', flush=True)
			
			# Stopping if no aggregated version had to be drawn
			if 'aggregate' not in config:
				continue

			# Not drawing the agreagated plot if no regions are defined for it
			if 'regions' not in config:
				continue

			# Drawing the aggregated version
			if config['aggregate'] not in self.CHOICES_AGGR:
				print(f"ERROR: Wrong aggregation option: {config['aggregate']}")
				print('         Should be one of: '+ ', '.join(self.CHOICES_AGGR))
				sys.exit(3)

			self.plot_data['file_id'] = None
			self.plot_data['data'] = data_aggregated
			self.plot_data['data_sum'] = data_aggregated_sum
			if config['aggregate'] == 'histogram':
				# Creating custom summary data
				if 'output_sum' in config:
					data_sum = self.calc_output_data(list(data_aggregated.values()),
					                                 config['output_sum'])
					self.plot_data['data_sum'].update(data_sum)

			self.draw_plot(skip_if_exists=(n_chips_updated == 0))

		# Writing out region definitions actually used by the plotter
		self.write_regions()




if __name__ == "__main__":

	import argparse
	import re


	parser = argparse.ArgumentParser(description='Plotter of JSON test output')
	parser.add_argument('data', metavar='DIR|FILE.json', type=str, nargs='+',
						help='Input JSON file(s)')
	parser.add_argument('-o', '--out_dir', metavar='DIR', type=str,
						help='Path to the output directory', default='./plots')
	parser.add_argument('-c', '--config', metavar='ID', type=str,
						help='Version number or path to the analyzer configuration file')
	parser.add_argument('-d', '--dry_run', action='store_true',
	                    help='Just produce book-keeping files without any plotting')
	parser.add_argument('-k', '--keep', action='store_true',
	                    help='Keep all the individual plots in addition to the merged PDFs')
	parser.add_argument('-n', '--name', metavar='NAME', type=str,
						help='Only produce plots matching the NAME pattern', default=None)
	parser.add_argument('-r', '--regions', metavar='FILE', type=str,
	                    help='Path to the python file with region definitions to be used')
	parser.add_argument('--stat_regions', type=float, nargs='+',
	                    help='Set chip-quality regions based on multiples of RMS', default=[])
	parser.add_argument('-v', '--verbose', metavar='1', type=int,
						help='Verbosity level', default=0)
	parser.add_argument('-w', '--waferonly', action='store_true',
	                    help='Only produce wafer-wise plots')
	args = parser.parse_args()

	# Building the list of JSON files to be loaded
	files = []
	for path in args.data:
		if os.path.isfile(path):
			# Adding a file directly
			file = os.path.basename(path)
			if not parse_filename(file, strict=True):
				continue
			files.append(path)
		elif os.path.isdir(path):
			# Selecting all relevant files from a folder
			files_dir = {}
			for file in os.listdir(path):
				if not os.path.splitext(file)[1] == '.json':
					continue
				scan_info = parse_filename(file, strict=True)
				if not scan_info:
					continue
				# Grouping files by chip ID
				chip_id = scan_info['chip']
				if chip_id not in files_dir:
					files_dir[chip_id] = []
				files_dir[chip_id].append({'path': os.path.join(path, file),
				                           'scan_time': scan_info['scan_time']})
			# Adding only the latest file for each chip from this folder
			for chip_id, files_chip in files_dir.items():
				files_chip.sort(key=lambda x: x['scan_time'])
				files.append(files_chip[-1]['path'])


	# Creating the plotter instance
	plotter = JsonPlotter(out_dir=args.out_dir, verbose=args.verbose, dry_run=args.dry_run)
	plotter.load_config(args.config, args.regions)
	print(f'Plotting data from {len(files)} files:')
	for f_in in files:
		print(' '+f_in)
		plotter.load_data(f_in)

	# Filtering the configurations to be processed
	for p_name, p_config in plotter.config.items():
		# Skipping doesn't match the name pattern
		if args.name and not re.search(args.name, p_name):
			p_config['skip'] = True
		if args.waferonly and 'aggregate' not in p_config:
			p_config['skip'] = True

	# Setting region-definition mode
	plotter.set_stat_regions(args.stat_regions)

	# Executing the actual plotting
	plotter.run()
	plotter.write_db_data()
	plotter.merge_summary()

	# Deleting the individual plots after merging
	if not args.keep:
		for name in os.listdir(plotter.out_dir):
			if name.startswith('_'):
				continue
			if name in ['summary', 'db']:
				continue
			path = os.path.join(plotter.out_dir, name)
			print(f'Removing: {path}')
			if os.path.isfile(path):
				os.remove(path)
			elif os.path.isdir(path):
				shutil.rmtree(path)

	print(f'Saved plots: {plotter.n_plotted}')
	print(f'         in: {plotter.out_dir}')
