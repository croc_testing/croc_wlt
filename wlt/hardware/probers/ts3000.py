#pylint: disable=C0103
"""
Module which contains the class used for interacting with the TS3000 probe
station used at the KSU probing site.

It relies on the Gpib package provided by Linux GPIB.
"""

import time

try:
	import Gpib
except ModuleNotFoundError:
	raise ModuleNotFoundError("Could not import package `Gpib`. Make sure that "
		"this package is installed in order to interact with the TS3000 probe "
		"station.") from None

from .prober import Prober, ProberReply, ReplyField
from . import errors

#encoding used for probe station communication
ENCODING = "ASCII"


class ReplyTS3000(ProberReply):
	"""Class used for handling TS3000 replies."""

	fields = {
		"get_chuck_site_status": [
			ReplyField("Home is set", int, False),
			ReplyField("Contact is set", int, False),
			ReplyField("Overtravel is active", int, False),
			ReplyField("Vacuum is on", int, False)
		],
		"get_chuck_site_heights": [
			ReplyField("Contact Height", float, False),
			ReplyField("Separation Height", float, False),
			ReplyField("Overtravel Gap", float, False),
			ReplyField("Hover Height", float, False),
		],
		"move_chuck_xy": [
			ReplyField("New X [um]", float, False),
			ReplyField("New Y [um]", float, False)
		],
		"get_chuck_xy": [
			ReplyField("X [um]", float, False),
			ReplyField("Y [um]", float, False)
		],
		"get_chuck_z": [
			ReplyField("Z [um]", float, False)
		],
		"map:step_die": [
			ReplyField("Column Index", int, False),
			ReplyField("Row Index", int, False),
			ReplyField("Subsite Index", int, False)
		],
		"map:step_next_die": [
			ReplyField("Column Index", int, False),
			ReplyField("Row Index", int, False),
			ReplyField("Subsite Index", int, False)
		],
		"map:step_previous_die": [
			ReplyField("Column Index", int, False),
			ReplyField("Row Index", int, False)
		],
		"map:step_die_seq": [
			ReplyField("Column Index", int, False),
			ReplyField("Row Index", int, False),
			ReplyField("Subsite Index", int, False)
		],
		"map:step_first_die": [
			ReplyField("Column Index", int, False),
			ReplyField("Row Index", int, False),
			ReplyField("Subsite Index", int, False)
		],
		"map:die:get_current_index": [
			ReplyField("List Index", int, False),
			ReplyField("Column Index", int, False),
			ReplyField("Row Index", int, False)
		],
		"move_chuck_contact": [
			ReplyField("New Z [um]", float, False)
		],
		"move_chuck_separation": [
			ReplyField("New Z [um]", float, False)
		],
		"move_chuck_load": [
			ReplyField("Setup", str, False)
		],
		"set_chuck_site_heights": [
			ReplyField("Chuck site", str, False),
			ReplyField("Contact height [um]", float, False),
			ReplyField("Separation height [um]", float, False),
			ReplyField("Overtravel gap [um]", float, False),
			ReplyField("Hover height [um]", float, False),
		],
		"set_chuck_site_overtravel_gap": [
			ReplyField("Setup", str, False)
		],
		"enable_chuck_site_overtravel": [
			ReplyField("Setup", str, False)
		],
		"set_soft_contact": [
			ReplyField("Setup", str, False)
		]
	}

	def __init__(self, command, reply, data_sep=",", error_sep=","):
		super().__init__(command, reply, data_sep=data_sep, error_sep=error_sep)
		self.regex = rf"^(?P<error>\d+){error_sep}(?P<id>\d+){data_sep}(?P<data>.*)$"

	def get_error(self):
		"""Returns the error code."""

		error_raw = super().get_error()
		if error_raw is None:
			return None
		return error_raw & 0x3FF

	def get_status(self):
		"""Returns the status bits."""

		error_raw = super().get_error()
		if error_raw is None:
			return (None, None)
		is_last_die = bool(error_raw & 0x400)
		is_last_subsite = bool(error_raw & 0x800)

		return (is_last_die, is_last_subsite)

	def verify(self):
		"""Check the error code and raise the associated `ProberError` exception."""

		#getting the error code and the error data
		code = self.get_error()
		if (code is None) or (code == 0):
			return
		data = self.get_data()
		exception = None

		#movement commands that can cause an InvalidDieError exception
		mov_commands = ["map:step_die", "map:step_die_seq"]

		#exception selection
		if (self.command in mov_commands) and (code in [6, 8, 12]):
			exception = errors.InvalidDieError
		elif code in [60, 62, 64, 65, 100, 101]:
			exception = errors.CriticalProberError
		elif code in [21, 22, 24, 25, 26]:
			exception = errors.AutomationError
		#elif code in [703]:
		#	exception = errors.EndOfWaferError
		else:
			#the default is the generic ProberError
			exception = errors.ProberError

		#raising the exception
		raise exception(data, code)


class TS3000(Prober):
	"""Class used for controlling the TS3000 wafer prober with the Python GPIB 
	library."""

	def __init__(self, port):
		"""
		Initializes the GPIB communication.

		The primary GPIB address is set to 0, while the secondary GPIB address 
		can be passed as a parameter. A default timeout of 30 s is set for the 
		communication.

		The Sentio commands are used for the communication by default.

		Parameters
		----------
		port : int
			Secondary GPIB address.
		"""

		#The timeout values for the GPIB library are not in seconds but are
		#essentially an enumeration. The values of interest are:
		#13 -> 10 s
		#14 -> 30 s
		#15 -> 100 s
		#16 -> 300 s
		#17 -> 1000 s
		self.interface = Gpib.Gpib(0, port, timeout=14)
		self.interface.write("*RCS Sentio") #run with Sentio commands

	def query(self, command):
		"""
		Performs a query operation.

		If the query involves an SCPI command, raw mode must be enabled.

		Parameters
		----------
		command : str
			The command to send to the probe station for the query operation.

		Returns
		-------
		str
			The reply from the probe station.

		Raises
		------
		RuntimeError
			If the query operation somehow fails.
		ProberError
			If the probe station returns a non-zero error code.
		"""

		#performing the query operation and getting the raw answer
		try:
			self.interface.write(command)
			reply = self.interface.read(200)
		except Exception as e:
			raise RuntimeError("Could not complete the query operation!") from e

		#decoding the raw answer
		try:
			decoded_reply = reply.decode(ENCODING)
		except UnicodeDecodeError:
			raise RuntimeError("Could not decode the probe station reply!")

		#processing the reply
		command_name = command.split(" ")[0]
		command_name.strip()
		reply = ReplyTS3000(command_name, decoded_reply)

		#if there has been an error, raise an exception
		reply.verify()

		return reply

	def set_position(self, x, y):
		"""
		Moves the chuck to the absolute position in um.

		Parameters
		----------
		x, y : float
			Absolute position.
		"""

		self.query(f"move_chuck_xy Home, {x:1.1f}, {y:1.1f}")

	def move_position(self, dx, dy):
		"""
		Moves the chuck relative to the actual position in um.

		Parameters
		----------
		dx, dy : float
			Relative position.
		"""

		self.query(f"move_chuck_xy Relative, {dx:1.1f}, {dy:1.1f}")

	def get_position(self):
		"""
		Reads chuck position (x, y, z) relative to the home position.

		Returns
		-------
		tuple of str
			The three coordinates.
		"""

		#querying xy position
		reply_xy = self.query("get_chuck_xy Wafer, Home")
		reply_z = self.query("get_chuck_z Contact")

		#getting the reply fields
		fields_xy = reply_xy.get_fields()
		fields_z = reply_z.get_fields()

		#getting the data
		x = fields_xy["X [um]"]
		y = fields_xy["Y [um]"]
		z = fields_z["Z [um]"]

		return (x, y, z)

	def get_position_from_zero(self):
		"""
		Reads chuck position (x, y, z) relative to the axes' origin.

		Returns
		-------
		tuple of float
			The three coordinates.
		"""

		#querying xy position
		reply_xy = self.query("get_chuck_xy Wafer, Zero")
		reply_z = self.query("get_chuck_z Zero")

		#getting the reply fields
		fields_xy = reply_xy.get_fields()
		fields_z = reply_z.get_fields()

		#getting the data
		x = fields_xy["X [um]"]
		y = fields_xy["Y [um]"]
		z = fields_z["Z [um]"]

		return (x, y, z)

	def is_in_contact(self):
		"""
		Returns the status of the chuck and whether it is at contact/overtravel 
		height or not.

		Returns
		-------
		bool
			The contact status, with True meaning that the probe station chuck 
			is at overtravel or contact height.
		"""

		#get the current chuck height
		reply = self.query("get_chuck_z Contact")
		contact_z = reply.get_fields()["Z [um]"]
		is_in_contact = (contact_z > -5.0)
		return is_in_contact

	def goto_die(self, col, row):
		"""
		Move chuck to wafer map chip index.

		Parameters
		----------
		col : int
			The column index of the die to move to.
		row : int
			The row index of the die to move to.
		"""

		self.query(f"map:step_die {col:d}, {row:d}")

	def goto_next_die(self):
		"""Move chuck to next die from wafer map."""

		self.query("map:step_next_die")

	def goto_die_number(self, number):
		"""
		Move chuck to `number`-th die from wafer map.

		Parameters
		----------
		number : int
			Number of the die to which the probe station chuck must move to.
		"""

		self.query(f"map:step_die_seq {number}")

	def goto_first_die(self):
		"""Move chuck to first die from wafer map."""

		self.query("map:step_first_die")

	def goto_prev_die(self):
		"""Move chuck to previous die from wafer map."""

		self.query("map:step_previous_die")

	def get_die(self):
		"""
		Get wafer map chip index.

		Returns
		-------
		tuple of int
			Tuple with the format (column_index, row_index).
		"""

		reply = self.query("map:die:get_current_index")
		fields = reply.get_fields()
		col = fields["Column Index"]
		row = fields["Row Index"]
		return (col, row)

	def contact(self):
		"""Move chuck to contact z position."""

		self.query("move_chuck_contact")

	def separate(self):
		"""Move chuck to separation z position."""

		self.query("move_chuck_separation")

	def load(self):
		"""Move chuck to load z position."""

		self.query("move_chuck_load")

	def get_chuck_heights(self):
		"""
		Reads the contact height, the overtravel gap, the alignment gap, and 
		the separation gap.

		Returns
		-------
		tuple of float
			Tuple with the contact height, the overtravel gap, and the 
			separation gap.
		"""

		reply = self.query("get_chuck_site_heights Wafer")

		fields = reply.get_fields()
		contact_z = fields["Contact Height"]
		overtravel_gap = fields["Overtravel Gap"]
		alignment_gap = fields["Hover Height"]
		separation_gap = fields["Separation Height"]

		return contact_z, overtravel_gap, alignment_gap, separation_gap

	def set_contact_height(self, height):
		"""
		Sets the contact height to the given value.

		Parameters
		----------
		height: int or float
			Contact height (in micrometres).
		"""

		self.query(f"set_chuck_site_heights Wafer,Contact,{height}")

	def set_alignment_gap(self, gap):
		"""
		Sets the alignment gap to the given value.

		Parameters
		----------
		gap: int or float
			Alignment gap (in micrometres).
		"""

		self.query(f"set_chuck_site_heights Wafer,Hover,{gap}")

	def set_separation_gap(self, gap):
		"""
		Sets the separation gap to the given value.

		Parameters
		----------
		gap: int or float
			Separation gap (in micrometres).
		"""

		reply = self.query("get_chuck_site_heights Wafer")

		fields = reply.get_fields()
		contact_z = fields["Contact Height"]
		abs_sep_position = contact_z - gap

		self.query(f"set_chuck_site_heights Wafer,Separation,{abs_sep_position}")

	def set_overtravel(self, distance):
		"""
		Sets the overtravel to the given value.

		Parameters
		----------
		distance: int or float
			Overtravel distance (in micrometres).

		Raises
		------
		ValueError
			If the overtravel is not in [0, 90 um].
		"""

		if (distance < 0) or (distance > 90):
			raise ValueError("Incorrect overtravel setting!")

		self.query(f"set_chuck_site_overtravel_gap Wafer,{distance}")

	def enable_overtravel(self):
		"""
		Enables overtravel.

		The chuck will move to overtravel height instead of contact height if 
		enabled.

		The rest of the chuck configuration remains the same.
		"""

		self.query("enable_chuck_site_overtravel Wafer,1")

	def disable_overtravel(self):
		"""
		Disables overtravel.

		The chuck will move exactly to the set contact height if disabled.

		The rest of the chuck configuration remains the same.
		"""

		self.query("enable_chuck_site_overtravel Wafer,0")

	def get_id(self):
		"""
		Ask ID of the wafer prober.

		Returns
		-------
		str
			The identification string of the probe station.
		"""

		reply = self.query("*IDN?")
		return reply.get_raw()

	def enable_soft_contact(self):
		"""
		Enables soft contact mode: the chuck slows down after reaching the 
		configured height.
		"""

		self.query("set_soft_contact True")

	def disable_soft_contact(self):
		"""
		Disables soft contact mode: the chuck does not slow down at a certain 
		height.
		"""

		self.query("set_soft_contact False")

	def clean_needles(self, sequence_name=""):
		"""
		Cleans the probe card tips by going to 5 random spots and doing 3 
		touchdowns on each spot.
		"""

		self.query("move_chuck_site AuxRight")
		self.query("set_chuck_speed Jog")
		self.query("set_soft_contact False")
		for _ in range(5):
			self.query("aux:cleaning:start AuxRight, 3")
			time.sleep(3)
		self.query("set_soft_contact True")
		self.query("move_chuck_site Wafer")
