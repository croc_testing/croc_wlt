"""Module containing the base classes for probers and replies."""

import re
from dataclasses import dataclass

from .errors import ProberError


class Prober:
	"""Base class for handling probe stations."""


class ProberReply:
	"""Class to handle probe station replies."""

	fields = {}

	def __init__(self, command, reply, data_sep=" ", error_sep=" "):
		self.command = command
		self.reply = reply
		self.data_sep = data_sep
		self.error_sep = error_sep
		self.regex = ""

	def __str__(self):
		return self.get_raw()

	def get_raw(self):
		"""Returns the raw probe station reply."""
		return self.reply

	def get_error(self):
		"""Returns the error code."""

		#parse the error code and the reply
		match = re.search(self.regex, self.reply)
		if match is None:
			return None
		return int(match.group("error"))

	def get_data(self):
		"""Returns the probe station reply data."""

		#parse the error code and the reply
		match = re.search(self.regex, self.reply)
		if match is None:
			#raise RuntimeError(f"Could not parse the response: '{self.reply}'")
			return None
		data = match.group("data")
		return data

	def get_fields(self):
		"""Returns the parsed reply fields."""

		#parse the error code and the reply
		match = re.search(self.regex, self.reply)
		if match is None:
			#raise RuntimeError(f"Could not parse the response: '{self.reply}'")
			return None
		data = match.group("data")

		#try to get the different fields for the given command
		try:
			fields = self.__class__.fields[self.command]
		except KeyError:
			#raise ValueError(f"No reply fields data for command: {self.command}") from None
			return None

		#splitting the reply
		values = data.split(self.data_sep)

		#check if there are optional output fields in the reply
		if len(values) != len(fields):
			#optional fields are missing: get only the non-optional ones
			fields = [x for x in fields if x.is_optional is False]

		output_dict = {}
		for i, field in enumerate(fields):
			try:
				values[i] = field.type_(values[i])
			except ValueError:
				values[i] = None
			except IndexError:
				#there shouldn't be, at this point, a mismatch in the lengths of
				#the fields and the values, so there is no point in parsing
				#raise RuntimeError("Mismatch between the reply and the fields!")
				return None
			output_dict.update({field.name: values[i]})

		return output_dict

	def verify(self):
		"""Check the error code and raise the associated `ProberError` exception."""

		code = self.get_error()
		if (code is not None) and (code != 0):
			data = self.get_data()
			raise ProberError(message=data, code=code)


@dataclass
class ReplyField:
	"""Simple data class used for handling reply fields."""
	name: str
	type_: type
	is_optional: bool
