"""Module containing the definition of the probe station exceptions."""


class ProberError(Exception):
	"""Base exception for probe station errors."""

	def __init__(self, message="", code=None):
		"""
		Stores the probe station error code and its message.

		Parameters
		----------
		message : str, optional
			Probe station error message.
		code : int or None, optional
			Error code returned by the probe station.
		"""

		self.code = code
		self.message = message

	def __str__(self):
		"""
		Prints both the error message and the error code.

		Returns
		-------
		str
			A string containing both the error message and the error code.
		"""

		return (f"{self.message}" if self.code is None
			else f"{self.message} ({self.code})")


class CriticalProberError(ProberError):
	"""Exception raised in case of critical problems that preclude the 
	possibility of testing a wafer."""


class AutomationError(ProberError):
	"""Exception used when the pattern recognition fails."""


class InvalidDieError(ProberError):
	"""Exception used when the movement to a non-existing die is requested."""


class EndOfWaferError(ProberError):
	"""Exception raised when the chuck is at the end of the wafer."""
