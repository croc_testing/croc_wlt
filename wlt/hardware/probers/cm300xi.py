#pylint: disable=C0103
"""
Module which contains the class used for interacting with the CM300xi probe
station used at the Torino probing site.

It relies on the Gpib package provided by Linux GPIB.
"""

import time

try:
	import Gpib
except ModuleNotFoundError:
	raise ModuleNotFoundError("Could not import package `Gpib`. Make sure that "
		"this package is installed in order to interact with the CM300xi probe "
		"station.") from None

from .prober import Prober, ProberReply, ReplyField
from . import errors

#encoding used for probe station communication
ENCODING = "ASCII"


class ReplyCM300xi(ProberReply):
	"""Class used for handling CM300xi replies."""

	fields = {
		"StepNextDie": [
			ReplyField("Column", int, False),
			ReplyField("Row", int, False),
			ReplyField("SubDie", int, False),
			ReplyField("Total SubDies", int, False)
		],
		"ReadChuckPosition": [
			ReplyField("X Value", float, False),
			ReplyField("Y Value", float, False),
			ReplyField("Z Value", float, False),
		],
		"ReadMapPosition": [
			ReplyField("Column", int, False),
			ReplyField("Row", int, False),
			ReplyField("xPosition", float, False),
			ReplyField("yPosition", float, False),
			ReplyField("CurrentSubDie", int, False),
			ReplyField("Total SubDies", int, False),
			ReplyField("CurrentDie", int, False),
			ReplyField("Total Dies", int, False),
			ReplyField("CurrentCluster", int, False),
			ReplyField("Total Clusters", int, False)
		],
		"ReadChuckStatus": [
			ReplyField("IsInitialized", int, False),
			ReplyField("Mode", int, False),
			ReplyField("OnEndLimit", int, False),
			ReplyField("IsMoving", int, False),
			ReplyField("CompMode", str, False),
			ReplyField("Vacuum", int, False),
			ReplyField("Z Height", str, False),
			ReplyField("Load Position", int, False),
			ReplyField("Lift", int, False),
			ReplyField("ChuckCamera", str, False)
		],
		"GetDieDataAsColRow": [
			ReplyField("DieNumber", int, False),
			ReplyField("Column", int, False),
			ReplyField("Row", int, False),
			ReplyField("Bincode", int, False),
			ReplyField("Result", str, True)
		],
		"ReadChuckHeights": [
			ReplyField("Contact Height", float, False),
			ReplyField("Overtravel Gap", float, False),
			ReplyField("Alignment Gap", float, False),
			ReplyField("Separation Gap", float, False),
			ReplyField("Search Gap", float, False)
		]
	}

	def __init__(self, command, reply, data_sep=" ", error_sep=":"):
		super().__init__(command, reply, data_sep=data_sep, error_sep=error_sep)
		self.regex = rf"^(?P<error>\d+){error_sep} (?P<data>.*)$"

	def verify(self):
		"""Check the error code and raise the associated `ProberError` exception."""

		code = self.get_error()
		if (code is None) or (code == 0):
			return
		data = self.get_data()
		exception = None

		#checking the error code
		if code in [895]:
			exception = errors.CriticalProberError
		elif code in [896]:
			exception = errors.AutomationError
		elif code in [703]:
			exception = errors.EndOfWaferError
		elif code in [704]:
			exception = errors.InvalidDieError
		else:
			#the default is the generic ProberError
			exception = errors.ProberError

		#raising the exception
		raise exception(data, code)


class CM300xi(Prober):
	"""
	Class used for controlling the CM300xi wafer prober with the Python GPIB 
	library.

	Dangerous commands like set_position while in contact are ignored
	by the command server.
	"""

	def __init__(self, port):
		"""
		Initializes the GPIB communication.

		The primary GPIB address is set to 0, while the secondary GPIB address 
		can be passed as a parameter. A default timeout of 30 s is set for the 
		communication.

		The SCI/Velox commands are used for the communication by default.

		Parameters
		----------
		port : int
			Secondary GPIB address.
		"""

		#The timeout values for the GPIB library are not in seconds but are
		#essentially an enumeration. The values of interest are:
		#13 -> 10 s
		#14 -> 30 s
		#15 -> 100 s
		#16 -> 300 s
		#17 -> 1000 s
		self.interface = Gpib.Gpib(0, port, timeout=14)

	def query(self, command):
		"""
		Performs a query operation.

		If the query involves an SCPI command, raw mode must be enabled.

		Parameters
		----------
		command : str
			The command to send to the probe station for the query operation.

		Returns
		-------
		str
			The reply from the probe station.

		Raises
		------
		RuntimeError
			If the query operation somehow fails.
		ProberError
			If the probe station returns a non-zero error code.
		"""

		#performing the query operation and getting the raw answer
		try:
			self.interface.write(command)
			reply_str = self.interface.read(200)
		except Exception as e:
			raise RuntimeError("Could not complete the query operation!") from e

		#decoding the raw answer
		try:
			decoded_reply = reply_str.decode(ENCODING)
		except UnicodeDecodeError:
			raise RuntimeError("Could not decode the probe station reply!")

		#processing the reply
		command_name = command.split(" ")[0]
		command_name.strip()
		reply = ReplyCM300xi(command_name, decoded_reply)

		#if there has been an error, raise an exception
		reply.verify()

		return reply

	def set_position(self, x, y, speed = None):
		"""
		Moves the chuck to the absolute position in um.

		Parameters
		----------
		x, y : float
			Absolute position.
		speed : int or float, optional
			Movement speed as a percentage of full speed.
		"""

		if speed:
			self.query(f"MoveChuckSubsite {x:1.1f} {y:1.1f} R Y {speed:d}")
		else:
			self.query(f"MoveChuckSubsite {x:1.1f} {y:1.1f} R Y")

	def move_position(self, dx, dy, speed = None):
		"""
		Moves the chuck relative to the actual position in um.

		Parameters
		----------
		dx, dy : float
			Relative position.
		speed : int or float, optional
			Movement speed as a percentage of full speed.
		"""

		if speed:
			self.query(f"MoveChuckPosition {dx:1.1f} {dy:1.1f} R Y {speed:d}")
		else:
			self.query(f"MoveChuckPosition {dx:1.1f} {dy:1.1f} R Y")

	def get_position(self):
		"""
		Reads chuck position (x, y, z) relative to the home position.

		Returns
		-------
		tuple of float
			The three coordinates.
		"""

		reply = self.query("ReadChuckPosition Y H")
		fields = reply.get_fields()
		x = fields["X Value"]
		y = fields["Y Value"]
		z = fields["Z Value"]
		return (x, y, z)

	def get_position_from_zero(self):
		"""
		Reads chuck position (x, y, z) relative to the axes' origin.

		Returns
		-------
		tuple of float
			The three coordinates.
		"""

		reply = self.query("ReadChuckPosition Y Z")
		fields = reply.get_fields()
		x = fields["X Value"]
		y = fields["Y Value"]
		z = fields["Z Value"]
		return (x, y, z)

	def is_in_contact(self):
		"""
		Returns the status of the chuck and whether it is at contact/overtravel 
		height or not.

		Returns
		-------
		bool
			The contact status, with True meaning that the probe station chuck 
			is at overtravel or contact height.
		"""

		reply = self.query("ReadChuckStatus")
		fields = reply.get_fields()
		#if the chuck height is set to "contact" or "overtravel", return
		#true, otherwise return false
		is_in_contact = (fields["Z Height"] in ["C", "O"])

		return is_in_contact

	def goto_die(self, col, row):
		"""
		Move chuck to wafer map chip index.

		Parameters
		----------
		col : int
			The column index of the die to move to.
		row : int
			The row index of the die to move to.
		"""

		self.query(f"StepNextDie {col:d} {row:d}")

	def goto_next_die(self):
		"""Move chuck to next die from wafer map."""

		self.query("StepNextDie")

	def get_die_number(self, index_x, index_y):
		"""
		Gets the number of the die located at (index_x, index_y).

		Parameters
		----------
		index_x : int
			The column index of the die to move to.
		index_y : int
			The row index of the die to move to.

		Returns
		-------
		int
			The die number.
		"""

		reply = self.query(f"GetDieDataAsColRow {index_x} {index_y}")
		fields = reply.get_fields()
		return fields["DieNumber"]

	def goto_die_number(self, number):
		"""
		Move chuck to `number`-th die from wafer map.

		Parameters
		----------
		number : int
			Number of the die to which the probe station chuck must move to.
		"""

		self.query(f"StepToDie {number}")

	def goto_first_die(self):
		"""Move chuck to first die from wafer map."""

		self.query("StepFirstDie")

	def get_die(self):
		"""
		Get wafer map chip index.

		Returns
		-------
		tuple of int
			Tuple with the format (column_index, row_index).
		"""

		reply = self.query("ReadMapPosition")
		fields = reply.get_fields()
		col = fields["Column"]
		row = fields["Row"]
		return (col, row)

	def contact(self):
		"""Move chuck to contact z position."""

		self.query("MoveChuckContact")

	def separate(self):
		"""Move chuck to separation z position."""

		self.query("MoveChuckSeparation")

	def load(self):
		"""Move chuck to load z position."""

		self.query("MoveChuckLoad")

	def get_chuck_heights(self):
		"""
		Reads the contact height, the overtravel gap, the alignment gap, and 
		the separation gap.

		Returns
		-------
		tuple of float
			Tuple with the contact height, the overtravel gap, the alignment 
			gap, and the separation gap.
		"""

		reply = self.query("ReadChuckHeights")

		fields = reply.get_fields()
		contact_z = fields["Contact Height"]
		overtravel_gap = fields["Overtravel Gap"]
		alignment_gap = fields["Alignment Gap"]
		separation_gap = fields["Separation Gap"]

		return contact_z, overtravel_gap, alignment_gap, separation_gap

	def set_contact_height(self, height):
		"""
		Sets the contact height to the given value.

		Parameters
		----------
		height: int or float
			Contact height (in micrometres).
		"""

		self.query(f"SetChuckHeight C V Y {height}")

	def set_alignment_gap(self, gap):
		"""
		Sets the alignment gap to the given value.

		Parameters
		----------
		gap: int or float
			Alignment gap (in micrometres).
		"""

		self.query(f"SetChuckHeight A V Y {gap}")

	def set_separation_gap(self, gap):
		"""
		Sets the separation gap to the given value.

		Parameters
		----------
		gap: int or float
			Separation gap (in micrometres).
		"""

		self.query(f"SetChuckHeight S V Y {gap}")

	def set_overtravel(self, distance):
		"""
		Sets the overtravel to the given value.

		Parameters
		----------
		distance: int or float
			Overtravel distance (in micrometres).

		Raises
		------
		ValueError
			If the overtravel is not in [0, 90 um].
		"""

		if (distance < 0) or (distance > 90):
			raise ValueError("Incorrect overtravel setting!")

		self.query(f"SetChuckHeight O V Y {distance}")

	def enable_overtravel(self):
		"""
		Enables overtravel.

		The chuck will move to overtravel height instead of contact height if 
		enabled.

		The rest of the chuck configuration remains the same.
		"""

		self.query("SetChuckMode 1 2 2 2 2 2")

	def disable_overtravel(self):
		"""
		Disables overtravel.

		The chuck will move exactly to the set contact height if disabled.

		The rest of the chuck configuration remains the same.
		"""

		self.query("SetChuckMode 0 2 2 2 2 2")

	def get_id(self):
		"""
		Ask ID of the wafer prober.

		Returns
		-------
		str
			The identification string of the probe station.
		"""

		reply = self.query("*IDN?")
		return reply.get_raw()

	def enable_soft_contact(self):
		"""
		Soft contact mode is not available for the CM300xi probe station.

		This method always raises an exception when called.

		Raises
		------
		NotImplementedError
			Unavailable command.
		"""

		raise NotImplementedError("Soft contact not available on CM300xi prober!")

	def disable_soft_contact(self):
		"""
		Soft contact mode is not available for the CM300xi probe station.

		This method always raises an exception when called.

		Raises
		------
		NotImplementedError
			Unavailable command.
		"""

		raise NotImplementedError("Soft contact not available on CM300xi prober!")

	def clean_needles(self, sequence_name=""):
		"""
		Performs the cleaning sequence with the given name.

		Parameters
		----------
		sequence_name : str, optional
			Name of the cleaning sequence to perform. If no sequence name is 
			provided, the default one is used.
		"""

		#execute the cleaning sequence with media reuse allowed
		self.query(f"ExecuteCleaningSequence \"{sequence_name}\" 1")

		#adding a bit of sleep to see if this fixes the chuck not being ready
		time.sleep(5)
