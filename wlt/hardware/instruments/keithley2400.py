"""
Module for handling Keithley 2400 series source-meter units.

The module has been developed and tested with a Keithley 2400/2401.
"""

import time

from .instrument import Instrument

#vendor and model information for this module
VENDOR = "Keithley"
MODELS = ["2400", "2401", "2410"]


class Keithley2400(Instrument):
	"""
	Class used to interface with Keithley source-meters of the 2400 series.

	It has been developed and tested using Keithley 2400/2401 source-meters.
	"""

	#list of valid channel names. Each entry is itself a list in order to allow
	#for variations
	channels = [["FRON", "FRONT"], ["REAR"]]

	def query_idn(self):
		"""
		Queries the instrument for the identification string.

		Returns
		-------
		str
			The identification string of the instrument.
		"""

		return self.query("*IDN?")

	def query_esr(self):
		"""
		Queries and clears the Event Status Register.

		Returns
		-------
		str
			The value of the Event Status Register.
		"""

		return self.query("*ESR?")

	def query_n_errors(self):
		"""
		Queries the instrument for the number of errors in the Error Queue.

		Returns
		-------
		int
			The number of errors in the Error Queue of the instrument.
		"""

		code = int(self.query("SYST:ERR:COUN?"))
		return code

	def query_next_error(self):
		"""
		Gets an error out of the Error Queue of the instrument.

		Returns
		-------
		str
			The next error in the Error Queue.
		"""

		return self.query("SYST:ERR:NEXT?")

	def query_error_queue(self):
		"""
		Queries the instrument for the Error Queue. The register is cleared
		afterwards.

		Returns
		-------
		str
			The error queue of the instrument.
		"""

		return self.query("SYST:ERR:ALL?")

	def set_channel(self, channel):
		"""Sets the active channel of the source-meter."""

		self._check_channel(channel)
		self.write(f"ROUT:TERM {channel}")

	def query_channel(self):
		"""
		Queries the instrument for the active channel.

		Returns
		-------
		str
			The active channel of the instrument.
		"""

		return self.query("ROUT:TERM?")

	def query_output(self):
		"""
		Queries the instrument for the status of the output for the active
		channel.

		Returns
		-------
		str
			The status of the output of the instrument.
		"""

		return self.query("OUTP?")

	def clear_status(self):
		"""
		Clears various instrument registers.

		The Standard Event Register, Operation Event Register, Measurement Event
		Register and Questionable Event Register are cleared.
		"""

		self.write("*CLS")

	def enable_output(self):
		"""Turns on the output of the instrument."""

		self.write("OUTP ON")

	def disable_output(self):
		"""Turns off the output of the instrument."""

		self.write("OUTP OFF")

	def read_output(self):
		"""
		Triggers and acquires readings from the instrument.

		The command performs an INITiate and then a FETCh. The INITiate command
		triggers a measurement cycle and stores the data in the Sample Buffer.
		This buffer is then read by sending the FETCh command.

		Returns
		-------
		str
			The results of the measurement.
		"""

		return self.query("READ?")

	def enable_beeper(self):
		"""Enables the instrument beeper."""

		self.write("SYST:BEEP:STAT 1")

	def disable_beeper(self):
		"""Disables the instrument beeper."""

		self.write("SYST:BEEP:STAT 0")

	def reset(self):
		"""
		Resets the instrument to the default settings, except for the remote 
		interface settings.

		After the reset, a small delay is set (100 ms) in order to wait for the 
		operation to complete.
		"""

		self.write("*RST")
		time.sleep(0.1)
