#pylint: disable=E1133
"""
Module used for interfacing with serial instruments employed for WLT.

The instruments are controlled using SCPI commands via serial communications.

The module has been developed and tested with a TTi QL355TP and Keithley
2400/2401.
"""

import serial

#SCPI commands are sent as ASCII strings
ENCODING = "ascii"


class Instrument:
	"""Base class from which the other power supply classes inherit."""

	#list of valid channels
	channels = None

	def __init__(self, device_file, baud_rate, device_name = "",
		device_model = "", write_termination = "\n", read_termination = "\n",
		xonxoff = 0, timeout = 1):

		self.device_name = device_name
		self.device_model = device_model
		self.device_file = device_file
		self.baud_rate = baud_rate
		self.write_termination = write_termination
		self.read_termination = read_termination
		self.xonxoff = xonxoff
		self.timeout = timeout
		self.connection = None

	def open(self):
		"""
		Opens the device file for the instrument with the configured baud
		rate, timeout and flow control.

		Raises
		------
		IOError
			If the port has already been opened or if a serial exception is 
			catched while trying to initialize the communication.
		ValueError
			When a wrong communication parameter has been passed and the 
			initialization of the device cannot proceed.
		"""

		#raise exception if the port is already open
		if self.is_open():
			raise IOError("Port already open!")

		try:
			self.connection = serial.Serial(
				self.device_file,
				self.baud_rate,
				timeout=self.timeout,
				xonxoff=self.xonxoff,
				write_timeout=5,
				exclusive=True
			)

		except serial.SerialException as e:
			self.connection = None
			raise IOError(f"Error while trying to open {self.device_file}") from e

		except ValueError as e:
			self.connection = None
			raise ValueError("Wrong parameter(s) for initializing the "
				"communication") from e

	def write(self, message):
		"""
		Low-level method to send a generic command to the instrument.

		Parameters
		----------
		message : str
			Command to send to the instrument, without the termination 
			character.

		Raises
		------
		IOError
			When the port has not been opened before calling this method.
		"""

		if not self.is_open():
			raise IOError(f"Port is not open, cannot write command: {message}")

		#add write termination
		term_message = f"{message}{self.write_termination}"

		#encode the terminated message using ASCII
		#TO DO: check for exceptions
		enc_message = term_message.encode(ENCODING)

		#write the ASCII-encoded command to the instrument
		self.connection.write(enc_message)

	def read(self):
		"""
		Low-level method to read a reply of the instrument.

		Returns
		-------
		str
			The decoded output of the instrument, without XON/XOFF characters 
			or the termination character.

		Raises
		------
		IOError
			When the port has not been opened before calling this method.
		"""

		if not self.is_open():
			raise IOError("Port is not open, cannot perform a reading")

		#encode the termination character using ASCII
		enc_termination = self.read_termination.encode(ENCODING)

		#read the message from the instrument using the encoded termination
		message = self.connection.read_until(enc_termination)

		#decode the instrument message from ASCII
		decoded_message = message.decode(ENCODING)

		#remove XON/XOFF characters, the termination character and return
		return decoded_message.strip("\x11\x13" + self.read_termination)

	def query(self, message):
		"""
		Low-level method to perform a query operation (write + read).

		Parameters
		----------
		message : str
			Command to send to the instrument, without the termination 
			character.

		Returns
		-------
		str
			The decoded output of the instrument, without XON/XOFF characters 
			or the termination character.

		Raises
		------
		IOError
			When the port has not been opened before calling this method.
		"""

		if not self.is_open():
			raise IOError(f"Port is not open, cannot perform query: {message}")

		self.write(message)
		return self.read()

	def is_open(self):
		"""
		Method to check whether the device file has been opened or not.

		Returns
		-------
		bool
			True if the device file for the instrument has been opened, False 
			otherwise.
		"""

		#first check if the connection attribute is None
		if self.connection is None:
			return False

		#return the status
		return self.connection.is_open

	def is_channel(self, channel):
		"""
		Method to check whether a string represents a valid channel name.

		Parameters
		----------
		channel : str
			The string with the channel name to be checked.

		Returns
		-------
		bool
			True if the channel parameter is a valid channel name for the 
			instrument, False otherwise.
		"""

		#list of valid channel names. Check all spellings
		channel_names = [val for sublist in self.channels for val in sublist]

		#check if the channel name is in the previous list
		is_channel_valid = (channel.upper() in channel_names)
		return is_channel_valid

	def _check_channel(self, channel):
		"""
		Private function for checking a channel name and raising an exception
		if the name is not valid.

		Parameters
		----------
		channel : str
			The string with the channel name to be checked.

		Raises
		------
		ValueError
			When the given string does not represent a valid instrument channel.
		"""

		if not self.is_channel(channel):
			raise ValueError(f"Invalid channel name: {channel}")

	def close(self):
		"""Closes the device file for the instrument."""

		if self.is_open():
			self.connection.close()
			self.connection = None
