"""
Module for handling the Keithley 2450 source-meter unit.

It relies on the Gpib package provided by Linux GPIB.
"""

try:
	import Gpib
except ModuleNotFoundError:
	raise ModuleNotFoundError("Could not import package `Gpib`. Make sure that "
		"this package is installed in order to interact with the Keithley "
		"source meter.") from None

from .instrument import Instrument

#vendor and model information for this module
VENDOR = "Keithley"
MODELS = ["2450"]

#encoding used for communication
ENCODING = "ASCII"


def _decode_reply(reply):
	"""
	Decodes the source meter replies.

	Parameters
	----------
	reply : str
		The source meter reply.

	Returns
	-------
	str
		The decoded reply.

	Raises
	------
	RuntimeError
		If the decoding of the source meter message fails.
	"""

	try:
		return reply.decode(ENCODING)
	except UnicodeDecodeError:
		raise RuntimeError("Could not decode the source meter reply!") from None


class Keithley2450(Instrument):
	"""
	Class used for controlling the Keithley 2450 source meter with the Python GPIB
	library.
	"""

	channels = [["FRON", "FRONT"], ["REAR"]]

	def __init__(self, port=18, device_file=None, baud_rate=None,
			device_name = "", device_model = "", write_termination = "\n",
			read_termination = "\n", xonxoff = 0, timeout = 1):
		"""
		Initializes the GPIB communication with the source-meter.

		The primary GPIB address is set to 0, while the secondary GPIB address
		can be passed as a parameter.

		Parameters
		----------
		port : secondary GPIB address.
		"""

		super().__init__(
			None, #device file
			None, #baud rate
			device_name=device_name,
			device_model=device_model,
			write_termination="",
			read_termination="",
			xonxoff=0,
			timeout=1
		)

		self.port = port

	def open(self):
		"""
		Initializes the GPIB communication with the Keithley.
		"""

		self.connection = Gpib.Gpib(0, self.port)

	def close(self):
		"""
		Dummy method that currently does not do anything.
		"""

	def write(self, message):
		"""
		Low-level method to send a generic command to the instrument.

		Parameters
		----------
		message : str
			Command to send to the instrument, without the termination 
			character.
		"""

		self.connection.write(message)

	def read(self):
		"""
		Reads a reply from the source meter.

		Returns
		-------
		str or None
			The reply of the source meter or None if something wrong happens 
			during the reading.
		"""

		try:
			reply = self.connection.read(100)
		except Exception:
			return None
		return reply.decode()

	def query(self, message):
		"""
		Low-level method to perform a query operation (write + read).

		Parameters
		----------
		message : str
			Command to send to the instrument, without the termination 
			character.

		Returns
		-------
		str
			The decoded output of the instrument.

		Raises
		------
		RuntimeError
			When the decoding of the reply fails.
		"""

		self.connection.write(message)
		reply = self.connection.read(100)
		try:
			decoded_reply = _decode_reply(reply)
		except RuntimeError as e:
			raise RuntimeError("Could not complete the query operation!") from e

		return decoded_reply

	def query_idn(self):
		"""
		Queries the instrument for the identification string.

		Returns
		-------
		str
			The identification string of the instrument.
		"""

		return self.query("*IDN?")

	def query_esr(self):
		"""
		Queries and clears the Event Status Register.

		Returns
		-------
		str
			The value of the Event Status Register.
		"""

		return self.query("*ESR?")

	def query_n_errors(self):
		"""
		Queries the instrument for the number of errors in the Error Queue.

		Returns
		-------
		int
			The number of errors in the Error Queue of the instrument.
		"""

		code = int(self.query("SYST:ERR:COUN?"))
		return code

	def query_next_error(self):
		"""
		Gets an error out of the Error Queue of the instrument.

		Returns
		-------
		str
			The next error in the Error Queue.
		"""

		return self.query("SYST:ERR:NEXT?")

	def query_error_queue(self):
		"""
		Queries the instrument for the Error Queue. The register is cleared
		afterwards.

		This command is not available for the Keithley 2450.

		Raises
		------
		NotImplementedError
			Unavailable command for the Keithley 2450.
		"""

		raise NotImplementedError("Command not available in Keithley 2450!")

	def set_channel(self, channel):
		"""Sets the active channel of the source-meter."""

		self._check_channel(channel)
		self.connection.write(f"ROUT:TERM {channel}")

	def query_channel(self):
		"""
		Queries the instrument for the active channel.

		Returns
		-------
		str
			The active channel of the instrument.
		"""

		return self.query("ROUT:TERM?")

	def query_output(self):
		"""
		Queries the instrument for the status of the output for the active
		channel.

		Returns
		-------
		str
			The status of the output of the instrument.
		"""

		return self.query("OUTP?")

	def clear_status(self):
		"""
		Clears various instrument registers.

		The Standard Event Register, Operation Event Register, Measurement Event
		Register and Questionable Event Register are cleared.
		"""

		self.connection.write("*CLS")

	def enable_output(self):
		"""Turns on the output of the instrument."""

		self.connection.write("OUTP ON")

	def disable_output(self):
		"""Turns off the output of the instrument."""

		self.connection.write("OUTP OFF")

	def read_output(self):
		"""
		Triggers and acquires readings from the instrument.

		The command performs an INITiate and then a FETCh. The INITiate command
		triggers a measurement cycle and stores the data in the Sample Buffer.
		This buffer is then read by sending the FETCh command.

		Returns
		-------
		str
			The results of the measurement.
		"""

		return self.query("READ?")

	def enable_beeper(self):
		"""Enables the instrument beeper."""

		self.connection.write("SYST:BEEP:STAT 1")

	def disable_beeper(self):
		"""Disables the instrument beeper."""

		self.connection.write("SYST:BEEP:STAT 0")

	def reset(self):
		"""Resets the instrument to the default settings, except for the remote
		interface settings."""

		self.connection.write("*RST")

	def is_channel(self, channel):
		"""
		Method to check whether a string represents a valid channel name.

		Parameters
		----------
		channel : str
			The string with the channel name to be checked.

		Returns
		-------
		bool
			True if the channel parameter is a valid channel name for the 
			instrument, False otherwise.
		"""

		#list of valid channel names. Check all spellings
		channel_names = [val for sublist in self.channels for val in sublist]

		#check if the channel name is in the previous list
		is_channel_valid = (channel.upper() in channel_names)
		return is_channel_valid

	def _check_channel(self, channel):
		"""
		Private function for checking a channel name and raising an exception
		if the name is not valid.

		Parameters
		----------
		channel : str
			The string with the channel name to be checked.

		Raises
		------
		ValueError
			When the given string does not represent a valid instrument channel.
		"""

		if not self.is_channel(channel):
			raise ValueError(f"Invalid channel name: {channel}")
