"""
Module used for interfacing TTi power supplies of the QL series.

The instruments are controlled using SCPI commands via serial communications.

The module has been developed and tested with a TTi QL355TP.
"""

import re
import time

from .instrument import Instrument

#vendor and model information for this module
VENDOR = "TTi"
MODELS = ["QL355TP"]


#function used to parse instrument replies
def _parse(input_string, expression_string):
	"""
	Private function used to parse the answers of the instrument.

	Parameters
	----------
	input_string : str
		String which has to be searched with a particular regex.
	expression_string : str
		Regular expression which has to be used on input_string. It must 
		contain one regex group named "target" for the _parse method to 
		work.

	Returns
	-------
	str
		The matching string contained in the "target" regex group.

	Raises
	------
	RuntimeError
		When the parsing of the instrument answer fails.
	"""

	match = re.search(expression_string, input_string)

	if match is None:
		raise RuntimeError("Parsing of instrument answer failed. Could not "
			f"parse '{input_string}' with regular expression "
			f"'{expression_string}'")

	return match.group("target")


class TTiQL(Instrument):
	"""
	Class used to interface with TTi power supplies of the QL series.

	It has been developed and tested considering a TTi QL355TP power supply.
	"""

	#list of valid channel names. Each entry is itself a list in order to allow
	#for variations. "3" is the auxiliary channel
	channels = [["1"], ["2"], ["3"]]

	#structure of the Limit Event Status Registers
	_lsr_dict = {
		#Limit Event Status Register 1
		"1": {
			"voltage_limit_ch1": {
				"channel": "1",
				"value": 2**0
			},
			"current_limit_ch1": {
				"channel": "1",
				"value": 2**1
			},
			"overvoltage_trip_ch1": {
				"channel": "1",
				"value": 2**2
			},
			"overcurrent_trip_ch1": {
				"channel": "1",
				"value": 2**3
			},
			"thermal_trip_ch1": {
				"channel": "1",
				"value": 2**4
			},
			"sense_trip_ch1": {
				"channel": "1",
				"value": 2**5
			},
		},
		#Limit Event Status Register 2
		"2": {
			"voltage_limit_ch2": {
				"channel": "2",
				"value": 2**0
			},
			"current_limit_ch2": {
				"channel": "2",
				"value": 2**1
			},
			"overvoltage_trip_ch2": {
				"channel": "2",
				"value": 2**2
			},
			"overcurrent_trip_ch2": {
				"channel": "2",
				"value": 2**3
			},
			"thermal_trip_ch2": {
				"channel": "2",
				"value": 2**4
			},
			"sense_trip_ch2": {
				"channel": "2",
				"value": 2**5
			},
			"current_limit_ch3": {
				"channel": "3",
				"value": 2**6
			}
		}
	}

	#mapping of channels to LSR1/LSR2
	_lsr_channel_map = {"1": "1", "2": "2", "3": "2"}

	def query_idn(self):
		"""
		Queries the instrument for the identification string.

		Returns
		-------
		str
			The identification string of the instrument.
		"""

		return self.query("*IDN?")

	def query_esr(self):
		"""
		Queries and clears the Event Status Register.

		Returns
		-------
		str
			The value of the Event Status Register.
		"""

		return self.query("*ESR?")

	def query_eer(self):
		"""
		Queries the instrument for the Execution Error Register. The register 
		is cleared afterwards.

		Returns
		-------
		str
			The value of the Execution Error Register.
		"""

		return self.query("EER?")

	def query_qer(self):
		"""
		Queries the instrument for the Query Error Register. The register is
		cleared afterwards.

		Returns
		-------
		str
			The value of the Query Error Register.
		"""

		return self.query("QER?")

	def query_lse(self, channel):
		"""
		Queries the instrument for the Limit Status Event Enable Register.
		The register is cleared afterwards.

		Parameters
		----------
		channel : str
			The name of the channel for which the LSE must be queried. It can 
			only be queried for channels 1 and 2.

		Returns
		-------
		str
			The value of the LSE register.

		Raises
		------
		ValueError
			When trying to query the LSE for the auxiliary output.
		"""

		self._check_channel(channel)

		if channel == "3":
			raise ValueError("The LSE register cannot be queried for auxiliary "
			"output")

		return self.query(f"LSE{channel}?")

	def query_lsr(self, index):
		"""
		Queries the instrument for the Limit Event Status Register. The
		register is cleared afterwards.

		Parameters
		----------
		index : str
			The index parameter can be 1 or 2, given that there are only two 
			Limit Event Status Registers.

		Returns
		-------
		int
			The value of the Limit Event Status Register 1 or 2.

		Raises
		------
		ValueError
			In case the index parameter is incorrect.
		RuntimeError
			If something goes wrong during the reading.
		"""

		#checking the index type
		try:
			int_index = int(index)
		except ValueError:
			raise ValueError("Invalid value for index parameter. Valid values: "
				"1, 2") from None

		#check the index
		if int_index not in [1, 2]:
			raise ValueError(f"Invalid index for LSR register: {int_index}")

		#query the status
		status_lsr = self.query(f"LSR{int_index}?")

		try:
			status_lsr_int = int(status_lsr)
		except ValueError as e:
			raise RuntimeError("Something went wrong while reading the status "
				"of the LSR register!") from e

		return status_lsr_int

	def get_trip_status(self, channel):
		"""
		Checks whether any trip event occurred for the provided channel.

		The Limit Event Status Register (LSR) for the relevant channel is
		queried and the answer is translated to human-readable format and
		returned.

		Parameters
		----------
		channel : str
			The name of the channel for which the trip status must be checked.

		Returns
		-------
		dict
			Dictionary containing the trip status. The keys are the trip names 
			and the values are booleans (True for occurred trip).
		"""

		self._check_channel(channel)

		#getting the index of the LSR associated to the channel
		index = TTiQL._lsr_channel_map[channel]

		#query the relevant LSR
		register_value = self.query_lsr(index)

		#get all register fields
		all_fields = TTiQL._lsr_dict[index]

		#select only the ones related to the selected channel
		channel_fields = [
			field
			for field in all_fields
			if all_fields[field]["channel"] == channel
		]

		#select only trips for the given channel
		channel_trip_fields = [
			field
			for field in channel_fields
			if "trip" in field
		]

		#status dictionary
		status_dict = dict()

		#loop on all fields
		for field in channel_trip_fields:
			#remove the channel suffix
			field_stripped = field.replace("_ch" + str(channel), "")

			#get the status as a bool
			bit_value = all_fields[field]["value"]
			bit_status = (register_value & bit_value) / bit_value
			bit_status_bool = bool(bit_status)

			#update the status dictionary
			status_dict[field_stripped] = bit_status_bool

		return dict(status_dict)

	def reset_trip(self):
		"""Tries to clear the trip condition from all outputs."""

		#remove the trip condition
		self.write("TRIPRST")

		#clear the LSR1 and LSR2 by reading them
		self.query_lsr("1")
		self.query_lsr("2")

	def set_voltage(self, channel, voltage):
		"""
		Sets the voltage limit for the selected channel.

		When this value is reached, the instrument switches to constant voltage
		mode.

		Parameters
		----------
		channel : str
			The name of the channel for which the voltage limit must be set.
		voltage : float or int
			Voltage limit to set on the chosen channel.
		"""

		self._check_channel(channel)
		self.write(f"V{channel} {voltage}")

	def query_voltage(self, channel):
		"""
		Queries the voltage limit for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the voltage limit must be queried.

		Returns
		-------
		float
			The set voltage limit for the given channel.
		"""

		self._check_channel(channel)
		answer = self.query(f"V{channel}?")
		regex = fr"V{channel} (?P<target>\d+.\d+)"
		voltage = _parse(answer, regex)

		return float(voltage)

	def query_output_voltage(self, channel):
		"""
		Queries the output voltage for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the output voltage must be 
			queried.

		Returns
		-------
		float
			The output voltage for the given channel.
		"""

		self._check_channel(channel)
		answer = self.query(f"V{channel}O?")
		regex = r"(?P<target>\d+.\d+)V"
		voltage = _parse(answer, regex)

		return float(voltage)

	def set_ovp(self, channel, ovp):
		"""
		Sets the overvoltage protection for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the OVP must be set.
		ovp : float or int
			The overvoltage value to set.
		"""

		self._check_channel(channel)
		self.write(f"OVP{channel} {ovp}")

	def query_ovp(self, channel):
		"""
		Queries the overvoltage protection setting for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the OVP must be queried.
		"""

		self._check_channel(channel)
		answer = self.query(f"OVP{channel}?")
		regex = r"VP[12] (?P<target>\d+.\d+)"
		voltage = _parse(answer, regex)

		return float(voltage)

	def set_current(self, channel, current):
		"""
		Sets the current limit for the selected channel.

		When this value is reached, the instrument switches to constant current
		mode.

		Parameters
		----------
		channel : str
			The name of the channel for which the current limit must be set.
		current: float or int
			The current limit to set.

		Raises
		------
		ValueError
			If trying to set the current of the auxiliary output.
		"""

		self._check_channel(channel)

		if channel == "3":
			raise ValueError("Cannot set current for auxiliary output")

		self.write(f"I{channel} {current}")

	def query_current(self, channel):
		"""
		Queries the current limit for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the current limit must be queried.

		Returns
		-------
		float
			The set current limit for the given channel.

		Raises
		------
		ValueError
			If trying to query the current of the auxiliary output.
		"""

		self._check_channel(channel)

		if channel == "3":
			raise ValueError("Cannot query current for auxiliary output")

		answer = self.query(f"I{channel}?")
		regex = fr"I{channel} (?P<target>\d+.\d+)"
		current = _parse(answer, regex)

		return float(current)

	def query_output_current(self, channel):
		"""
		Queries the output voltage for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the output current must be 
			queried.

		Returns
		-------
		float
			The output current for the given channel.
		"""

		self._check_channel(channel)

		answer = self.query(f"I{channel}O?")
		regex = r"(?P<target>\d+.\d+)A"
		current = _parse(answer, regex)

		return float(current)

	def set_ocp(self, channel, ocp):
		"""
		Sets the overcurrent protection for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the OCP must be set.
		ocp : float or int
			The overcurrent value to set.
		"""

		self._check_channel(channel)
		self.write(f"OCP{channel} {ocp}")

	def query_ocp(self, channel):
		"""
		Queries the overcurrent protection setting for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the OCP must be queried.
		"""

		self._check_channel(channel)

		answer = self.query(f"OCP{channel}?")
		regex = r"CP[12] (?P<target>\d+.\d+)"
		voltage = _parse(answer, regex)

		return float(voltage)

	def enable_output(self, channel):
		"""
		Sets the output of the selected channel on.

		Parameters
		----------
		channel : str
			The name of the channel.
		"""

		self._check_channel(channel)
		self.write(f"OP{channel} 1")

	def disable_output(self, channel):
		"""
		Sets the output of the selected channel off.

		Parameters
		----------
		channel : str
			The name of the channel.
		"""

		self._check_channel(channel)
		self.write(f"OP{channel} 0")

	def query_output(self, channel):
		"""
		Queries the status of the output for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel.

		Returns
		-------
		str
			The status of the selected channel: '0' if the channel is off and 
			'1' if it is on.
		"""

		self._check_channel(channel)
		answer = self.query(f"OP{channel}?")

		return answer

	def toggle_output(self, channel):
		"""
		Toggles the status of the output for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel.
		"""

		self._check_channel(channel)

		current_status_string = self.query_output(channel)
		is_on = (current_status_string == "1")

		if is_on:
			self.disable_output(channel)
		else:
			self.enable_output(channel)

	def set_range(self, channel, range_):
		"""
		Sets the voltage range for the selected output.

		The valid range values are:
			0: 15 V (5 A)
			1: 35 V (3 A)
			2: 35 V (500 mA)

		Parameters
		----------
		channel : str
			The name of the channel.
		range_ : int
			The voltage range to set. The possible values are: 0, 1, 2.

		Raises
		------
		ValueError
			If the range_ parameter is not one of 0, 1, 2.
		"""

		self._check_channel(channel)

		if range_ not in range(0, 3):
			raise ValueError("Wrong value supplied for the voltage range: "
			f"{range_}")

		self.write(f"RANGE{channel} {range_}")

	def query_range(self, channel):
		"""
		Gets the voltage range for the selected output.

		The valid range values are:
			0: 15 V (5 A)
			1: 35 V (3 A)
			2: 35 V (500 mA)
		"""

		self._check_channel(channel)

		answer = self.query(f"RANGE{channel}?")
		regex = fr"R{channel} (?P<target>\d)"
		range_val = _parse(answer, regex)

		return int(range_val)

	def enable_all_outputs(self):
		"""Sets the output of all instrument channels on."""

		self.write("OPALL 1")

	def disable_all_outputs(self):
		"""Sets the output of all instrument channels off."""

		self.write("OPALL 0")

	def clear_status(self):
		"""
		Clears various instrument registers.

		The Standard Event Status Register, Query Error Register and Execution
		Error Register are cleared. This also clears the Status Byte Register
		indirectly.
		"""

		self.write("*CLS")

	def set_link_mode(self, mode):
		"""
		Sets the link mode for the instrument.

		The valid values for link mode can be 0 (linked), 1 (control to channel
		1) or 2 (control to channel 2).

		Parameters
		----------
		mode : int
			Value to set for link mode.

		Raises
		------
		TypeError
			If the 'mode' parameter is not an integer.
		ValueError
			If 'mode' has a value that is not one of: [0, 1, 2].
		"""

		#checking type of `mode` parameter
		if not isinstance(mode, int):
			raise TypeError("The 'mode' parameter must be an integer!")

		#checking the value of the `mode` parameter
		if mode not in [0, 1, 2]:
			raise ValueError("Invalid value for parameter 'mode'!")

		self.write(f"MODE {mode}")

	def set_sense_mode(self, channel, status):
		"""
		Sets the sensing mode of the power supply.

		Parameters
		----------
		channel : str
			The name of the power supply channel to configure.
		status : int
			The status of the sense mode to apply. Valid values are 0 for 
			setting local sensing and 1 for remote sensing.

		Raises
		------
		TypeError
			If the `status` parameter is not an integer.
		ValueError
			If the `status` is neither 0 nor 1.
		"""

		#checking the `channel` parameter. Reraise the `ValueError` exception
		#raised by the private method `_check_channel`
		try:
			self._check_channel(channel)
		except ValueError:
			raise

		#checking the type of the `status` parameter
		if isinstance(status, int) is False:
			raise TypeError("The status parameter must be an integer!")

		#checking the value of the `status` parameter
		if status not in [0, 1]:
			raise ValueError(f"Invalid value for status parameter: {status}." 
				"Must be either 0 or 1")

		self.write(f"SENSE{channel} {status}")

	def reset(self):
		"""
		Resets the instrument to the default settings, except for the remote 
		interface settings.

		After the reset, a small delay is set (500 ms) in order to wait for the 
		operation to complete.
		"""

		self.write("*RST")
		time.sleep(0.5)
