"""
Second version of the Torino probe card (v1.1).

A new I2C expander on address 0x24 has been added in order to handle the new 
voltage divider circuit for the Arduino DACs.
"""

from wlt.duts.chips import ITKPIX_V1, ITKPIX_V2
from wlt.duts.chips import CROC_V1, CROC_V2

CROC_PROBE_CARD_V1P1 = {
	#list of chip types that can be probed with this probe card
	"chip_types": [ITKPIX_V1, CROC_V1, ITKPIX_V2, CROC_V2],
	#list of valid I2C addresses
	"i2c_addr": [0x20, 0x21, 0x22, 0x24],
	#size of probe card registers
	"i2c_reg_size": 8,
	#mapping of the multiplexer on the probe card
	"mux": {
		"VINA_SNS": {
			"address": 0x0,
			"ground": "GNDA_REF2"
		},
		"VIND_SNS": {
			"address": 0x1,
			"ground": "GNDD_REF",
		},
		"VDDA": {
			"address": 0x2,
			"ground": "GNDA_REF2"
		},
		"VDDD": {
			"address": 0x3,
			"ground": "GNDD_REF"
		},
		"VDD_CML": {
			"address": 0x4,
			"ground": "GNDA_REF2"
		},
		"VDD_PLL": {
			"address": 0x5,
			"ground": "GNDA_REF2"
		},
		"VDD_PRE": {
			"address": 0x6,
			"ground": "GNDA_REF2"
		},
		"IMUX_OUT": {
			"address": 0x7,
			"ground": "GNDA_REF1"
		},
		"VMUX_OUT": {
			"address": 0x8,
			"ground": "GNDA_REF1"
		},
		"R_IREF": {
			"address": 0x9,
			"ground": "GNDA_REF1"
		},
		"NTC": {
			"address": 0xA,
			"ground": "GNDA_REF1"
		},
		"VREF_ADC": {
			"address": 0xB,
			"ground": "GNDA_REF1"
		},
		"VOFS": { #34.9 kohm resistor to GND
			"address": 0xC,
			"ground": "GNDA_REF2"
		},
		"VOFS_LP": {
			"address": 0xD,
			"ground": "GNDA_REF2"
		},
		"REXTA": {
			"address": 0xE,
			"ground": "GNDA_REF2"
		},
		"REXTD": {
			"address": 0xF,
			"ground": "GNDD_REF"
		},
		"VREFA": {
			"address": 0x10,
			"ground": "GNDA_REF2"
		},
		"VREFD": {
			"address": 0x11,
			"ground": "GNDD_REF"
		},
		"VREF_OVP": {
			"address": 0x12,
			"ground": "GNDD_REF"
		},
		"GNDA_REF1": {
			"address": 0x13,
			"ground": "GND"
		},
		"GNDD_REF": {
			"address": 0x14,
			"ground": "GND"
		},
		"GND": {
			"address": 0x15,
			"ground": "GND"
		},
		"VDD_EFUSE": {
			"address": 0x16,
			"ground": "GND"
		},
		"GNDA_REF2": {
			"address": 0x17,
			"ground": "GND"
		},
		"VDD_SHUNT": {
			"address": 0x18,
			"ground": "GNDD_REF"
		},
		"ES1": {
			"address": 0x19,
			"ground": "GND"
		},
		"ES2": {
			"address": 0x1A,
			"ground": "GND"
		},
		"NC28": {
			"address": 0x1B,
			"ground": "GND"
		},
		"VIND_PC": {
			"address": 0x1C,
			"ground": "GNDD_REF"
		},
		"NC30": {
			"address": 0x1D,
			"ground": "GND"
		},
		"VINA_PC": {
			"address": 0x1E,
			"ground": "GNDA_REF2"
		},
		"NC32": {
			"address": 0x1F,
			"ground": "GND"
		},
		#for compatibility purposes
		"VDDA_PC": {
			"address": 0x2, #VDDA_SNS
			"ground": "GNDA_REF2"
		},
		"VDDD_PC": {
			"address": 0x3, #VDDD_SNS
			"ground": "GNDD_REF"
		}
	},
	#default value to select when the multiplexer is not in use
	"default_mux": "NC32",
	"gpio": {
		### 0x20 ###
		"EN_R_IREF": { #0b00000001
			"address": 0x20,
			"offset": 0,
			"size": 1,
			"default": 1
		},
		"EN_R_IMUX": { #0b00000010
			"address": 0x20,
			"offset": 1,
			"size": 1,
			"default": 1
		},
		"MUX_SEL": { #0b01111100
			"address": 0x20,
			"offset": 2,
			"size": 5,
			"default": 31
		},
		"EN_REXT": { #0b10000000
			"address": 0x20,
			"offset": 7,
			"size": 1,
			"default": 1
		},
		### 0x21 ###
		"CHIP_ID": { #0b00001111
			"address": 0x21,
			"offset": 0,
			"size": 4,
			"default": 0
		},
		"BYPASS": { #0b00010000
			"address": 0x21,
			"offset": 4,
			"size": 1,
			"default": 0
		},
		"TEST": { #0b00100000
			"address": 0x21,
			"offset": 5,
			"size": 1,
			"default": 0
		},
		"EN_VDD_SHUNT": { #0b01000000
			"address": 0x21,
			"offset": 6,
			"size": 1,
			"default": 0
		},
		"EN_VDD_EFUSE": { #0b10000000
			"address": 0x21,
			"offset": 7,
			"size": 1,
			"default": 0
		},
		### 0x22 ###
		"IREF_TRIM": { #0b00001111
			"address": 0x22,
			"offset": 0,
			"size": 4,
			"default": 7
		},
		"PLL_VCTRL_RST_B": { #0b00010000
			"address": 0x22,
			"offset": 4,
			"size": 1,
			"default": 1
		},
		"SCAN": { #0b00100000
			"address": 0x22,
			"offset": 5,
			"size": 1,
			"default": 0
		},
		"ES": { #0b11000000
			"address": 0x22,
			"offset": 6,
			"size": 2,
			"default": None
		},
		### 0x24 ###
		"EN_MUX_ARDU": { #0b00000001
			"address": 0x24,
			"offset": 0,
			"size": 1,
			"default": 0
		},
		"EN_DAC1/2": { #0b00000010
			"address": 0x24,
			"offset": 1,
			"size": 1,
			"default": 0
		},
		"EN_DAC1/4": { #0b00000100
			"address": 0x24,
			"offset": 2,
			"size": 1,
			"default": 0
		}
	}
}
