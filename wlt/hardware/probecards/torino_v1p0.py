"""
First version of the Torino probe card (v1.0).

VOFS_LP must be used as a ground because of a hardware bug.
"""

from wlt.duts.chips import ITKPIX_V1, ITKPIX_V2
from wlt.duts.chips import CROC_V1, CROC_V2

CROC_PROBE_CARD_V1P0 = {
	#list of chip types that can be probed with this probe card
	"chip_types": [ITKPIX_V1, CROC_V1, ITKPIX_V2, CROC_V2],
	#list of valid I2C addresses
	"i2c_addr": [0x20, 0x21, 0x22],
	#size of probe card registers
	"i2c_reg_size": 8,
	#mapping of the multiplexer on the probe card
	"mux": {
		"VINA_SNS": {
			"address": 0x0,
			"ground": "VOFS_LP"
		},
		"VIND_SNS": {
			"address": 0x1,
			"ground": "VOFS_LP"
		},
		"VDDA": {
			"address": 0x2,
			"ground": "VOFS_LP"
		},
		"VDDD": {
			"address": 0x3,
			"ground": "VOFS_LP"
		},
		"VDD_CML": {
			"address": 0x4,
			"ground": "VOFS_LP"
		},
		"VDD_PLL": {
			"address": 0x5,
			"ground": "VOFS_LP"
		},
		"VDD_PRE": {
			"address": 0x6,
			"ground": "VOFS_LP"
		},
		"IMUX_OUT": {
			"address": 0x7,
			"ground": "VOFS_LP"
		},
		"VMUX_OUT": {
			"address": 0x8,
			"ground": "VOFS_LP"
		},
		"R_IREF": {
			"address": 0x9,
			"ground": "VOFS_LP"
		},
		"NTC": {
			"address": 0xA,
			"ground": "VOFS_LP"
		},
		"VREF_ADC": {
			"address": 0xB,
			"ground": "VOFS_LP"
		},
		"VOFS": {
			"address": 0xC,
			"ground": "VOFS_LP"
		}, #34.9 kohm resistor to GND
		"VOFS_LP": {
			"address": 0xD,
			"ground": "GND"
		},
		"REXTA": {
			"address": 0xE,
			"ground": "VOFS_LP"
		},
		"REXTD": {
			"address": 0xF,
			"ground": "VOFS_LP"
		},
		#mux channels from 16 to 31 are affected by the multiplexer bug, so they
		#cannot be read on the CROC probe card v1.0. As a precaution, they are
		#set to VOFS_LP
		"VREFA": {
			"address": 0xD,
			"ground": "VOFS_LP"
		},
		"VREFD": {
			"address": 0xD,
			"ground": "VOFS_LP"
		},
		"VREF_OVP": {
			"address": 0xD,
			"ground": "VOFS_LP"
		},
		"GNDA_REF": {
			"address": 0xD,
			"ground": "GND"
		},
		"GNDD_REF": {
			"address": 0xD,
			"ground": "GND"
		},
		"GND": {
			"address": 0xD,
			"ground": "GND"
		},
		#virtual multiplexer outputs used to keep compatibility between probe
		#cards
		"GNDA_REF1": {
			"address": 0xD, #VOFS_LP
			"ground": "GND"
		},
		"GNDA_REF2": {
			"address": 0xD, #VOFS_LP
			"ground": "GND"
		},
		"VINA_PC": {
			"address": 0x0, #VINA_SNS
			"ground": "GNDA_REF2"
		},
		"VIND_PC": {
			"address": 0x1, #VIND_SNS
			"ground": "GNDD_REF"
		},
		"VDDA_PC": {
			"address": 0x2, #VDDA_SNS
			"ground": "GNDA_REF2"
		},
		"VDDD_PC": {
			"address": 0x3, #VDDD_SNS
			"ground": "GNDD_REF"
		}
		#the others are not connected
	},
	#default value to select when the multiplexer is not in use
	"default_mux": "VINA_SNS",
	#GPIO signals on probe card
	"gpio": {
		### 0x20 ###
		"EN_R_IREF": { #0b00000001
			"address": 0x20,
			"offset": 0,
			"size": 1,
			"default": 1
		},
		"EN_R_IMUX": { #0b00000010
			"address": 0x20,
			"offset": 1,
			"size": 1,
			"default": 1
		},
		"MUX_SEL": { #0b01111100
			"address": 0x20,
			"offset": 2,
			"size": 5,
			"default": 2
		},
		"EN_REXT": { #0b10000000
			"address": 0x20,
			"offset": 7,
			"size": 1,
			"default": 1
		},
		### 0x21 ###
		"CHIP_ID": { #0b00001111
			"address": 0x21,
			"offset": 0,
			"size": 4,
			"default": 0
		},
		"BYPASS": { #0b00010000
			"address": 0x21,
			"offset": 4,
			"size": 1,
			"default": 0
		},
		"TEST": { #0b00100000
			"address": 0x21,
			"offset": 5,
			"size": 1,
			"default": 0
		},
		"EN_VDD_SHUNT": { #0b01000000
			"address": 0x21,
			"offset": 6,
			"size": 1,
			"default": 0
		},
		"EN_VDD_EFUSE": { #0b10000000
			"address": 0x21,
			"offset": 7,
			"size": 1,
			"default": 0
		},
		### 0x22 ###
		"IREF_TRIM": { #0b00001111
			"address": 0x22,
			"offset": 0,
			"size": 4,
			"default": 7
		},
		"PLL_VCTRL_RST_B": { #0b00010000
			"address": 0x22,
			"offset": 4,
			"size": 1,
			"default": 1
		},
		"ES": { #0b11000000
			"address": 0x22,
			"offset": 6,
			"size": 2,
			"default": None
		}
	}
}
