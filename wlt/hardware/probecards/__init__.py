"""Subpackage for handling probe card definitions."""

from .torino_v1p0 import CROC_PROBE_CARD_V1P0
from .torino_v1p1 import CROC_PROBE_CARD_V1P1
from .torino_v2p0 import CROC_PROBE_CARD_V2P0

#list of all available probe cards
PROBE_CARDS = [CROC_PROBE_CARD_V1P0, CROC_PROBE_CARD_V1P1, CROC_PROBE_CARD_V2P0]
