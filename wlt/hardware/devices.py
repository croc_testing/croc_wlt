"""Module that defines the wafer-level testing hardware in use."""

from .wpac import Wpac
from ..config import WPAC_CONF
from ..config import PROBER_CLS, PROBER_ADDR

#initialisation of shared objects
_prober = PROBER_CLS(PROBER_ADDR)
_wpac = Wpac(**WPAC_CONF)


def get_wpac():
	"""
	Returns the global WPAC object.

	If communication has not been initialised already, it is done in this 
	function.

	Returns
	-------
	Wpac
		The WPAC object.
	"""

	if _wpac.is_open() is False:
		_wpac.open()
	return _wpac


def get_prober():
	"""
	Returns the global prober object.

	No additional actions are performed.
	"""
	return _prober
