"""Default versions configuration to be updated at runtime if needed"""

import os
import sys
import importlib
import inspect
import wlt.config

# Defining the default version -> should be the latest available
CONFIG_ID = '13'
REGIONS_FILE = None


########### Utility functions for loading the necessary configuration and region definitions

def config_dir(version):
	"""Generates the folder path for the configured analysis version"""
	init_dir = os.path.dirname(inspect.getfile(wlt.config))
	return os.path.join(init_dir, f'analyzer_v{version}')


def load_config(version=CONFIG_ID, config_path=None):
	"""Loads analysis configuration according to the specified version"""
	if version is None:
		version = CONFIG_ID
	if config_path is None:
		config_path = os.path.join(config_dir(version), 'analyzer_config.py')
	# Ensuring that the configuration file exists
	if not os.path.isfile(config_path):
		print(f'ERROR: No configuration file found: {config_path}')
		sys.exit(1)
	# Loading the module
	config_loader = importlib.util.spec_from_file_location('CONFIG', config_path)
	config = importlib.util.module_from_spec(config_loader)
	config_loader.loader.exec_module(config)
	print(f'Loaded plot configurations from:\n {config_path}')
	# Checking that the loaded configuration has a consistent version definition
	config_version = config.CONFIG['GLOBAL']['version']
	if version is not None and config_version != version:
		raise RuntimeWarning(f'Misleading `version` in `GLOBAL` section: `{config_version}` vs `{version}`')

	return config.CONFIG


def load_regions(version):
	"""Loads region definitions according to the specified version"""
	regions_path = REGIONS_FILE
	# Using default regions file for the version
	if not REGIONS_FILE:
		regions_path = os.path.join(config_dir(version), 'analyzer_regions.py')
	# Ensuring that the regions file exists
	if not os.path.isfile(regions_path):
		print(f'ERROR: No regions-definition file found: {regions_path}')
		sys.exit(1)
	# Loading the module
	regions_loader = importlib.util.spec_from_file_location('REGIONS', regions_path)
	regions = importlib.util.module_from_spec(regions_loader)
	regions_loader.loader.exec_module(regions)
	print(f'Loaded region definitions from:\n {regions_path}')
	# Validating binnings of each region
	bins_valid = True
	for name, config in regions.REGIONS.items():
		bounds, bins = config[:2]
		for bound in bounds[1:]:
			# Calculating the deviation from a bin boundary in %
			bin_deviation = round((((bound - bounds[0])%bins) / bins) * 100)
			if 0 < bin_deviation < 100:
				print(f'WARNING: Inconsistent binning for regions of `{name}` -> {bounds} / {bins}')
				bins_valid = False
				break

	return regions.REGIONS
