"""Module with definitions of DAQ exceptions."""


class DAQException(Exception):
	"""Base exception for the `daq` subpackage in case of problems when running 
	the DAQ."""


class DAQTimeout(DAQException):
	"""Exception raised when a Ph2_ACF run times out."""


class DAQError(DAQException):
	"""Exception raised when a Ph2_ACF run returns a non-zero exit code."""
