"""
Module which contains definitions related to the RD53 chips.
"""

##############
# chip types #
##############

#RD53B chips
ITKPIX_V1 = "ITkPixv1"
CROC_V1 = "CROCv1"

#RD53C chips
ITKPIX_V2 = "ITkPixv2"
CROC_V2 = "CROCv2"

#list of all valid chip types
CHIPS = [ITKPIX_V1, ITKPIX_V2, CROC_V1, CROC_V2]

#chip types organised by generation
RD53B_CHIPS = [CROC_V1, ITKPIX_V1]
RD53C_CHIPS = [CROC_V2, ITKPIX_V2]

#dictionary containing data related to the chips, such as the number of
#pixel/core columns and rows
CHIP_DATA = {
	ITKPIX_V1: {
		"pixel_cols": 400,
		"pixel_rows": 384,
		"pixel_count": 153600,
		"core_cols": 50,
		"core_rows": 48,
		"core_count": 2400
	},
	ITKPIX_V2: {
		"pixel_cols": 400,
		"pixel_rows": 384,
		"pixel_count": 153600,
		"core_cols": 50,
		"core_rows": 48,
		"core_count": 2400
	},
	CROC_V1: {
		"pixel_cols": 432,
		"pixel_rows": 336,
		"pixel_count": 145152,
		"core_cols": 54,
		"core_rows": 42,
		"core_count": 2268
	},
	CROC_V2: {
		"pixel_cols": 432,
		"pixel_rows": 336,
		"pixel_count": 145152,
		"core_cols": 54,
		"core_rows": 42,
		"core_count": 2268
	},
}
