"""
Subpackage with definitions related to the Devices Under Test (DUTs) for 
wafer-level testing.

Modules:
	chips: contains chip-related constant

	wafer: contains the wafer maps and additional wafer-related data
"""

import os
import sqlite3
import importlib.resources
from dataclasses import dataclass

from wlt.duts.chips import CROC_V2


#Python data class used to store information about wafers, such as the foundry
#batch or the identifier
@dataclass
class Wafer:
	"""Dataclass used for storing wafer data."""

	#wafer attributes
	#the wafer map should also be added here
	chip_type: str
	foundry_batch: str
	foundry_batch_number: int
	foundry_id: str
	waferprobing_id: int

	def label(self):
		"""Generates a label for the DCA database"""
		if self.chip_type == CROC_V2:
			return f'{self.foundry_batch}_{self.foundry_id}'
		return f'{self.foundry_batch_number:d}_{self.foundry_id}'


#file containing wafer data (batch, serial and waferprobing identifier)
WAFER_DATA_FILE = "wafers.sqlite"

#list of all registered wafers available from local database
WAFERS = []


def load_wafers_db():
	"""Reads the wafers database and loads all registered wafers in the 
	`WAFERS` global list."""

	#reading the database
	with importlib.resources.path(__package__, WAFER_DATA_FILE) as path:
		#opening the SQLite3 database
		conn = sqlite3.connect(path)

	#getting the data for all wafers from the database
	cursor = conn.execute("SELECT * FROM wafers")
	entries = cursor.fetchall()

	#adding all wafers to the global list
	for entry in entries:
		wafer = Wafer(chip_type=entry[4], foundry_batch=entry[0],
			foundry_batch_number=entry[1], foundry_id=entry[2],
			waferprobing_id=entry[3])
		WAFERS.append(wafer)


#load the database and store the wafers information
load_wafers_db()


def get_wafer(batch, id_):
	"""
	Returns the Wafer object from the selected batch and with the required 
	id.

	Parameters
	----------
	batch, id_ : str
		Foundry batch and ID for the wafer.

	Returns
	-------
	Wafer or None
		Wafer data class or None if no matching wafers have been found.
	"""

	#get the wafer with the required batch and id; if no wafer matches
	try:
		wafer = [wafer for wafer in WAFERS if wafer.foundry_batch == batch and
			wafer.foundry_id == id_][0]
	except IndexError:
		wafer = None

	return wafer


def get_waferprobing_id(batch, id_):
	"""
	Returns the waferprobing ID of the wafer with the requested batch and id.

	Parameters
	----------
	batch, id_ : str
		Foundry batch and ID for the wafer.

	Returns
	-------
	int or None
		The waferprobing ID or None if no matching wafers have been found.
	"""

	#getting the waferprobing ID from the CSV data and converting it to an
	#integer before returning it
	wafer = get_wafer(batch, id_)
	return wafer.waferprobing_id if wafer is not None else None
