#!/bin/bash
BATCH=$1
# First call for authentication
./db_reader.py -2 -w "+${BATCH}*"
# Second call for actually downloading the data
echo "+ Checking each wafer:"
./db_reader.py -2 -w "+${BATCH}*" | grep "^${BATCH}" | while read line; do w=$(echo $line | cut -w -f1); echo $w; ./db_reader.py -w $w -c - | grep "Error"; done
echo "+ Done"