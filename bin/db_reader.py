#!/usr/bin/env python3

from __future__ import print_function

import pdb
import glob
import sqlite3
import csv
from io import StringIO

import requests
import re
import os
import json
import sys
import logging
import tempfile
import subprocess
import warnings
from base64 import b64encode, b64decode
import xml.etree.ElementTree as ET

from bs4 import BeautifulSoup
import urllib3
from urllib.parse import urlparse
from requests.utils import requote_uri

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
requests.packages.urllib3.disable_warnings()
# warnings.filterwarnings("error")

if sys.version_info < (3,):
    from cookielib import Cookie, MozillaCookieJar
else:
    from http.cookiejar import Cookie, MozillaCookieJar

def findformeotppage(html_content):
    soup = BeautifulSoup(html_content, 'html.parser')
    otp_form = soup.find('form', {'id': 'kc-otp-login-form'})
    action_url = otp_form['action']
    method = otp_form['method']
    otp = input("Otp: ")
    
    otp_input = otp_form.find('input', {'id': 'otp'})
    otp_input['value'] = otp
    form_data = {}
    for input_tag in otp_form.find_all('input'):
        form_data[input_tag.get('name')] = input_tag.get('value', '')
    return action_url,form_data

def login_page1(html_content,username,password):
    soup = BeautifulSoup(html_content, 'html.parser')
    login_form = soup.find('form', {'id': 'kc-form-login'})
    action_url = login_form['action']
    form_data = {}
    for input_tag in soup.find_all('input'):
        name = input_tag.get('name')
        value = input_tag.get('value', '')
        form_data[name] = value

    form_data['username'] = username
    form_data['password'] = password

    return action_url,form_data

class CernSSO:
    DEFAULT_TIMEOUT_SECONDS = 10

    def load_cookies_from_mozilla(self, filename):
        ns_cookiejar = MozillaCookieJar()
        ns_cookiejar.load(filename, ignore_discard=True, ignore_expires=True)
        return ns_cookiejar

    def krb_sign_on(self, url, cookiejar={}, force_level=0):
        tfile = tempfile.mktemp()
        cmd = 'auth-get-sso-cookie -u "%s" -o "%s" -v --nocertverify' % (url, tfile)
        with warnings.catch_warnings():
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        logging.debug("%s returned %s" % (cmd, p.returncode))
        logging.debug(p.stdout.read())
        err = p.stderr.read()
        logging.debug(err)
        cookies = self.load_cookies_from_mozilla(tfile)
        os.remove(tfile)
        return cookies

    def file_mtime(self, file_path):
        try:
            return os.path.getmtime(file_path)
        except OSError:
            return -1

    def html_root(self, r):
        c = r.content.decode('utf-8')
        #dump = open('data.txt','w')
        #dump.write(c)
        #dump.close()
        c = re.sub('<meta [^>]*>', '', c, flags=re.IGNORECASE)
        c = re.sub('<hr>', '', c, flags=re.IGNORECASE)
        c = re.sub("=\\'([^']*)\\'", '="\g<1>"', c, flags=re.IGNORECASE)
        c = re.sub(" autofocus ", " ", c, flags=re.IGNORECASE)
        c = re.sub('<img ([^>]*)>', '<img \g<1>/>', c, flags=re.IGNORECASE)
        c = re.sub('<script>[^<]*</script>', '', c, flags=re.IGNORECASE)
        return ET.fromstring(c)

    def read_form(self, r):
        root = self.html_root(r)
        #form = root.find(".//{http://www.w3.org/1999/xhtml}form")
        form = root.find(".//form")
        action = form.get('action')
        form_data = dict(
            #((e.get('name'), e.get('value')) for e in form.findall(".//{http://www.w3.org/1999/xhtml}input")))
            ((e.get('name'), e.get('value')) for e in form.findall(".//input")))
        return action, form_data

    def is_email(self, s):
        regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
        return re.search(regex, s)

    def split_url(self, url):
        a = len(urlparse(url).query) + len(urlparse(url).path)
        return url[:-(a + 1)], url[len(url) - a - 1:]

    def login_sign_on(self, url,login_type, cache_file=".session.cache", force_level=0):

        from ilock import ILock
        from getpass import getpass
        from os import remove, path

        cache = None
        cache_file = path.abspath(cache_file)
        cache_lock_id = b64encode(cache_file.encode('utf-8')).decode()
        cache_time = self.file_mtime(cache_file)

        with ILock(cache_lock_id):

            if force_level == 1 and cache_time != self.file_mtime(cache_file):
                force_level = 0

            if force_level == 2:
                remove(cache_file)

            if path.isfile(cache_file):

                logging.debug('%s found', cache_file)
                with open(cache_file, 'r') as f:
                    cache = json.loads(f.read())

            else:

                logging.debug('%s not found', cache_file)

            if force_level > 0 or cache is None or 'cookies' not in cache:

                if cache is not None and 'secret' in cache:
                    secret = b64decode(cache['secret'].encode()).decode()
                    username, password = secret.split('/')
                    password = b64decode(password.encode()).decode()
                else:
                    username = input("Username: ")
                    password = getpass("Password: ")
                    logging.warning('Credentials will be stored in a NOT secure way!')

                with requests.Session() as s:

                    r1 = s.get(url, timeout=10, verify=False, allow_redirects=True)
                    r1.raise_for_status()

                    if self.is_email(username):
                        logging.debug("%s is guest account." % username)

                        root = self.html_root(r1)
                        link = root.find(".//{http://www.w3.org/1999/xhtml}a[@id='zocial-guest']")
                        guest_url = self.split_url(r1.url)[0] + self.split_url(link.get('href'))[1]
                        logging.debug(guest_url)
                        r1 = s.get(guest_url, timeout=10, verify=False, allow_redirects=True)
                        r1.raise_for_status()

                    else:
                        logging.debug("%s is a regular account." % username)
 
                    action,form_data=login_page1(r1.content.decode('utf-8'),username,password)
                    # action, form_data = self.read_form(r1)

                    # form_data['username'] = username
                    # form_data['password'] = password

                    r2 = s.post(url=action, data=form_data, timeout=self.DEFAULT_TIMEOUT_SECONDS, allow_redirects=True)
                    r2.raise_for_status()
                    if login_type=='simple':
                        action, form_data = self.read_form(r2)
                        r3 = s.post(url=action, data=form_data, timeout=self.DEFAULT_TIMEOUT_SECONDS, allow_redirects=True)
                    elif login_type=='2fa':
                        action,form_data=findformeotppage(r2.content.decode('utf-8'))
                        r3 = s.post(url=action, data=form_data, timeout=self.DEFAULT_TIMEOUT_SECONDS, allow_redirects=True)
                        action, form_data = self.read_form(r3)
                        r4 = s.post(url=action, data=form_data, timeout=self.DEFAULT_TIMEOUT_SECONDS, allow_redirects=True)
                        


                    

                    cache = {
                        'secret': b64encode((username + '/' + b64encode(password.encode()).decode()).encode()).decode(),
                        'location': url,
                        'cookies': {c.name: c.value for c in s.cookies}
                    }

                    with open(cache_file, 'w') as f:
                        f.write(json.dumps(cache))

            return cache['cookies']


class RhApiRowCountError(Exception):

    def __init__(self, totalRows, fetchedRows):
        self.totalRows = totalRows
        self.fetchedRows = fetchedRows

    def __str__(self):
        return 'Total rows count (%d) mismatch with fetched rows count (%d)' % (self.totalRows, self.fetchedRows)


class RhApiRowLimitError(Exception):

    def __init__(self, count, rowsLimit):
        self.count = count
        self.rowsLimit = rowsLimit

    def __str__(self):
        return 'Rows count (%d) is larger than rows limit (%d) for a single result' % (self.count, self.rowsLimit)


class RhApiPageSizeError(Exception):

    def __init__(self, count, rowsLimit, pageSize):
        self.count = count
        self.rowsLimit = rowsLimit
        self.pageSize = pageSize

    def __str__(self):
        return 'Page size (%d) is larger than rows limit (%d) for a single result' % (self.pageSize, self.rowsLimit)


class BadColumnNameError(Exception):

    def __init__(self, bad_column, columns_list, table_name):
        self.bad_column = bad_column
        self.columns_list = columns_list
        self.table_name = table_name

    def __str__(self):
        return 'Column name (%s) does not exist in the table (%s). Try these columns: (%s).' \
               % (self.bad_column, self.table_name, json.dumps(self.columns_list))


class RhApi:
    """
    RestHub API object
    """

    def __init__(self, url, debug=False, sso=None):
        """
        Construct API object.
        url: URL to RestHub endpoint, i.e. http://localhost:8080/api
        debug: should debug messages be printed out? Verbose!
        sso: use cookie provider from SSO_COOKIE_PROVIDER string
        """
        if re.match("/$", url) is None:
            url = url + "/"
        self.url = url
        self.debug = debug
        self.dprint("url = ", self.url)

        self.cprov = None
        if sso is not None and re.search("^https", url):
            if sso == 'login':
                self.cprov = lambda url, force_level: (
                CernSSO().login_sign_on(url, force_level=force_level,login_type='simple'), force_level)
            if sso == 'login2':
                self.cprov = lambda url, force_level: (
                CernSSO().login_sign_on(url, force_level=force_level,login_type='2fa'), force_level)
            if sso == 'krb':
                self.cprov = lambda url, force_level: (CernSSO().krb_sign_on(url), 2)

    def _action(self, action, url, headers, data):
        force_level = 0
        while True:

            cookies = None
            if self.cprov is not None:
                cookies, force_level = self.cprov(self.url, force_level)

            with warnings.catch_warnings():
                r = action(url=url, headers=headers, data=data, cookies=cookies, verify=False)

            if r.status_code == 200 and r.url.startswith(SSO_LOGIN_URL):
                if force_level < 2:
                    force_level = force_level + 1
                    continue
                else:
                    if self.cprov is None:
                        raise Exception('Resource is secured by SSO. Please try --sso')
                    else:
                        raise Exception('Error while logging to HTTPS/SSO')

            return r

    def dprint(self, *args):
        """
        Print debug information
        """
        if self.debug:
            print("RhApi:", end='')
            for arg in args:
                print(arg, end='')
            print()

    def get(self, parts, data=None, headers=None, params=None, verbose=False, cols=False, inline_clobs=False,
            method=None):
        """
        General API call (do not use it directly!)
        """

        if type(params) != dict: params = {}
        if verbose: params["_verbose"] = True
        if cols: params["_cols"] = True
        if inline_clobs: params["_inclob"] = True

        #
        # Constructing request path
        #

        callurl = self.url + "/".join(requote_uri(str(p)) for p in parts)
        callurl = callurl + "?" + "&".join(p + "=" + requote_uri(str(params[p])) for p in params.keys())

        sdata = None
        if data != None:
            sdata = json.dumps(data)

        #
        # Do the query and respond
        #

        self.dprint(callurl, "with payload", sdata, "and headers", headers)

        if method is None:
            if data is None:
                method = 'get'
            else:
                method = 'post'
        else:
            method = method.lower()

        action = getattr(requests, method, None)
        if action:
            resp = self._action(action, headers=headers, url=callurl, data=data)
        else:
            raise NameError('Unknown HTTP method: ' + method)

        self.dprint("Response", resp.status_code, " ".join(str(resp.headers.get('content-type')).split("\r\n")))

        if resp.status_code == requests.codes.ok:
            rdata = resp.text
            if re.search("json", resp.headers.get('content-type')):
                try:
                    return json.loads(rdata)
                except TypeError as e:
                    self.dprint(e)
                    return rdata
            else:
                return rdata
        elif resp.status_code < 300:
            return None
        else:
            raise Exception('Response (' + str(resp.status_code) + '): ' + resp.text)

    def lob(self, url, file_name=None):
        """
        Retrieve blob from url.
        url: blob URL from query response
        file_name: (optional) file name to write lob content to
        returns:
            - lob content: if file_name is None
            - True: if file was written
            - None: if no content found
        Exception: if anything nasty happens
        """
        action = getattr(requests, "get", None)
        resp = self._action(action, url=url, headers=None, data=None)

        self.dprint("Response", resp.status_code, " ".join(str(resp.headers.get('content-type')).split("\r\n")))

        if resp.status_code == requests.codes.ok:
            if file_name is not None:
                with open(file_name, "wb") as f:
                    f.write(resp.content)
                return True
            else:
                return resp.content
        elif resp.status_code < 300:
            return None
        else:
            raise Exception('Response (' + str(resp.status_code) + '): ' + resp.text)

    def info(self, verbose=False):
        """
        Get server version information
        """
        return self.get(["info"], verbose=verbose)

    def folders(self, verbose=False):
        """
        Get list of folders
        """
        return list(self.get(["tables"], verbose=verbose).keys())

    def tables(self, folder, verbose=False):
        """
        Get tables for folder or all
        """
        raw = self.get(["tables"], verbose=verbose)
        d = []
        for t in raw[folder].keys():
            d.append(t)
        return d

    def table(self, folder, table, verbose=False):
        """
        Get info for table
        """
        return self.get(["table", folder, table], verbose=verbose)

    def qid(self, query):
        """
        Create query based on [query] and return its ID
        """
        return self.get(["query"], query)

    def query(self, qid, verbose=False):
        """
        Return qid metadata (assuming it exists..)
        """
        return self.get(["query", qid], verbose=verbose)

    def clean(self, qid, verbose=False):
        """
        Remove cache for query (assuming it exists..)
        """
        return self.get(["query", qid, "cache"], verbose=verbose, method='DELETE')

    def count(self, qid, params=None, verbose=False):
        """
        Get number of rows in a query
        """
        return int(self.get(["query", qid, "count"], params=params, verbose=verbose))

    def histo(self, qid, column, bins=None, bounds=None, params=None, verbose=False):
        """
        Get histogram bins for query column.
        """

        path = ["query", qid, "histo", column]

        assert bounds is None or (isinstance(bounds, (list, tuple)) and len(bounds) == 2 and all(
            isinstance(b, (int, float)) for b in bounds) and bounds[0] < bounds[1])
        if bounds: path += [",".join([str(i) for i in bounds])]

        assert bins is None or isinstance(bins, (int))
        if bins: path += [bins]

        return self.get(path, params=params, verbose=verbose)

    def data(self, qid, params=None, form='text/csv', pagesize=None, page=None, verbose=False, cols=False,
             inline_clobs=False):
        """
        Get data rows
        """

        rowsLimit = self.query(qid, verbose=True)["rowsLimit"]
        count = int(self.count(qid))

        ps = ["query", qid]
        if pagesize is None or page is None:
            if count > rowsLimit:
                raise RhApiRowLimitError(count, rowsLimit)
        else:
            if pagesize > rowsLimit:
                raise RhApiPageSizeError(count, rowsLimit, pagesize)
            else:
                ps.extend(["page", pagesize, page]);

        ps.append("data")
        return self.get(ps, None, {"Accept": form}, params, verbose=verbose, cols=cols, inline_clobs=inline_clobs)

    def csv(self, query, params=None, pagesize=None, page=None, verbose=False, inline_clobs=False):
        """
        Get rows in CSV format
        """
        qid = self.qid(query)
        return self.data(qid, params, 'text/csv', pagesize, page, verbose=verbose, inline_clobs=inline_clobs)

    def xml(self, query, params=None, pagesize=None, page=None, verbose=False, inline_clobs=False):
        """
        Get rows in XML format
        """
        qid = self.qid(query)
        return self.data(qid, params, 'text/xml', pagesize, page, verbose=verbose, inline_clobs=inline_clobs)

    def json(self, query, params=None, pagesize=None, page=None, verbose=False, cols=False, inline_clobs=False):
        """
        Get rows in JSON format (array of arrays)
        """
        qid = self.qid(query)
        return self.data(qid, params, 'application/json', pagesize, page, verbose=verbose, cols=cols,
                         inline_clobs=inline_clobs)

    def json_all(self, query, params=None, verbose=False, cols=False, inline_clobs=False):
        """
        Get all rows in JSON format (array of arrays)
        """

        rows = []

        qid = self.qid(query)
        rowsLimit = self.query(qid, verbose=True)["rowsLimit"]
        count = int(self.count(qid, params))
        pages = int(count / rowsLimit) + 1

        for page in range(1, (pages + 1)):
            data = self.data(qid, params, form="application/json", page=page, pagesize=rowsLimit, verbose=verbose,
                             cols=cols, inline_clobs=inline_clobs)
            rows.extend(data["data"])

        if count != len(rows):
            raise RhApiRowCountError(count, len(rows))

        return rows

    def json2(self, query, params=None, pagesize=None, page=None, verbose=False, cols=False, inline_clobs=False):
        """
        Get rows in JSON2 format (array or objects)
        """
        qid = self.qid(query)
        return self.data(qid, params, 'application/json2', pagesize, page, verbose=verbose, cols=cols,
                         inline_clobs=inline_clobs)


import argparse
import pprint


SSO_LOGIN_URL = "https://auth.cern.ch/"

class DBReader:

    DEFAULT_FORMAT = "csv"
    FORMATS = ["csv", "json"]
    GRADES = ["green", "yellow", "red"]
    RHAPI_URL = "https://cmsdca.cern.ch/trk_rhapi"

    # Field names that should be ignored
    FIELD_TRANSLATIONS = {
        'wafer': {
            'LABEL': 'partNameLabel'
        },
        'chip': {
            'TEST_START': None,
            'CHIP_COL': None,
            'CHIP_ROW': None,
            'LABEL': 'PART_NAME_LABEL',
        }
    }

    def __init__(self):

        self.pp = pprint.PrettyPrinter(indent=4)
        parser = argparse.ArgumentParser(description="Reads CROC WLT data from the DCA database")
        parser.add_argument("-2", "--2fa", dest='twofa', action="store_true",
                            help="Use 2-factor authentication")
        parser.add_argument("-w", "--wafer", metavar="ID", type=str,
                            help="Wafer to be read from DB")
        parser.add_argument("-c", "--chips", metavar="ID", type=str, nargs='+',
                            help="Specific chips to be read from DB. Default: all chips")
        parser.add_argument("-f", "--fields", metavar="FIELD", type=str, nargs='*',
                            help="Fields to be read from DB")
        parser.add_argument("-d", "--dev", action="store_true",
                            help="Query development DB instead of production DB")
        parser.add_argument("-p", "--proto", action="store_true",
                            help="Query CROC Proto parts instead of CROC parts")
        parser.add_argument("-v", "--verbose", action="store_true",
                            help="Verbose output")
        parser.add_argument("-i", "--input", metavar="FOLDER", type=str,
                            help="Folder with input data for DB read/write validation")
        parser.add_argument("--format", metavar="FORMAT", nargs='?', choices=self.FORMATS, default=self.DEFAULT_FORMAT,
                            help="data output format [%s]. Default: %s" % (", ".join(self.FORMATS), self.DEFAULT_FORMAT))
        parser.add_argument("-o", "--out", metavar="FOLDER", type=str, default='./',
                            help="Folder where to store the output. Default: ./")
        parser.add_argument("-s", "--sqlite", metavar="FILE", type=str,
                            help="SQLite file where to store the wafer IDs")
        parser.add_argument("-t", "--test", action="store_true",
                            help="Test DB connection listing the available fields")

        # parser.add_argument("-b", "--inclob", action="store_true",
        #                          help="inline clobs directly into the output")

        self.opts = parser.parse_args()
        self.setup_table_names()
        # Setting the authentication type
        self.sso = 'login'
        if self.opts.twofa:
            self.sso = 'login2'
        # Forcing JSON output format for validation of DB input
        if self.opts.input is not None:
            self.opts.format = 'json'
        # Defining the chip type
        self.chip_type = 'CROCv1' if self.opts.proto else 'CROCv2'
        # Output data storage before writing
        self.wafer_info = None
        self.chip_info = {}
        self.chip_data = {}
        # Input data storage
        self.input_wafer = None
        self.input_chip = {}
        # Number of differences wrt the input data
        self.diffs = 0

    
    def setup_table_names(self):
        """Defines relevant table names based on the development and prototype options"""
        # Defining the DB name
        if not self.opts.dev:
            # Production DB
            db = 'trker_cmsr'
            table = {
                # Proto wafers: CROCv1
                'wafer_part': 'p10640',
                'wafer':      'c13400',
                'chip':       'c13420',
                'chip_data':  'c13440'
            } if self.opts.proto else {
                # Wafers: CROCv2
                'wafer_part': 'p10680',
                'wafer':      'c18200',
                'chip':       'c18220',
                'chip_data':  'c18240'
            }
        else:
            # Development DB
            db = 'trker_int2r'
            table = {
                # Proto wafers: CROCv1
                'wafer_part': 'p17020',
                'wafer':      'c20620',
                'chip':       'c20640',
                'chip_data':  'c20660'
            } if self.opts.proto else {
                # Wafers: CROCv2
                'wafer_part': 'p17060',
                'wafer':      'c25020',
                'chip':       'c25040',
                'chip_data':  'c25060'
            }
        # Assigning the full names in format: <DB>.<TABLE>
        self.TBL_WAFER_PART = f'{db}.{table["wafer_part"]}'
        self.TBL_WAFER = f'{db}.{table["wafer"]}'
        self.TBL_CHIP = f'{db}.{table["chip"]}'
        self.TBL_CHIP_DATA = f'{db}.{table["chip_data"]}'


    def pprint(self, data):
        self.pp.pprint(data)


    def to_camel_case(self, name):
        """Converts a column name from SNAKE_CASE to camelCase"""
        first, *rest = name.split('_')
        return first.lower() + ''.join(map(str.title, rest))
    

    def dca_link(self, part_id=None):
        """Generates a link to the DCA page of the wafer or chip"""
        try:
            part_id = int(part_id)
            if self.opts.dev:
                return '[DEV]'
            return f'https://cmsdca.cern.ch/trk_cmsr/construct/part/view/{part_id}'
        except ValueError:
            return '[DCA LINK]'

    
    def test_dca(self):
        """Test connection with DCA DB"""
        # Testing wafer-info table
        res = self.api.csv(f'select w.* from {self.TBL_WAFER} w fetch first 0 rows only')
        fields = res.split('\n')[0].split(',')
        if self.opts.test:
            print('Wafer-info fields:')
            for f in fields:
                print('  ', f)
        # Testing chip-info table
        res = self.api.csv(f'select c.* from {self.TBL_CHIP} c fetch first 0 rows only')
        fields = res.split('\n')[0].split(',')
        if self.opts.test:
            print('\nChip-info fields:')
            for f in fields:
                print('  ', f)
        # Testing chip-data table
        res = self.api.csv(f'select distinct c.CROC_DATA_ID from {self.TBL_CHIP_DATA} c')
        fields = res.split('\n')[1:]
        if self.opts.test:
            print('\nChip-data measurements:')
            for f in fields:
                print('  ', f)


    def list_wafers(self, pattern, use_parts=False):
        """List wafers registered in DB"""
        msg_add = '' if use_parts else ' with WLT resutls'
        # Targeting table with WLT results
        table = self.TBL_WAFER
        columns = ['PART_NAME_LABEL', 'PROBE_SITE', 'CUT_VERSION']
        if use_parts:
            # Targeting table with parts instead
            table = self.TBL_WAFER_PART
            columns = ['NAME_LABEL', 'BATCH_NUMBER', 'SERIAL_NUMBER', 'ID']
            pattern = pattern[1:]
        print(f'Listing all {self.chip_type} wafers{msg_add} matching the pattern: \'{pattern}\'')
        pattern = pattern.replace('*', '%')
        q_columns = ','.join([f'w.{col}' for col in columns])
        query = f"SELECT DISTINCT {q_columns} from {table} w WHERE w.{columns[0]} LIKE '{pattern}'"
        if self.opts.verbose:
            print('Query to list the wafers:')
            print('> ' + query)
        # Executing the query
        res = self.api.csv(query, verbose=self.opts.verbose)
        reader = csv.reader(StringIO(res))
        # Checking where to put the results
        out_path = self.opts.sqlite
        if not out_path:
            # Printing results to the screen
            for row in reader:
                line = ' '.join(f'{str(v):16s}' for v in row)
                if use_parts:
                    line += f'  {self.dca_link(row[-1])}'
                print(line)
        else:
            # Writing wafer parts to the DB
            append = False
            if out_path.endswith('+'):
                append = True
                out_path = out_path[:-1]
            if os.path.isfile(out_path) and not append:
                print(f'Deleting existing SQLite file: {out_path}')
                os.remove(out_path)
            self.write_wafers_to_sqlite(reader, out_path, append)


    def write_wafers_to_sqlite(self, csv, out_path, append=True):
        """Stores list of wafers to an SQLite file"""
        con = sqlite3.connect(out_path)
        cur = con.cursor()
        if not append:
            cur.execute('CREATE TABLE wafers(batch_id TEXT, batch_num INTEGER, wafer_id TEXT, waferprobing_id INTEGER, chip_type TEXT)')
        n_rows = 0
        for i, line in enumerate(csv):
            # Skipping the header
            if i == 0:
                continue
            label, batch_id, serial_n, _ = line
            # Skipping wafers without serial number
            if not serial_n:
                continue
            batch_n, wafer_id = label.split('_')
            query = f"INSERT INTO wafers VALUES ('{batch_id}','{batch_n}','{wafer_id}','{serial_n}', '{self.chip_type}')"
            cur.execute(query)
            n_rows += 1
        con.commit()
        print(f'+ Inserted {n_rows} wafers in file: {out_path}')
        


    def run(self):
        """Executes all the DB-processing functionality"""

        opts = self.opts

        # Checking that the output folder exists
        if opts.out is not None and not os.path.isdir(opts.out):
            raise NotADirectoryError(f"ERROR! Output folder doesn't exist: {opts.out}")

        # Creating the API instance to communicate with the DB
        self.api = RhApi(self.RHAPI_URL, debug=opts.verbose, sso=self.sso)
        api_method = None
        if opts.format == 'json':
            api_method = self.api.json2
        elif opts.format == 'csv':
            api_method = self.api.csv

        # Testing connection if needed
        if opts.test or opts.fields:
            try:
                self.test_dca()
            except Exception as e:
                print('ERROR: Reading from DB failed')
                raise(e)
            # Stopping if it was just a connection test
            if opts.test:
                return False
        
        # Listing available wafers in DB if no wafer name provided
        if any(v in opts.wafer for v in '+*'):
            use_parts = True if opts.wafer.startswith('+') else False
            self.list_wafers(opts.wafer, use_parts)
            exit(0)


        # Reading data about the wafer
        query = f"select w.* from {self.TBL_WAFER} w where w.PART_NAME_LABEL like '%{opts.wafer}' "\
                f"and w.CONDITION_DATA_SET_ID = "\
                f"(select max(x.CONDITION_DATA_SET_ID) as set_id from {self.TBL_WAFER} x where x.PART_NAME_LABEL like '%{opts.wafer}')"
        if self.opts.verbose:
            print('Query to identify the wafer:')
            print('> ' + query)
        res = self.api.json2(query, verbose=opts.verbose)
        if not res['data']:
            msg = f'No WLT data found for wafer: {opts.wafer}'
            if not self.opts.proto:
                msg += '\nIf it is a CROC Proto Wafer, add option: -p'
            raise RuntimeError(msg)
        self.wafer_info = res['data'][0]

        # Reading input data for comparison to DB
        if opts.input:
            self.read_input_data()

        chip_part_ids = []
        chip_labels = []
        # # Finding the last dataset ID
        # # FIXME: Can't do this as long as every chip gets its own CONDITION_DATA_SET_ID.
        # # DB should share 1 value across all chips of the wafer in 1 data submission
        # # query = f"select max(c.CONDITION_DATA_SET_ID) as set_id from {self.TBL_CHIP} c where c.PART_NAME_LABEL like '%{opts.wafer}_%'"
        # query = f"select c.* from {self.TBL_CHIP} c where c.PART_NAME_LABEL like '%{opts.wafer}_%'"
        # res = self.api.csv(query)
        # dataset_id = res['data'][0][0]

        # Building the query to select chips
        select = f"select distinct c.PART_NAME_LABEL, c.PART_ID from {self.TBL_CHIP} c "\
                 f"where c.PART_NAME_LABEL like '%{opts.wafer}_%'"
                 # f"where c.PART_NAME_LABEL like '%{opts.wafer}_%' and c.CONDITION_DATA_SET_ID = {dataset_id}"
        conditions = []
        # Adding condition for each chip ID or GRADE
        if opts.chips:
            for chip_id in opts.chips:
                if chip_id in self.GRADES:
                    raise RuntimeError(f'ERROR! Selecting chips by grade is not yet supported...')
                    # conditions.append(f"c.GRADE='{chip_id}'")
                else:
                    conditions.append(f"c.PART_NAME_LABEL like '%{opts.wafer}_{chip_id}'")
        # Collecting chip labels and part IDs according to each condition
        condition = ' or '.join(conditions)
        query = f'{select} and ({condition})' if conditions else select
        if self.opts.verbose:
            print('Query to get chip labels:')
            print('> ' + query)
        res = self.api.json(query, verbose=opts.verbose)
        for data in res['data']:
            if data[0] in chip_labels:
                continue
            chip_labels.append(data[0])
            chip_part_ids.append(data[1])

        # Validating the input wafer info
        if opts.input:
            self.diffs += self.validate_input_wafer(chip_labels)

        # Reading data for every chip
        wafer_part_id = self.wafer_info['partId']
        print(f'Reading data from {len(chip_labels)} chip(s) of wafer: {self.wafer_info["partNameLabel"]}  > {self.dca_link(wafer_part_id)}')
        # Not retrieving actual data if no fields were requested
        if opts.fields == ['-']:
            return
        for iC, chip_label in enumerate(chip_labels):
            chip_part_id = chip_part_ids[iC]
            print(f'Reading data for chip {iC+1}/{len(chip_labels)}: {chip_label}  > {self.dca_link(chip_part_id)}')
            fields = 'c.*'
            if opts.fields:
                fields = ['PART_NAME_LABEL'] + opts.fields
                fields = ', '.join([f'c.{f}' for f in fields])
            query = f"select {fields} from {self.TBL_CHIP} c "\
                    f"where c.PART_ID = '{chip_part_id}' and c.CONDITION_DATA_SET_ID = "\
                    f"(select max(d.CONDITION_DATA_SET_ID) as set_id from {self.TBL_CHIP} d where d.PART_ID = '{chip_part_id}')"
            if self.opts.verbose:
                print('Query to retrieve the chip info:')
                print('> ' + query)
            res = api_method(query, verbose=opts.verbose)
            # Storing data to the dictionary
            data = res
            if opts.format == 'json':
                data = res['data'][0]
            self.chip_info[chip_label] = data
            # Reading also measurement points from the latest uploaded dataset
            query = f"select c.PART_NAME_LABEL, c.CROC_DATA_ID, c.X, c.Y from {self.TBL_CHIP_DATA} c "\
                    f"where c.PART_ID = '{chip_part_id}' and c.CONDITION_DATA_SET_ID = "\
                    f"(select max(d.CONDITION_DATA_SET_ID) as set_id from {self.TBL_CHIP_DATA} d where d.PART_ID = '{chip_part_id}')"
            if self.opts.verbose:
                print('Query to retrieve the chip data:')
                print('> ' + query)
            res = api_method(query, verbose=opts.verbose)
            # Storing data to the dictionary
            data = res
            # Refactoring JSON data into multipoint dictionaries
            if opts.format == 'json':
                data = {}
                for el in res['data']:
                    x, y, name, chip = [el[n] for n in ['x', 'y', 'crocDataId', 'partNameLabel']]
                    if name not in data:
                        data[name] = {}
                    data[name][x] = y
            self.chip_data[chip_label] = data
            # Validating with the input data if specified
            if opts.input:
                self.diffs += self.validate_input_chip(chip_label)
        # Not writing out DB data in validation mode
        if opts.input:
            if self.diffs > 0:
                raise RuntimeWarning(f'Found unexpected differences: {self.diffs}')
            else:
                print('SUCCESS! Input data is consistent with data in DB')

            return False
        return True


    def read_input_data(self):
        """Reads input data from disk"""
        dir_in = self.opts.input
        print(f'Reading data to be validated from: {dir_in}')
        for file_path in glob.glob(f'{dir_in}/*.json'):
            file_name = os.path.basename(file_path)
            data = None
            with open(file_path) as f_in:
                data = json.load(f_in)
            if file_name.startswith('wafer'):
                self.input_wafer = data
            elif file_name.startswith('chip'):
                chip_label = data['LABEL']
                self.input_chip[chip_label] = data


    def validate_input_wafer(self, db_chips):
        """Compares the wafer info read from DB with the input data"""
        print('Validating wafer')
        keys = set(self.wafer_info.keys())
        diffs = 0
        # Keys known to have non-identical values in DB
        known_diffs = ['TEST_START']
        for k_in, v_in in self.input_wafer.items():
            # Converting the key name to camel-case used by DB JSON output
            k_out = self.to_camel_case(k_in)
            if k_out not in keys:
                continue
            v_out = self.wafer_info[k_out]
            # Checking if input and output values are consistent
            if not self.compare_values(k_in, v_in, v_out):
                if k_in not in known_diffs:
                    diffs += 1
        # Checking if all the input chip labels are present in DB
        for chip_label in self.input_chip.keys():
            chip_id = chip_label.split('_')[-1]
            if self.opts.chips and chip_id not in self.opts.chips:
                continue
            if chip_label not in db_chips:
                print(f' WARNING: Chip `{chip_label}` not in DB data')
        return diffs


    def validate_input_chip(self, chip_label):
        """Compares the chip data read from DB with the input data"""
        diffs = 0
        for k_in, v_in in self.input_chip[chip_label].items():
            # Finding the right output data for the given key
            k_out = k_in
            # Applying field-name conversion for special cases
            if k_out in self.FIELD_TRANSLATIONS['chip']:
                k_out = self.FIELD_TRANSLATIONS['chip'][k_out]
            # Skipping the field if it should be ignored
            if k_out is None:
                continue
            # Select the relevant table data for chip
            if k_out.endswith(' vector'):
                k_out = k_out.split(' vector')[0]
                data_out = self.chip_data[chip_label]
            else:
                # Chip-info fields are column names -> returned in camelCase
                k_out = self.to_camel_case(k_out)
                data_out = self.chip_info[chip_label]
            # No comparison if the key is not in DB output
            if k_out not in data_out:
                if v_in is None:
                    continue
                print(f' not in DB: "{k_in}"')
                continue

            v_out = data_out[k_out]
            if not self.compare_values(k_in, v_out, v_in):
                diffs += 1
        return diffs


    def compare_values(self, key, v1, v2, tolerance=1e-6):
        """Checks whether two values are consistent"""
        type1, type2 = type(v1), type(v2)
        if type1 != type2:
            if int in [type1, type2]:
                v1 = int(v1)
                v2 = int(v2)
                type1 = int
                type2 = int
            else:
                print(f" {key+':':<25} `{type1}`  ->  `{type2}`  [wrong types]")
                return False
        if type1 is float:
            if abs(v1 - v2) > tolerance*max(abs(v1), abs(v2)):
                print(f" {key+':':<25} `{v1:.7e}`  ->  `{v2:.7e}`")
                return False
        elif type1 is dict:
            same = True
            for k1, d1 in v1.items():
                for k2, d2 in v2.items():
                    same_key = False
                    for typ in [int, float, str]:
                        try:
                            if typ(k1) == typ(k2):
                                same_key = True
                                break
                        except ValueError:
                            continue
                    if not same_key:
                        continue
                    if not self.compare_values(k1, d1, d2):
                        same = False
                        break
            if not same:
                print(f" {key}:")
                print('  > ', v1)
                print('  - - - - -')
                print('  < ', v2)
                return False
        elif v1 != v2:
            print(f" {key+':':<25} `{v1}`  ->  `{v2}`")
            return False

        return True


    def write_data(self):
        """Writes collected wafer/chip data to separate files"""
        opts = self.opts
        print('- - - - -')
        # Writing wafer info
        out_path = os.path.join(opts.out, f'wafer_{opts.wafer}.json')
        with open(out_path, 'w') as f_out:
            f_out.write(json.dumps(self.wafer_info, indent=4))
            print(f'Wrote data to: {out_path}')
        # Writing chip info and data
        for name, data in {'chip_info': self.chip_info, 'chip_data': self.chip_data}.items():
            out_path = os.path.join(opts.out, f'{name}_{opts.wafer}.{opts.format}')
            with open(out_path, 'w') as f_out:
                if opts.format == 'json':
                    f_out.write(json.dumps(data, indent=4))
                elif opts.format == 'csv':
                    for iC, csv_data in enumerate(data.values()):
                        start_row = 0 if iC == 0 else 1
                        f_out.write('\n'.join(csv_data.split('\n')[start_row:]))
            print(f'Wrote data to: {out_path}')


    def run_safe(self):
        """Executes the DB access handling potential exceptions"""

        try:
            return self.run()

        except RhApiRowLimitError as e:
            print("ERROR: %s\nDetails: %s, consider --all option" % (type(e).__name__, e))

        except requests.exceptions.RequestException as e:
            reason = e.reason if hasattr(e, 'reason') else '%s' % e
            print("ERROR: %s\nDetails: %s" % (reason, e))

        except Exception as e:
            print("ERROR: %s\nDetails: %s" % (type(e).__name__, e))
            import traceback
            traceback.print_exc()


if __name__ == '__main__':

    reader = DBReader()
    if reader.run_safe():
        reader.write_data()
