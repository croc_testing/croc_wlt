////////////////////////////////////////////////////////////////////////////////
/// Sketch for controlling the Wafer Probing Auxiliary Card (WPAC).
///
/// @file wpac.ino
/// @author Michael Grippo
////////////////////////////////////////////////////////////////////////////////

#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>

//serial communication
#define SERIAL Serial1
#define BAUD_RATE 230400  //baud rate for the serial communication with the computer
#define WRITE_TERM '\n'   //termination character used when writing on serial bus
#define READ_TERM '\n'    //termination character used when reading from serial bus
#define TIMEOUT_MILLIS 50 //serial communication timeout

//i2c
#define WIRE Wire1
#define INPUT_REG 0
#define OUTPUT_REG 1
#define POLARITY_REG 2
#define CONFIGURATION_REG 3

//probe card types
#define CROC_PROBE_CARD_V1P0 1 //RD53B-CMS probe card v1.0
#define CROC_PROBE_CARD_V1P1 2 //RD53B-CMS probe card v1.1
#define CROC_PROBE_CARD_V2P0 3 //RD53B-CMS probe card v2.0

//////////////////////////
// PROBE CARD SELECTION //
//////////////////////////
#define PROBE_CARD CROC_PROBE_CARD_V2P0

//probe card configuration
#define POWER_DELAY_MILLIS 100 //time to wait when changing the power of the probe card
#define ENVCC_PIN 5            //pin used to send the ENVCC signal

//LEDs
#define LE1_PIN 8
#define LE2_PIN 9
#define LE3_PIN 10
#define LE4_PIN 11

//temperature sensors
#define N_TEMP_SENSORS 2
//#define DS1820_1_PIN 31
//#define DS1820_2_PIN 30
#define DS1820_3_PIN 32
#define DS1820_4_PIN 33

//TMUX1101
#define MUXSEL_PIN 3

//CMOS lines
#define DIG0_PIN 22  //SCAN_ARDU
#define DIG1_PIN 23  //LP_EN_AC_ARDU
#define DIG2_PIN 24  //NC on CROC probe cards v1.0 and v1.1
#define SPARE_PIN 25 //NC on CROC probe cards v1.0 and v1.1

//dac
#define DAC_RESOLUTION 12 //resolution for the DAC (maximum: 12 bit)
#define DAC_MAX 1023      //safety limit because the DAC can go up to 2,75 V
#define DAC_PIN DAC0

//adc
#define ADC_RESOLUTION 12
#define ADC_PIN A0

//edge sensors
#define ES1_PIN 52
#define ES2_PIN 53

//power card pins
#define PC0_PIN 50
#define PC1_PIN 51
#define PC2_PIN 30

//patch board pins
#define SEL_PIN 31

//DEBUG MODE: if DEBUG is defined, additional data is written to the serial bus
//#define DEBUG

//other
#define SLEEP_TIME_MILLIS 0 //time to sleep at the end of the loop function (ms)
#define BUFFER_IN_SIZE 20   //maximum input command size for command messages
#define BUFFER_OUT_SIZE 60  //maximum output message size for WPAC answers
#define N_MAX_FIELDS 3      //maximum number of subfields in command messages

//global variables
uint8_t gReadByte = 0;                  //stores the byte read from PCA9554 register
char gInputData[BUFFER_IN_SIZE] = {};   //char array to hold the input message
char gOutputData[BUFFER_OUT_SIZE] = {}; //char array, stores the output message
char * gInputToken = NULL;              //char pointer for splitting input messages
char * gSplitInput[N_MAX_FIELDS] = {};  //pointer to char array, stores split input

//available I2C addressess
#if PROBE_CARD == CROC_PROBE_CARD_V1P0
const size_t N_ADDRS = 3;
const uint8_t I2C_ADDRS[N_ADDRS] = {0x20, 0x21, 0x22};
#elif PROBE_CARD == CROC_PROBE_CARD_V1P1
const size_t N_ADDRS = 4;
const uint8_t I2C_ADDRS[N_ADDRS] = {0x20, 0x21, 0x22, 0x24};
#elif PROBE_CARD == CROC_PROBE_CARD_V2P0
const size_t N_ADDRS = 4;
const uint8_t I2C_ADDRS[N_ADDRS] = {0x20, 0x21, 0x22, 0x24};
#endif

//initialization of temperature sensors

//struct for handling temperature sensors
struct TempSensor {
	//attributes
	uint8_t id;
	uint8_t pin;
	OneWire bus;
	DallasTemperature interface;

	//constructor
	TempSensor(uint8_t pId, uint8_t pPin) {
		id = pId;
		pin = pPin;
		bus = OneWire(pin);
		interface = DallasTemperature(&bus);
	}

	//method to initialize the sensor
	void init() {
		interface.begin();
	}

	//method to read the temperature
	bool readTemperature(float_t & output) {
		interface.requestTemperatures();
		output = interface.getTempCByIndex(0);
		return (output != DEVICE_DISCONNECTED_C);
	}
};

//array of temperature sensors
TempSensor tempSensors[N_TEMP_SENSORS] = {
	//TempSensor(1, DS1820_1_PIN),
	//TempSensor(2, DS1820_2_PIN),
	TempSensor(3, DS1820_3_PIN),
	TempSensor(4, DS1820_4_PIN),
};

// TODO: put every WPAC command in a function called wpacCommand<COMMAND>.
// Examples: wpacCommandRS, wpacCommandSC, wpacCommandRA, wpacCommandIW

// TODO: monitor the I2C lines or something else in order to check when a probe
// card gets connected to the WPAC.

// TODO: first write the output logic level and then set the pin to output (if
// necessary)

// TODO: write OK and ERR strings only once in the firmware to save space and
// improve code quality

/////////////////////////////////////////////////////
// Forward declaration for initialization function //
/////////////////////////////////////////////////////
uint8_t initialize (bool=false);

////////////////////////////////////////////////////////////////////////////////
/// SETUP: initialize the Arduino Due board and the probe card.
////////////////////////////////////////////////////////////////////////////////
void setup() {
	//initializing serial communication
	SERIAL.begin(BAUD_RATE);
	SERIAL.setTimeout(TIMEOUT_MILLIS);

	//set ENVCC pin to output
	pinMode(ENVCC_PIN, OUTPUT);

	//settings edge sensors as **inputs** explicitly
	pinMode(ES1_PIN, INPUT);
	pinMode(ES2_PIN, INPUT);

	//setting the CMOS lines as **outputs**. The default is LOW
	pinMode(DIG0_PIN, OUTPUT);
	pinMode(DIG1_PIN, OUTPUT);

	//setting the power card pins as **outputs**. The default is LOW
	pinMode(PC0_PIN, OUTPUT);
	pinMode(PC1_PIN, OUTPUT);
	pinMode(PC2_PIN, OUTPUT);

	//setting the SEL GPIO for the Patch Board as a LOW output
	digitalWrite(SEL_PIN, HIGH);
	pinMode(SEL_PIN, OUTPUT);

	//setting LED pins to output
	pinMode(LE1_PIN, OUTPUT);
	pinMode(LE2_PIN, OUTPUT);
	pinMode(LE3_PIN, OUTPUT);
	pinMode(LE4_PIN, OUTPUT);

	//initializing DAC and ADC
	pinMode(DAC_PIN, OUTPUT);
	pinMode(ADC_PIN, INPUT);
	analogWriteResolution(DAC_RESOLUTION);
	analogReadResolution(ADC_RESOLUTION);

	//setting MUXSEL pin as output and writing a **low** logic value
	pinMode(MUXSEL_PIN, OUTPUT);

	//power cycle the probe card
	digitalWrite(ENVCC_PIN, LOW);
	delay(POWER_DELAY_MILLIS);
	digitalWrite(ENVCC_PIN, HIGH);
	delay(POWER_DELAY_MILLIS);

	//initializing I2C communication
	WIRE.begin();

	//initializing temperature sensor
	for (size_t i = 0; i < N_TEMP_SENSORS; i++) {
		tempSensors[i].init();
	}

	//////////////////////////////
	// Initialization procedure //
	//////////////////////////////
	uint8_t errorCode = initialize(true); //true <-> reconfigureI2C

	#ifdef DEBUG
	//checking the error code for the initialization procedure
	if (errorCode == 0) {
		printLine("Initialization completed successfully");
	} else {
		printLine("Initialization failed!");
	}

	//logging the status of the I2C expanders
	for (uint8_t j = 0; j < N_ADDRS; j++) {
		for (uint8_t i = 0; i < 4; i++) {
			readRegister(I2C_ADDRS[j], i, gReadByte);
			sprintf(
				gOutputData,
				"Address %i, register %i, value: %i",
				I2C_ADDRS[j],
				i,
				gReadByte
			);
			printLine(gOutputData);
		}
	}

	//logging the end of the setup procedure
	printLine("Setup completed");
	#endif
}

////////////////////////////////////////////////////////////////////////////////
/// LOOP: checks whether there is data on the serial bus and, if present, parses
/// it and sleeps for SLEEP_TIME_MILLIS milliseconds.
////////////////////////////////////////////////////////////////////////////////
void loop() {
	//waiting for a command
	if (SERIAL.available() > 0) {
		#ifdef DEBUG
		printLine("Command received");
		#endif

		//reading command
		readInput();

		#ifdef DEBUG
		printLine(gInputData);
		#endif

		//input parsing
		parseInput();
	}
	//sleep
	delay(SLEEP_TIME_MILLIS);
}

////////////////////////////////////////////////////////////////////////////////
/// Set the Wafer Probing Auxiliary Card and the probe card to the default
/// configuration.
///
/// @param doReconfigureI2C boolean to select whether to reconfigure completely
/// the I2C expanders or not. The default is false.
///
/// @return An error code: 0 if successful; 1 if an error occurred.
////////////////////////////////////////////////////////////////////////////////
uint8_t initialize(bool doReconfigureI2C) {

	//TO DO: write default configuration for probe cards somewhere instead of
	//copying the configuration from one function to another

	////////////////////////
	// WPAC configuration //
	////////////////////////

	//turning off the LED to signal a reconfiguration procedure
	digitalWrite(LE1_PIN, LOW);

	//resetting CMOS lines
	digitalWrite(DIG0_PIN, LOW);
	digitalWrite(DIG1_PIN, LOW);

	//making sure that the power card configuration bits are all 0
	digitalWrite(PC0_PIN, LOW);
	digitalWrite(PC1_PIN, LOW);
	digitalWrite(PC2_PIN, LOW);

	//resetting the configuration for the patch board
	digitalWrite(SEL_PIN, HIGH);

	//resetting MUX_SEL
	digitalWrite(MUXSEL_PIN, LOW);

	//checking that the probe card is on
	digitalWrite(ENVCC_PIN, HIGH);
	delay(POWER_DELAY_MILLIS);

	//////////////////////////////
	// Probe card configuration //
	//////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	// CROC PROBE CARD v1.0
	//
	// reg. name,       addr,   off, size,  def
	// 0x20
	// EN_R_IREF,       0x20,    0,    1,    1
	// EN_R_IMUX,       0x20,    1,    1,    1
	// MUX_SEL,         0x20,    2,    5,    2 (0b00010) (S3 is VDDA)
	// EN_REXT,         0x20,    7,    1,    1 (sets external R for shunt slope)
	// global value: 0b10001011
	// 
	// 0x21
	// CHIP_ID,         0x21,    0,    4,    0
	// BYPASS,          0x21,    4,    1,    0 (it is active high)
	// TEST,            0x21,    5,    1,    0 (select alternative pinout for testing, active high)
	// EN_VDD_SHUNT,    0x21,    6,    1,    0
	// EN_VDD_EFUSE,    0x21,    7,    1,    0
	// global value: 0b00000000
	// 
	// 0x22
	// IREF_TRIM,       0x22,    0,    4,    7 (0b0111)
	// PLL_VCTRL_RST_B, 0x22,    4,    1,    1 (active LOW)
	// NC,              0x22,    5,    1,    0 (no effect)
	// ES               0x22,    6,    2,    0 (INPUT) (no effect)
	// global value: 0b00010111
	// 
	// Notes:
	// - it's probably better to put a ground on MUX_SEL by default (VOFS_LP?)
	// - pin 6 and 7 of 0x22 are inputs -> 2^7+2^6=192=0xC0 for the config reg
	// - should set CHIP_ID = 15 as the default value?
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	// CROC PROBE CARD v1.1
	//
	// reg. name,       addr,   off, size,  def
	// 0x20
	// EN_R_IREF,       0x20,    0,    1,    1
	// EN_R_IMUX,       0x20,    1,    1,    1
	// MUX_SEL,         0x20,    2,    5,   31 (0b11111) (S32 is not connected)
	// EN_REXT,         0x20,    7,    1,    1 (sets external R for shunt slope)
	// global value: 0b11111111
	//
	// 0x21
	// CHIP_ID,         0x21,    0,    4,    0
	// BYPASS,          0x21,    4,    1,    0 (it is active high)
	// TEST,            0x21,    5,    1,    0 (select alternative pinout for testing, active high)
	// EN_VDD_SHUNT,    0x21,    6,    1,    0
	// EN_VDD_EFUSE,    0x21,    7,    1,    0
	// global value: 0b00000000
	//
	// 0x22
	// IREF_TRIM,       0x22,    0,    4,    7 (0b0111)
	// PLL_VCTRL_RST_B, 0x22,    4,    1,    1 (active LOW)
	// SCAN,            0x22,    5,    1,    0 (SCAN_ARDU)
	// ES               0x22,    6,    2,    0 (INPUT) (no effect)
	// global value: 0b00010111
	//
	// 0x24
	// EN_MUX_ARDU,     0x24,    0,    1,    0 (use the Keithley instead of the Arduino)
	// EN_DAC1/2,       0x24,    1,    1,    0 (do not enable the 1/2 voltage divider)
	// EN_DAC1/4        0x24,    2,    1,    0 (do not enable the 1/4 voltage divider)
	// global value: 0b00000000
	//
	// Notes:
	// - pin 6 and 7 of 0x22 are inputs -> 2^7+2^6=192=0xC0 for the config reg
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	// CROC PROBE CARD v2.0
	//
	// reg. name,       addr,   off, size,  def
	// 0x20
	// EN_R_IREF,       0x20,    0,    1,    1
	// EN_R_IMUX,       0x20,    1,    1,    1
	// MUX_SEL,         0x20,    2,    5,   31 (0b11111) (S32 is not connected)
	// EN_REXT,         0x20,    7,    1,    0 (sets external R for shunt slope)
	// global value: 0b01111111
	//
	// 0x21
	// CHIP_ID,         0x21,    0,    4,    0
	// BYPASS,          0x21,    4,    1,    0 (it is active high)
	// TEST,            0x21,    5,    1,    0 (select alternative pinout for testing, active high)
	// EN_VDD_SHUNT,    0x21,    6,    1,    0
	// EN_VDD_EFUSE,    0x21,    7,    1,    0
	// global value: 0b00000000
	//
	// 0x22
	// IREF_TRIM,       0x22,    0,    4,    7 (0b0111)
	// PLL_VCTRL_RST_B, 0x22,    4,    1,    1 (active LOW)
	// SCAN,            0x22,    5,    1,    0 (SCAN_ARDU)
	// ES               0x22,    6,    2,    0 (INPUT) (no effect)
	// global value: 0b00010111
	//
	// 0x24
	// EN_MUX_ARDU,     0x24,    0,    1,    0 (use the Keithley instead of the Arduino)
	// EN_DAC1/2,       0x24,    1,    1,    0 (do not enable the 1/2 voltage divider)
	// EN_DAC1/4        0x24,    2,    1,    0 (do not enable the 1/4 voltage divider)
	// EN_VDDD_CAP      0x24,    3,    1,    0 (do not enable the VDDD capacitor by default)
	// global value: 0b00000000
	//
	// Notes:
	// - pin 6 and 7 of 0x22 are inputs -> 2^7+2^6=192=0xC0 for the config reg
	////////////////////////////////////////////////////////////////////////////

	//looping on I2C expanders to check if they can all be found on the I2C bus
	uint8_t nFound = 0;
	uint8_t addr = 0;
	for (size_t i = 0; i < N_ADDRS; i++) {
		addr = I2C_ADDRS[i];
		WIRE.beginTransmission(addr);
		uint8_t error = WIRE.endTransmission();
		if (error == 0) {
			#ifdef DEBUG
			sprintf(gOutputData, "Found I2C expander at address %#x", addr);
			printLine(gOutputData);
			#endif
			nFound += 1;
		}
	}

	//check if all I2C expanders have been found
	if (nFound != N_ADDRS) {
		#ifdef DEBUG
		printLine("Not all I2C expanders found!");
		#endif
		//keeping the LED off
		digitalWrite(LE1_PIN, LOW);
		//return 2 if at least one of the I2C expanders doesn't answer
		return 2;
	}

	//reconfiguring the I2C expanders (setting pins as inputs/outputs and so on)
	if (doReconfigureI2C) {
		#ifdef DEBUG
		printLine("Reconfiguring I2C expanders");
		#endif

		//performing the configuration depending on the probe card type
		#if PROBE_CARD == CROC_PROBE_CARD_V1P0
		//output
		writeRegister(0x20, OUTPUT_REG, 0x00);
		writeRegister(0x21, OUTPUT_REG, 0x00);
		writeRegister(0x22, OUTPUT_REG, 0x00);
		//polarity
		writeRegister(0x20, POLARITY_REG, 0x00);
		writeRegister(0x21, POLARITY_REG, 0x00);
		writeRegister(0x22, POLARITY_REG, 0x00);
		//configuration
		writeRegister(0x20, CONFIGURATION_REG, 0x00);
		writeRegister(0x21, CONFIGURATION_REG, 0x00);
		writeRegister(0x22, CONFIGURATION_REG, 0xC0);
		#elif PROBE_CARD == CROC_PROBE_CARD_V1P1
		//output
		writeRegister(0x20, OUTPUT_REG, 0x00);
		writeRegister(0x21, OUTPUT_REG, 0x00);
		writeRegister(0x22, OUTPUT_REG, 0x00);
		writeRegister(0x24, OUTPUT_REG, 0x00);
		//polarity
		writeRegister(0x20, POLARITY_REG, 0x00);
		writeRegister(0x21, POLARITY_REG, 0x00);
		writeRegister(0x22, POLARITY_REG, 0x00);
		writeRegister(0x24, POLARITY_REG, 0x00);
		//configuration
		writeRegister(0x20, CONFIGURATION_REG, 0x00);
		writeRegister(0x21, CONFIGURATION_REG, 0x00);
		writeRegister(0x22, CONFIGURATION_REG, 0xC0);
		writeRegister(0x24, CONFIGURATION_REG, 0x00);
		#elif PROBE_CARD == CROC_PROBE_CARD_V2P0
		//output
		writeRegister(0x20, OUTPUT_REG, 0x00);
		writeRegister(0x21, OUTPUT_REG, 0x00);
		writeRegister(0x22, OUTPUT_REG, 0x00);
		writeRegister(0x24, OUTPUT_REG, 0x00);
		//polarity
		writeRegister(0x20, POLARITY_REG, 0x00);
		writeRegister(0x21, POLARITY_REG, 0x00);
		writeRegister(0x22, POLARITY_REG, 0x00);
		writeRegister(0x24, POLARITY_REG, 0x00);
		//configuration
		writeRegister(0x20, CONFIGURATION_REG, 0x00);
		writeRegister(0x21, CONFIGURATION_REG, 0x00);
		writeRegister(0x22, CONFIGURATION_REG, 0xC0);
		writeRegister(0x24, CONFIGURATION_REG, 0x00);
		#endif
	}

	//reconfiguring probe card registers
	uint8_t nBytesWritten = 0;
	#if PROBE_CARD == CROC_PROBE_CARD_V1P0
	uint8_t nBytesToWrite = 3;
	nBytesWritten += writeRegister(0x20, OUTPUT_REG, 0b10001011);
	nBytesWritten += writeRegister(0x21, OUTPUT_REG, 0b00000000);
	nBytesWritten += writeRegister(0x22, OUTPUT_REG, 0b00010111);
	#elif PROBE_CARD == CROC_PROBE_CARD_V1P1
	uint8_t nBytesToWrite = 4;
	nBytesWritten += writeRegister(0x20, OUTPUT_REG, 0b11111111);
	nBytesWritten += writeRegister(0x21, OUTPUT_REG, 0b00000000);
	nBytesWritten += writeRegister(0x22, OUTPUT_REG, 0b00010111);
	nBytesWritten += writeRegister(0x24, OUTPUT_REG, 0b00000000);
	#elif PROBE_CARD == CROC_PROBE_CARD_V2P0
	uint8_t nBytesToWrite = 4;
	nBytesWritten += writeRegister(0x20, OUTPUT_REG, 0b01111111);
	nBytesWritten += writeRegister(0x21, OUTPUT_REG, 0b00000000);
	nBytesWritten += writeRegister(0x22, OUTPUT_REG, 0b00010111);
	nBytesWritten += writeRegister(0x24, OUTPUT_REG, 0b00000000);
	#endif

	//checking that the probe card has been reconfigured
	if (nBytesWritten != nBytesToWrite) {
		//keeping the LED off
		digitalWrite(LE1_PIN, LOW);
		//returning 1 <-> **failure**
		return 1;
	} else {
		//turning the LED on
		digitalWrite(LE1_PIN, HIGH);
		//returning 0 <-> **success**
		return 0;
	}
}

////////////////////////////////////////////////////////////////////////////////
/// Read the incoming data on the serial bus and store it in the gInputData
/// global variable.
////////////////////////////////////////////////////////////////////////////////
void readInput() {
	//clear the input array
	memset(&gInputData[0], 0, sizeof(gInputData));

	//save the input into the array
	SERIAL.readBytesUntil(READ_TERM, gInputData, sizeof(gInputData) - 1);
}

////////////////////////////////////////////////////////////////////////////////
/// Utility function to print data on the serial bus with the proper termination 
/// character.
///
/// @param message The message to write on the serial bus, without the 
/// termination character.
////////////////////////////////////////////////////////////////////////////////
void printLine(char message []) {
	SERIAL.print(message);    //write the message to the serial bus
	SERIAL.print(WRITE_TERM); //add the termination character
}

////////////////////////////////////////////////////////////////////////////////
/// Reads data from a register stored inside a PCA9554 I2C expander.
///
/// @param address The address of the I2C expander on the I2C bus.
/// @param registerNumber Number associated to the register: 0 for the input 
/// register, 1 for the output register, 2 for polarity register and 3 for the 
/// configuration register.
/// @param value Variable in which the read value will be stored.
/// @return The number of bytes read from the register.
////////////////////////////////////////////////////////////////////////////////
uint8_t readRegister(uint8_t address, uint8_t registerNumber, uint8_t & value) {
	//variable to be returned: stores the number of bytes read by the function
	uint8_t numberOfReadBytes = 0;

	//if DEBUG is defined, print the register being read
	#ifdef DEBUG
	sprintf(gOutputData, "Reading register %d", registerNumber);
	printLine(gOutputData);
	#endif

	//requesting data from I2C expander
	WIRE.beginTransmission(address);
	WIRE.write(registerNumber);
	uint8_t error = WIRE.endTransmission();

	if (error != 0) {
		//if DEBUG is defined and an error has occurred, print the address and
		//the register on the serial bus
		#ifdef DEBUG
		sprintf(gOutputData, "Error during write at address %d: %d", address, error);
		printLine(gOutputData);
		#endif
		//no byte was read -> return 0
		return 0;
	}

	//reading data from I2C expander
	WIRE.requestFrom(address, 1u);
	if ( WIRE.available() ) {
		value = WIRE.read();
		numberOfReadBytes++;
	} else {
		//if no data to read, print an error on the serial bus
		#ifdef DEBUG
		printLine("No available data to read!");
		#endif
		//no byte was read -> return 0
		return 0;
	}
	//return the number of successfully read bytes
	return numberOfReadBytes;
}

////////////////////////////////////////////////////////////////////////////////
/// Writes data to a register stored inside a PCA9554 I2C expander.
///
/// @param address The address of the I2C expander on the I2C bus.
/// @param registerNumber Number associated to the register: 0 for the input 
/// register, 1 for the output register, 2 for polarity register and 3 for the 
/// configuration register.
/// @param value The value to write to the register.
/// @return The number of bytes written to the register.
////////////////////////////////////////////////////////////////////////////////
uint8_t writeRegister(uint8_t address, uint8_t registerNumber, uint8_t value) {
	//if DEBUG is true, print on the serial bus what is about to be written and
	//where
	#ifdef DEBUG
	sprintf(
		gOutputData,
		"Writing %#x to register %#x at address %#x",
		value,
		registerNumber,
		address
	);
	printLine(gOutputData);
	#endif

	//writing the register of the I2C expander
	WIRE.beginTransmission(address);
	WIRE.write(registerNumber);
	WIRE.write(value);
	uint8_t error = WIRE.endTransmission();

	if (error != 0) {
		//if DEBUG is defined and an error has occurred, log what failed
		#ifdef DEBUG
		sprintf(gOutputData, "Error during write at address %d: %d", address, error);
		printLine(gOutputData);
		#endif
		//no bytes have been written to the register -> return 0
		return 0;
	}
	//return the number of written bytes. The PCA9554 have 1 byte registers, so
	//the number of successfully written bytes is always 1.
	return 1;
}

////////////////////////////////////////////////////////////////////////////////
/// Parses the input command stored into the gSplitInput global variable and
/// executes the operations related to the given command.
////////////////////////////////////////////////////////////////////////////////
void parseInput() {
	//TODO: add return codes

	/////////////////////////
	// Splitting the input //
	/////////////////////////

	//initialization: set gInputToken to null pointer and clear gSplitInput
	gInputToken = NULL;
	memset(&gSplitInput[0], 0, sizeof(gSplitInput));

	//split gInputData at each comma and store its pieces in gSplitInput
	gInputToken = strtok(gInputData, ",");
	int i = 0;
	while (gInputToken != NULL) {
		gSplitInput[i] = gInputToken;
		gInputToken = strtok(NULL, ",");
		i++;
	}

	//if DEBUG is defined, print all the fields in the input command
	#ifdef DEBUG
	SERIAL.print("Field 1: ");
	printLine(gSplitInput[0]);
	SERIAL.print("Field 2: ");
	printLine(gSplitInput[1]);
	SERIAL.print("Field 3: ");
	printLine(gSplitInput[2]);
	#endif

	//////////////////////////
	// Checking the command //
	//////////////////////////

	//hard reset: restart the sketch and power cycle the probe card
	if ( strcmp(gSplitInput[0], "RS") == 0 ) {
		#ifdef DEBUG
		printLine("Reset command received");
		#endif

		//serial output
		printLine("OK");

		//wait 10 ms before resetting; probably better for the communication
		//instead of resetting instantly after writing on the serial bus
		delay(10);

		//perform a software reset using the function from the Atmel Advanced
		//Software Framework (ASF)
		rstc_start_software_reset(RSTC);
	}

	//soft reset: reinitialize the WPAC and the probe card by writing the
	//default configuration
	if ( strcmp(gSplitInput[0], "IN") == 0 ) {
		#ifdef DEBUG
		printLine("Initialization command received");
		#endif

		//run the reinitialization function and get the error code
		uint8_t errorCode = initialize(false); // false <-> do not reconfigure I2C

		//checking the error code
		if (errorCode == 0) {
			sprintf(gOutputData, "OK");
		} else {
			sprintf(gOutputData, "ERR");
		}

		//printing the response
		printLine(gOutputData);
	}

	//CMOS
	else if ( strcmp(gSplitInput[0], "SC") == 0 ) {
		#ifdef DEBUG
		printLine("CMOS set command received");
		#endif

		/*
		//getting the CMOS line number as a base 10 number; don't care about the
		//end pointer (set to NULL)
		uint8_t lineNumber = strtoul(gSplitInput[1], NULL, 10);

		//getting the output logic value as a base 10 number; don't care about
		//the end pointer (set to NULL)
		uint8_t outputValue = strtoul(gSplitInput[2], NULL, 10);

		bool isInputOk = true;

		//checking line number
		if (lineNumber != 0 && lineNumber != 1) {
			isInputOk = false;
		}

		//checking the logic level
		if (outputValue != 0 && outputValue != 1) {
			isInputOk = false;
		}

		if (isInputOk) {
			sprintf(gOutputData, "OK");
		} else {
			sprintf(gOutputData, "ERR");
		}

		printLine(gOutputData);
		*/

		//serial output
		printLine("OK");
	}

	//set analog
	else if ( strcmp(gSplitInput[0], "SA") == 0 ) {
		#ifdef DEBUG
		printLine("Analog set command received");
		#endif

		//do something
		//...

		//serial output
		printLine("OK");
	}

	//read analog
	else if ( strcmp(gSplitInput[0], "RA") == 0 ) {
		#ifdef DEBUG
		printLine("Analog read command received");
		#endif

		//right now only A0 can be read; answer ERR if trying to read another
		//one
		if ( strcmp(gSplitInput[1], "0") == 0 ){
			//read value from ADC
			uint16_t wordFromADC = analogRead(A0);

			//formatting the answer message
			sprintf(gOutputData, "A,%d,%d", 0, wordFromADC);
		} else {
			sprintf(gOutputData, "ERR");
		}

		//serial output
		printLine(gOutputData);
	}

	//I2C write
	else if ( strcmp(gSplitInput[0], "IW") == 0 ) {
		#ifdef DEBUG
		printLine("I2C write command received");
		#endif

		//get the address from the input data as a base 16 number; don't care
		//about the end pointer (set to NULL)
		uint8_t addressUnsigned = strtoul(gSplitInput[1], NULL, 16);

		//get the value from the input data as a base 16 number; don't care
		//about the end pointer (set to NULL)
		uint8_t valueUnsigned = strtoul(gSplitInput[2], NULL, 16);

		//write data and get the number of bytes written
		uint8_t numberOfWrittenBytes = writeRegister(
			addressUnsigned, //address
			OUTPUT_REG,      //register
			valueUnsigned    //value to write
		);

		//check that the number of written bytes is not 0
		if (numberOfWrittenBytes > 0) {
			sprintf(gOutputData, "OK");
		} else {
			sprintf(gOutputData, "ERR");
		}

		//serial output
		printLine(gOutputData);
	}

	//I2C read
	else if ( strcmp(gSplitInput[0], "IR") == 0 ) {
		#ifdef DEBUG
		printLine("I2C read command received");
		#endif

		//getting the address from the input data as a base 16 number; don't
		//care about the end pointer (set to NULL)
		uint8_t addressUnsigned = strtoul(gSplitInput[1], NULL, 16);

		//reading the input register and getting the number of bytes read
		uint8_t numberOfReadBytes = readRegister(
			addressUnsigned, //address
			INPUT_REG,       //register
			gReadByte        //buffer
		);

		//if the number of read bytes is 0, answer ERR, otherwise answer normally
		if (numberOfReadBytes > 0) {
			//formatting the answer message
			sprintf(gOutputData, "I,0x%02X", gReadByte);
		} else {
			sprintf(gOutputData, "ERR");
		}

		//serial output
		printLine(gOutputData);
	}

	//read temperature
	else if ( strcmp(gSplitInput[0], "RT") == 0 ) {
		#ifdef DEBUG
		printLine("Temperature read command received");
		#endif

		//getting the temperature sensor ID
		uint8_t id = strtoul(gSplitInput[1], NULL, 10);
		bool success = false;
		float_t tempReading;

		//checking the value
		for (size_t i = 0; i < N_TEMP_SENSORS; i++) {
			if (tempSensors[i].id == id) {
				success = tempSensors[i].readTemperature(tempReading);
				break;
			}
		}

		//formatting the answer
		if (success == true) {
			sprintf(gOutputData, "T,%2.2f", tempReading);
		} else {
			sprintf(gOutputData, "ERR");
		}

		//serial output
		printLine(gOutputData);
	}

	//set MUX
	else if ( strcmp(gSplitInput[0], "SM") == 0 ) {
		#ifdef DEBUG
		printLine("MUX set command received");
		#endif

		//check that the status is either 0 or 1 and perform the required
		//action, otherwise answer ERR
		if ( strcmp(gSplitInput[1], "0") == 0 ) {
			digitalWrite(MUXSEL_PIN, LOW);
			sprintf(gOutputData, "OK");
		} else if ( strcmp(gSplitInput[1], "1") == 0 ) {
			digitalWrite(MUXSEL_PIN, HIGH);
			sprintf(gOutputData, "OK");
		} else {
			sprintf(gOutputData, "ERR");
		}

		//serial output
		printLine(gOutputData);
	}

	//set DAC
	else if ( strcmp(gSplitInput[0], "SD") == 0 ) {
		#ifdef DEBUG
		printLine("DAC set command received");
		#endif

		//getting the DAC code from the input message as a base 10 number; don't
		//care about the end pointer (set to NULL)
		uint16_t dacValue = strtoul(gSplitInput[1], NULL, 10);

		//checking that it's not greater than the safety limit
		if (dacValue < 0 || dacValue > DAC_MAX) {
			sprintf(gOutputData, "ERR");
		} else {
			analogWrite(DAC_PIN, dacValue);
			sprintf(gOutputData, "OK");
		}

		//serial output
		printLine(gOutputData);
	}

	//enable/disable VCC
	else if ( strcmp(gSplitInput[0], "EV") == 0 ) {
		#ifdef DEBUG
		printLine("ENVCC set command received");
		#endif

		//check that the status is either 0 or 1 and perform the required
		//action, otherwise answer ERR
		if ( strcmp(gSplitInput[1], "1") == 0 ) {
			digitalWrite(ENVCC_PIN, HIGH);
			sprintf(gOutputData, "OK");
		} else if ( strcmp(gSplitInput[1], "0") == 0 ) {
			digitalWrite(ENVCC_PIN, LOW);
			sprintf(gOutputData, "OK");
		} else {
			sprintf(gOutputData, "ERR");
		}

		//serial output
		printLine(gOutputData);
	}

	//read edge sensors via microcontroller GPIO pins
	else if ( strcmp(gSplitInput[0], "ES") == 0 ) {
		#ifdef DEBUG
		printLine("Edge sensors reading command received");
		#endif

		//perform a digital reading on the edge sensor pins
		uint8_t statusES1 = digitalRead(ES1_PIN);
		uint8_t statusES2 = digitalRead(ES2_PIN);
		uint8_t statusES = (statusES2 << 1) + statusES1;

		//formatting the answer: returning the values as "ES,(0,1,2,3)"
		sprintf(gOutputData, "ES,%u", statusES);

		//serial output
		printLine(gOutputData);
	}

	//set powering scheme for the chip
	else if ( strcmp(gSplitInput[0], "PC") == 0 ) {
		#ifdef DEBUG
		printLine("Power board configuration command received");
		#endif

		//getting the configuration to set from input
		uint8_t value = strtoul(gSplitInput[1], NULL, 10);

		//checking the input value
		if (value < 0 || value > 3) {
			//if the value is incorrect, do nothing and just return the error
			//string
			sprintf(gOutputData, "ERR");
		} else {
			//Extracting the two logic states from the unsigned integer. Note
			//that HIGH and LOW are two macros and map to, respectively, 1 and
			//0. This part of the code would break if this part of the
			//Arduino libraries changed, but this is not likely, as it would
			//probably break a lot of external libraries as well.
			uint8_t pc0_state = value & 1;
			uint8_t pc1_state = (value & 2) >> 1;

			//Setting first the bit that selects between direct powering and
			//normal one, then the bit that enables or disables the output.
			digitalWrite(PC1_PIN, pc1_state);
			//A delay might be needed here, depending on the use-case. At the
			//moment the correct configuration must be performed by the
			//library (Python-side).
			digitalWrite(PC0_PIN, pc0_state);
			//setting the output message
			sprintf(gOutputData, "OK");
		}

		//serial output
		printLine(gOutputData);
	}

	//set a short between VINA and VIND on the Power board v1.1
	else if ( strcmp(gSplitInput[0], "PS") == 0 ) {
		#ifdef DEBUG
		printLine("Power board shorting command received");
		#endif

		//getting the configuration to set from input
		uint8_t value = strtoul(gSplitInput[1], NULL, 10);

		//checking the input value
		if (value < 0 || value > 3) {
			//If the value is incorrect, do nothing and just return the error
			//string,
			sprintf(gOutputData, "ERR");
		} else {
			//Setting the last bit that shorts together the channels.
			digitalWrite(PC2_PIN, value);
			//setting the output message
			sprintf(gOutputData, "OK");
		}
		//serial output
		printLine(gOutputData);
	}

	//select the I2C main device through the Patch Board
	else if ( strcmp(gSplitInput[0], "PB") == 0 ) {
		#ifdef DEBUG
		printLine("Patch Board control command received");
		#endif

		//getting the configuration to set from input
		uint8_t value = strtoul(gSplitInput[1], NULL, 10);

		//checking the input value
		if (value < 0 || value > 1) {
			//If the value is incorrect, do nothing and just return the error
			//string,
			sprintf(gOutputData, "ERR");
		} else {
			//Setting the SEL signal for the Patch Board
			digitalWrite(SEL_PIN, value);
			//setting the output message
			sprintf(gOutputData, "OK");
		}
		//serial output
		printLine(gOutputData);
	}

	//no match
	else {
		#ifdef DEBUG
		printLine("Invalid command");
		#endif

		//serial output
		printLine("ERR");
	}
}
