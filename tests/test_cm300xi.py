"""Script to test the probe station library for the CM300xi."""

import unittest

from wlt.hardware.probers import CM300xi, ReplyCM300xi
from wlt.hardware.probers import ProberError, InvalidDieError

PORT = 22
SKIP_PROBER_QUERIES = False
_SKIP_MESSAGE = "No prober to test"


class TestReplyGood(unittest.TestCase):
	"""Tests that the parsing of the replies works as expected."""

	def setUp(self):
		self.raw = "0: 7 15 22355.6253 -131771.93393 0 1 2 138 0 0"
		self.command = "ReadMapPosition"
		self.reply = ReplyCM300xi(self.command, self.raw)

	def test_error_not_none(self):
		self.assertIsNotNone(self.reply.get_error())

	def test_error_zero(self):
		self.assertEqual(self.reply.get_error(), 0)

	def test_fields_not_none(self):
		self.assertIsNotNone(self.reply.get_fields())

	def test_fields(self):
		fields = self.reply.get_fields()
		col = fields["Column"]
		row = fields["Row"]
		self.assertEqual((col, row), (7, 15))


class TestReplyBad(unittest.TestCase):
	"""Tests that the parsing of the replies works as expected."""

	def setUp(self):
		self.raw = "0: 7 15 223"
		self.command = "ReadMapPosition"
		self.reply = ReplyCM300xi(self.command, self.raw)

	def test_error_not_none(self):
		self.assertIsNotNone(self.reply.get_error())

	def test_error_zero(self):
		self.assertEqual(self.reply.get_error(), 0)

	def test_fields_none(self):
		self.assertIsNone(self.reply.get_fields())


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestQuery(unittest.TestCase):
	"""Tests that probe station queries work as expected."""

	def setUp(self):
		self.prober = CM300xi(PORT)

	def test_good_command(self):
		self.prober.query("*IDN?")

	def test_bad_command(self):
		with self.assertRaises(ProberError):
			self.prober.query("SomeNonExistingCommand")


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestId(unittest.TestCase):
	"""Tests that the probe card identifier can be queried."""

	def setUp(self):
		self.prober = CM300xi(PORT)

	def test_communication(self):
		idn = self.prober.get_id()
		self.assertIsNotNone(idn)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestGetDie(unittest.TestCase):
	"""Tests the `get_die` probe station method."""

	def setUp(self):
		self.prober = CM300xi(PORT)
		self.col, self.row = self.prober.get_die()

	def test_values(self):
		self.assertIsInstance(self.col, int)
		self.assertIsInstance(self.row, int)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestGotoDie(unittest.TestCase):
	"""Tests that probe station movements with column and row are working."""

	def setUp(self):
		self.prober = CM300xi(PORT)

	def test_good_die(self):
		#try to go to a central die for the CROC wafer map
		col_exp = 6
		row_exp = 8
		self.prober.goto_die(col_exp, row_exp)
		col, row = self.prober.get_die()
		self.assertEqual((col_exp, row_exp), (col, row))

	def test_good_die2(self):
		#try to go to another die in the wafermap
		col_exp = 7
		row_exp = 9
		self.prober.goto_die(col_exp, row_exp)
		col, row = self.prober.get_die()
		self.assertEqual((col_exp, row_exp), (col, row))

	def test_bad_die(self):
		#try to go to a non-existing die in the CROC wafer map
		with self.assertRaises(InvalidDieError):
			self.prober.goto_die(42, 42)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestIsInContact(unittest.TestCase):
	"""Tests that the method to check if the chuck is at contact height works."""

	def setUp(self):
		self.prober = CM300xi(PORT)

	def test_bool(self):
		self.assertIsInstance(self.prober.is_in_contact(), bool)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestGoToContact(unittest.TestCase):
	"""Tests that the method to go to contact height works."""

	def setUp(self):
		self.prober = CM300xi(PORT)

	def test_bool(self):
		self.prober.contact()
		self.assertEqual(self.prober.is_in_contact(), True)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestGoToSeparation(unittest.TestCase):
	"""Tests that the method to go to separation height works."""

	def setUp(self):
		self.prober = CM300xi(PORT)

	def test_bool(self):
		self.prober.separate()
		self.assertEqual(self.prober.is_in_contact(), False)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestGetPosition(unittest.TestCase):
	"""Tests that the method to go to separation height works."""

	def setUp(self):
		self.prober = CM300xi(PORT)

	def test_float(self):
		x, y, z = self.prober.get_position()
		self.assertIsInstance(x, float)
		self.assertIsInstance(y, float)
		self.assertIsInstance(z, float)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestGetPositionZero(unittest.TestCase):
	"""Tests that the method to go to separation height works."""

	def setUp(self):
		self.prober = CM300xi(PORT)

	def test_float(self):
		x, y, z = self.prober.get_position_from_zero()
		self.assertIsInstance(x, float)
		self.assertIsInstance(y, float)
		self.assertIsInstance(z, float)


#run the tests
unittest.main()

#reply = prober.query("ReadChuckPosition")
#fields = reply.get_fields()
#assert reply.get_error() is not None
#assert fields is not None
#assert len(reply.get_fields()) != 0
#
#reply = prober.query("*IDN?")
#assert reply.get_error() is None
#assert reply.get_fields() is None
#
#reply = prober.query("StepNextDie 6 8")
#fields = reply.get_fields()
#assert reply.get_error() is not None
#assert reply.get_error() == 0
#assert fields["Column"] == 6
#assert fields["Row"] == 8
#
#reply= prober.query("StepNextDie")
#fields = reply.get_fields()
#assert reply.get_error() is not None
#assert reply.get_error() == 0
#assert fields["Column"] == 7
#assert fields["Row"] == 8
#
#reply = prober.query("ReadChuckStatus")
#fields = reply.get_fields()
#assert reply.get_error() is not None
#assert reply.get_error() == 0
#assert fields is not None
#
#reply = prober.query("GetDieDataAsColRow")
#fields = reply.get_fields()
#assert fields is not None

#reply = prober.query("StepNextDie 15 15")
