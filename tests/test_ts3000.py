"""Script to test the probe station library for the TS3000."""

import unittest

from wlt.hardware.probers import TS3000, ReplyTS3000
from wlt.hardware.probers import ProberError, InvalidDieError

PORT = 11
SKIP_PROBER_QUERIES = True
_SKIP_MESSAGE = "No prober to test"


class TestReplyGood(unittest.TestCase):
	"""Tests that the parsing of the replies works as expected."""

	def setUp(self):
		self.raw = "0,0,6,8,0"
		self.command = "map:step_die"
		self.reply = ReplyTS3000(self.command, self.raw)

	def test_error_not_none(self):
		self.assertIsNotNone(self.reply.get_error())

	def test_error_zero(self):
		self.assertEqual(self.reply.get_error(), 0)

	def test_fields_not_none(self):
		self.assertIsNotNone(self.reply.get_fields())

	def test_fields(self):
		fields = self.reply.get_fields()
		col = fields["Column Index"]
		row = fields["Row Index"]
		self.assertEqual((col, row), (6, 8))


class TestReplyBad(unittest.TestCase):
	"""Tests that the parsing of the replies works as expected."""

	def setUp(self):
		self.raw = "0,6,8,0"
		self.command = "map:step_die"
		self.reply = ReplyTS3000(self.command, self.raw)

	def test_error_not_none(self):
		self.assertIsNotNone(self.reply.get_error())

	def test_error_zero(self):
		self.assertEqual(self.reply.get_error(), 0)

	def test_fields_none(self):
		self.assertIsNone(self.reply.get_fields())


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestQuery(unittest.TestCase):
	"""Tests that probe station queries work as expected."""

	def setUp(self):
		self.prober = TS3000(PORT)

	def test_good_command(self):
		self.prober.query("*IDN?")

	def test_bad_command(self):
		with self.assertRaises(ProberError):
			self.prober.query("SomeNonExistingCommand")


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestId(unittest.TestCase):
	"""Tests that the probe card identifier can be queried."""

	def setUp(self):
		self.prober = TS3000(PORT)

	def test_communication(self):
		idn = self.prober.get_id()
		self.assertIsNotNone(idn)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestGetDie(unittest.TestCase):
	"""Tests the `get_die` probe station method."""

	def setUp(self):
		self.prober = TS3000(PORT)
		self.col, self.row = self.prober.get_die()

	def test_values(self):
		self.assertIsInstance(self.col, int)
		self.assertIsInstance(self.row, int)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestGotoDie(unittest.TestCase):
	"""Tests that probe station movements with column and row are working."""

	def setUp(self):
		self.prober = TS3000(PORT)

	def test_good_die(self):
		#try to go to a central die for the CROC wafer map
		col_exp = 6
		row_exp = 8
		self.prober.goto_die(col_exp, row_exp)
		col, row = self.prober.get_die()
		self.assertEqual((col_exp, row_exp), (col, row))

	def test_good_die2(self):
		#try to go to another die in the wafermap
		col_exp = 7
		row_exp = 9
		self.prober.goto_die(col_exp, row_exp)
		col, row = self.prober.get_die()
		self.assertEqual((col_exp, row_exp), (col, row))

	def test_bad_die(self):
		#try to go to a non-existing die in the CROC wafer map
		self.assertRaises(self.prober.goto_die(42, 42), InvalidDieError)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestIsInContact(unittest.TestCase):
	"""Tests that the method to check if the chuck is at contact height works."""

	def setUp(self):
		self.prober = TS3000(PORT)

	def test_bool(self):
		self.assertIsInstance(self.prober.is_in_contact(), bool)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestGoToContact(unittest.TestCase):
	"""Tests that the method to go to contact height works."""

	def setUp(self):
		self.prober = TS3000(PORT)

	def test_bool(self):
		self.prober.contact()
		self.assertEqual(self.prober.is_in_contact(), True)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestGoToSeparation(unittest.TestCase):
	"""Tests that the method to go to separation height works."""

	def setUp(self):
		self.prober = TS3000(PORT)

	def test_bool(self):
		self.prober.separate()
		self.assertEqual(self.prober.is_in_contact(), False)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestGetPosition(unittest.TestCase):
	"""Tests that the method to go to separation height works."""

	def setUp(self):
		self.prober = TS3000(PORT)

	def test_float(self):
		x, y, z = self.prober.get_position()
		self.assertIsInstance(x, float)
		self.assertIsInstance(y, float)
		self.assertIsInstance(z, float)


@unittest.skipIf(SKIP_PROBER_QUERIES is True, _SKIP_MESSAGE)
class TestGetPositionZero(unittest.TestCase):
	"""Tests that the method to go to separation height works."""

	def setUp(self):
		self.prober = TS3000(PORT)

	def test_float(self):
		x, y, z = self.prober.get_position_from_zero()
		self.assertIsInstance(x, float)
		self.assertIsInstance(y, float)
		self.assertIsInstance(z, float)


#run the tests
unittest.main()
